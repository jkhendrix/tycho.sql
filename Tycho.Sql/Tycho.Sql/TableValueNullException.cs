﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tycho.Sql
{
    /// <summary>
    /// Exception raised by Tycho Sql when a null value is used in a TableParameter DTO in a property identified as "not null".
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class TableValueNullException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TableValueNullException"/> class.
        /// </summary>
        public TableValueNullException() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="TableValueNullException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public TableValueNullException(string message) : base(message) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="TableValueNullException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (<see langword="Nothing" /> in Visual Basic) if no inner exception is specified.</param>
        public TableValueNullException(string message, Exception innerException) : base(message, innerException) { }
    }
}
