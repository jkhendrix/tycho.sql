﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tycho.Sql
{
    /// <summary>
    /// Attribute used to indicate a class is a DTO which can be used by Tycho Sql in a Table Parameter.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    /// <remarks>
    ///     <para>All Table Parameter DTO classes must bind to a Sql user-defined table type.  When configuring a DTO class, this type name is passed as a parameter to the TableParameter Attribute:</para>
    ///     <code language="cs">
    ///         [TableParameter("dbo.SomeUserDefinedTableType")]
    ///         public class SomeDTO 
    ///         {
    ///             // define columns here
    ///         }
    ///     </code>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Class)]
    public class TableParameterAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets the DB user-defined table type name.
        /// </summary>
        /// <value>
        /// The DB user-defined table type name.
        /// </value>
        public string TypeName { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TableParameterAttribute"/> class.
        /// </summary>
        /// <param name="TypeName">DB user-defined table type name.</param>
        public TableParameterAttribute(string TypeName)
        {
            this.TypeName = TypeName;
        }
    }
}
