﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tycho.Sql
{
    /// <summary>
    /// Attribute used to indicate how DTO properties bind to the Sql user-defined table type for a Table Parameter in Tycho Sql.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    /// <remarks>
    ///     <para>
    ///         Binding DTO properties to a user defined table type can be difficult.  You need to determine which properties of the DTO bind to which columns of the Sql type.  Start with a Sql user-defined table type to which you want to bind:
    ///     </para>
    ///     <code language="sql">
    ///         CREATE TYPE [dbo].[SomeType] AS TABLE(
    ///             [SomeName] [varchar](100) NULL,
    ///             [SomeOtherName] [bit] NOT NULL,
    ///             [ThirdColumn] [varchar] (20) NOT NULL
    ///         )
    ///     </code>
    ///     <para>A DTO for this Sql type might begin as a simple class definition:</para>
    ///     <code language="cs">
    ///         [TableParameter("dbo.SomeType")]
    ///         public class TableTypeWithParameters
    ///         {
    ///             public string SomeName { get; set; }
    ///             public bool SomeOtherName { get; set; }
    ///             public string ThirdColumn { get; set; }
    ///         }
    ///     </code>
    ///     <para>
    ///         This, however, is not enough information to complete the data binding process.  We still need
    ///         <list type="number">
    ///             <item>a strong definition of the column name</item>
    ///             <item>an indication of column nullability (optional)</item>
    ///             <item>a maximum length for string values (optional)</item>
    ///         </list>
    ///         These can all be defined by using the TableParameterColumn Attribute in your class DTO definition:
    ///     </para>
    ///     <code language="cs">
    ///         [TableParameter("dbo.SomeType")]
    ///         public class TableTypeWithParameters
    ///         {
    ///             [TableParameterColumn("SomeName")]
    ///             public string Id { get; set; }
    ///             [TableParameterColumn("SomeOtherName", IsNullable = false)]
    ///             public bool BooleanValue { get; set; }
    ///             [TableParameterColumn("ThirdColumn", IsNullable = false, MaxLength = 20)]
    ///             public string ThirdValue { get; set; }
    ///         }
    ///     </code>
    ///     <para>
    ///         There are a number of points to observe about the above definition.
    ///     </para>
    ///     <list type="bullet">
    ///         <item>
    ///             Column Name should match exactly with the definition of the column in Sql.  By defining a strong Column Name, you are free to rename the DTO properties within your application to anything that makes sense.  Notice how the Column "SomeName" is now bound to the DTO property "Id".
    ///         </item>
    ///         <item>
    ///             If your Sql user-defined table type defines NOT NULL columns, you can indicate these in your DTO with an additional "IsNullable = false" parameter.  Tycho Sql will then throw a <see cref="TableValueNullException"/> when it encounters a null value which has been assigned to this DTO property.  This exception will be thrown when the IEnumerable of DTOs is used in the fluent action <see cref="SqlAction.AddTableParameter{T}(string, IEnumerable{T})"/>.
    ///         </item>
    ///         <item>
    ///             The MaxLength parameter of the TableParameterColumn Attribute will actively trim your input to the defined length, thus preventing any errors caused by sending string values that are too large to fit in the defined column type in Sql.
    ///         </item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Property)]
    public class TableParameterColumnAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets the name of the column.
        /// </summary>
        /// <value>
        /// The name of the column.
        /// </value>
        public string ColumnName { get; private set; }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is nullable.  Default: TRUE
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is nullable; otherwise, <c>false</c>.
        /// </value>
        public bool IsNullable { get; set; }
        /// <summary>
        /// Gets or sets the maximum length of this STRING column value.
        /// </summary>
        /// <value>
        /// The maximum length.
        /// </value>
        public int MaxLength { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TableParameterColumnAttribute"/> class.
        /// </summary>
        /// <param name="ColumnName">Name of the column.</param>
        public TableParameterColumnAttribute(string ColumnName)
        {
            this.ColumnName = ColumnName;
            this.IsNullable = true;
            this.MaxLength = 0;
        }
    }
}
