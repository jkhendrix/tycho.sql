﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tycho.Sql
{
    /// <summary>
    /// Extensions to standard objects
    /// </summary>
    internal static class Extensions
    {
        /// <summary>
        /// Extracts a substring before a matching string.  If match isn't found, original is returned.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="match">The match.</param>
        /// <returns></returns>
        internal static string SubstringBefore(this string value, string match)
        {
            var x = value.IndexOf(match);
            return (x != -1) ? value.Substring(0, x) : value;
        }

        /// <summary>
        /// Truncates a string to a specified maximum length.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="maxLength">The maximum length.</param>
        /// <returns></returns>
        internal static string TruncateTo(this string value, int? maxLength)
        {
            return (value != null && maxLength.HasValue && maxLength > 0 && value.Length > maxLength) 
                ? value.Substring(0, maxLength.Value) 
                : value;
        }
    }
}
