﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tycho.Sql
{
    internal class TableParameter
    {
        public bool HasTableParameter { get; set; }
        public string TypeName { get; set; }
        public List<TableColumn> Columns { get; set; }
    }
}
