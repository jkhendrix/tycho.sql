﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Tycho.Sql.Output;

namespace Tycho.Sql.Interfaces
{
    /// <summary>
    /// Top level interface for Tycho.Sql service.
    /// </summary>
    public interface ISql
    {
        /// <summary>
        /// Begins a Stored Procedure call to the database.
        /// </summary>
        /// <param name="Name">The Stored Procedure name.</param>
        /// <param name="Schema">The Schema in which the Stored Procedure resides.</param>
        /// <returns>An ISqlActionConfiguring to allow fluent configuration of the Stored Procedure call.</returns>
        ISqlActionConfiguring Procedure(string Name, string Schema);

        /// <summary>
        /// Begins a Stored Procedure call to the database on the default (dbo) schema.
        /// </summary>
        /// <param name="Name">The Stored Procedure name.</param>
        /// <returns>An ISqlActionConfiguring to allow fluent configuration of the Stored Procedure call.</returns>
        ISqlActionConfiguring Procedure(string Name);

        /// <summary>
        /// Begins a Sql Statement call to the database.
        /// </summary>
        /// <param name="Text">The text of the Sql Statement.</param>
        /// <returns>An ISqlActionConfiguring to allow fluent configuration of the Stored Procedure call.</returns>
        ISqlActionConfiguring Statement(string Text);
    }

    /// <summary>
    /// Fluent interface allowing access to configuration options of a Tycho.Sql call.
    /// </summary>
    public interface ISqlActionConfiguring : ISqlActionConfigured
    {
        #region Setup

        /// <summary>
        /// Sets the exeuction timeout (in seconds) of this call.  A value of 0 has no timeout and will run until finished.  This value will override any default configured in the top level Sql service.
        /// </summary>
        /// <param name="Timeout">The timeout (in seconds).</param>
        /// <returns>Fluent ISqlActionConfiguring.</returns>
        ISqlActionConfiguring SetTimeout(int Timeout);

        #endregion

        #region Event Hooks

        /// <summary>
        /// Synchronous Func delegate to execute in case of any exception.  Returning True triggers an automatic re-execution of the DB call.  YOU are responsible for preventing endless loops!
        /// </summary>
        /// <param name="Handler">A synchronous Func delegate.</param>
        /// <returns>Fluent ISqlActionConfiguring.</returns>
        ISqlActionConfiguring OnException(Func<Exception, bool> Handler);

        /// <summary>
        /// Async Func delegate to execute in case of any exception.  Returning True triggers an automatic re-execution of the DB call.  YOU are responsible for preventing endless loops!
        /// </summary>
        /// <param name="Handler">A, async Func delegate.</param>
        /// <returns>Fluent ISqlActionConfiguring.</returns>
        ISqlActionConfiguring OnException(Func<Exception, Task<bool>> Handler);

        /// <summary>
        /// Synchronous Action delegate to call before the DB command is executed.  This passes the raw SqlCommand to your handler.  It is STRONGLY advised that you do not alter this SqlCommand, it is provided for inspection purposes only.
        /// </summary>
        /// <param name="Handler">A synchronous Action delegate.</param>
        /// <returns>Fluent ISqlActionConfiguring.</returns>
        ISqlActionConfiguring OnBeforeCommandExecute(Action<SqlCommand> Handler);

        /// <summary>
        /// Async Action delegate to call before the DB command is executed.  This passes the raw SqlCommand to your handler.  It is STRONGLY advised that you do not alter this SqlCommand, it is provided for inspection purposes only.
        /// </summary>
        /// <param name="Handler">An async Action delegate.</param>
        /// <returns>Fluent ISqlActionConfiguring.</returns>
        ISqlActionConfiguring OnBeforeCommandExecute(Func<SqlCommand, Task> Handler);

        #endregion
    }

    /// <summary>
    /// Fluent interface providing parameter definition and statement execution for a Tycho.Sql call.
    /// </summary>
    public interface ISqlActionConfigured
    {
        #region Parameters

        /// <summary>
        /// Add a named parameter to a Sql call.
        /// </summary>
        /// <param name="Name">The name of the parameter.</param>
        /// <param name="Value">The object value.</param>
        /// <param name="Type">The SqlDbType.</param>
        /// <returns>Fluent ISqlActionConfigured.</returns>
        ISqlActionConfigured AddParameter(string Name, object Value, SqlDbType Type);

        /// <summary>
        /// Adds the table parameter to a Sql call.
        /// </summary>
        /// <typeparam name="T">A DTO which has been decorated with [TableParameter] and [TableParameterColumn] attributes.</typeparam>
        /// <param name="Name">The name of the parameter.</param>
        /// <param name="TableData">An IEnumerable containing the table data.</param>
        /// <returns>
        /// Fluent ISqlActionConfigured.
        /// </returns>
        ISqlActionConfigured AddTableParameter<T>(string Name, IEnumerable<T> TableData) where T : class;

        /// <summary>
        /// Adds an INPUT/OUTPUT parameter to a Sql call.
        /// </summary>
        /// <typeparam name="T">The type of the output parameter.</typeparam>
        /// <param name="Name">The name of the parameter.</param>
        /// <param name="Type">The SqlDbType.</param>
        /// <param name="OutputDelegate">An Action delegate that will be called directly with the output value.</param>
        /// <returns>Fluent ISqlActionConfigured.</returns>
        ISqlActionConfigured AddOutputParameter<T>(string Name, SqlDbType Type, Action<T> OutputDelegate);

        /// <summary>
        /// Adds a sized INPUT/OUTPUT parameter to a Sql call.  This is usually used for CHAR/VARCHAR parameters.
        /// </summary>
        /// <typeparam name="T">The type of the output parameter.</typeparam>
        /// <param name="Name">The name of the parameter.</param>
        /// <param name="Type">The SqlDbType.</param>
        /// <param name="Size">The size of the parameter, e.g.: VARCHAR(20) = 20.</param>
        /// <param name="OutputDelegate">An Action delegate that will be called directly with the output value.</param>
        /// <returns>Fluent ISqlActionConfigured.</returns>
        ISqlActionConfigured AddOutputParameter<T>(string Name, SqlDbType Type, int Size, Action<T> OutputDelegate);

        /// <summary>
        /// Adds a precise numeric INPUT/OUTPUT parameter to a Sql Call.  This is useful for passing very specific floating point values both IN and OUT of the DB.
        /// </summary>
        /// <typeparam name="T">The type of the output parameter.</typeparam>
        /// <param name="Name">The name of the parameter.</param>
        /// <param name="Type">The SqlDbType.</param>
        /// <param name="Precision">The precision of the floating point number.</param>
        /// <param name="Scale">The scale of the floating point number.</param>
        /// <param name="OutputDelegate">An Action delegate that will be called directly with the output value.</param>
        /// <returns>Fluent ISqlActionConfigured.</returns>
        ISqlActionConfigured AddOutputParameter<T>(string Name, SqlDbType Type, byte Precision, byte Scale, Action<T> OutputDelegate);

        /// <summary>
        /// Adds an INPUT/OUTPUT parameter to a Sql call.
        /// </summary>
        /// <typeparam name="T">The type of the output parameter.</typeparam>
        /// <param name="Name">The name of the parameter.</param>
        /// <param name="Type">The SqlDbType.</param>
        /// <param name="OutputDelegate">An Action delegate that will be called directly with the output value.</param>
        /// <param name="InitialValue">The input value of the output parameter.</param>
        /// <returns>Fluent ISqlActionConfigured.</returns>
        ISqlActionConfigured AddOutputParameter<T>(string Name, SqlDbType Type, Action<T> OutputDelegate, T InitialValue);

        /// <summary>
        /// Adds a sized INPUT/OUTPUT parameter to a Sql call.  This is usually used for CHAR/VARCHAR parameters.
        /// </summary>
        /// <typeparam name="T">The type of the output parameter.</typeparam>
        /// <param name="Name">The name of the parameter.</param>
        /// <param name="Type">The SqlDbType.</param>
        /// <param name="Size">The size of the parameter, e.g.: VARCHAR(20) = 20.</param>
        /// <param name="OutputDelegate">An Action delegate that will be called directly with the output value.</param>
        /// <param name="InitialValue">The input value of the output parameter.</param>
        /// <returns>Fluent ISqlActionConfigured.</returns>
        ISqlActionConfigured AddOutputParameter<T>(string Name, SqlDbType Type, int Size, Action<T> OutputDelegate, T InitialValue);

        /// <summary>
        /// Adds a precise numeric INPUT/OUTPUT parameter to a Sql Call.  This is useful for passing very specific floating point values both IN and OUT of the DB.
        /// </summary>
        /// <typeparam name="T">The type of the output parameter.</typeparam>
        /// <param name="Name">The name of the parameter.</param>
        /// <param name="Type">The SqlDbType.</param>
        /// <param name="Precision">The precision of the floating point number.</param>
        /// <param name="Scale">The scale of the floating point number.</param>
        /// <param name="OutputDelegate">An Action delegate that will be called directly with the output value.</param>
        /// <param name="InitialValue">The input value of the output parameter.</param>
        /// <returns>Fluent ISqlActionConfigured.</returns>
        ISqlActionConfigured AddOutputParameter<T>(string Name, SqlDbType Type, byte Precision, byte Scale, Action<T> OutputDelegate, T InitialValue);

        #endregion

        #region Synchronous Execution

        /// <summary>
        /// Simple Sql action execution returning the number of records affected.
        /// </summary>
        /// <returns>Integer representing the number of records affected.</returns>
        int Execute();

        /// <summary>
        /// Executes the Sql action returning the value from the first column of the first row of the first result set.
        /// </summary>
        /// <typeparam name="T">The type of the returned value.</typeparam>
        /// <returns>The value from the first column of the first row of the first result set.</returns>
        T ExecuteScalar<T>();

        /// <summary>
        /// Executes the Sql action applying the specified Func Delegate to the first result set.
        /// </summary>
        /// <typeparam name="T">Object type returned by the Func Delegate.</typeparam>
        /// <param name="Delegate">The Func Delegate to apply to records from the result set.</param>
        /// <returns>An IEnumerable of output records.</returns>
        IEnumerable<T> ExecuteReader<T>(Func<SqlReader, T> Delegate);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <returns>A MultipleResulSets object allowing access to all IEnumerable result sets.</returns>
        MultipleResultSets<T1, T2> ExecuteReader<T1, T2>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        MultipleResultSets<T1, T2, T3> ExecuteReader<T1, T2, T3>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        MultipleResultSets<T1, T2, T3, T4> ExecuteReader<T1, T2, T3, T4>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        MultipleResultSets<T1, T2, T3, T4, T5> ExecuteReader<T1, T2, T3, T4, T5>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        MultipleResultSets<T1, T2, T3, T4, T5, T6> ExecuteReader<T1, T2, T3, T4, T5, T6>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        MultipleResultSets<T1, T2, T3, T4, T5, T6, T7> ExecuteReader<T1, T2, T3, T4, T5, T6, T7>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <param name="Delegate8">The Func Delegate to apply to records from the 8th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8> ExecuteReader<T1, T2, T3, T4, T5, T6, T7, T8>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7,
            Func<SqlReader, T8> Delegate8);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
        /// <typeparam name="T9">Object type returned by the 9th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <param name="Delegate8">The Func Delegate to apply to records from the 8th result set.</param>
        /// <param name="Delegate9">The Func Delegate to apply to records from the 9th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9> ExecuteReader<T1, T2, T3, T4, T5, T6, T7, T8, T9>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7,
            Func<SqlReader, T8> Delegate8,
            Func<SqlReader, T9> Delegate9);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
        /// <typeparam name="T9">Object type returned by the 9th Func Delegate.</typeparam>
        /// <typeparam name="T10">Object type returned by the 10th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <param name="Delegate8">The Func Delegate to apply to records from the 8th result set.</param>
        /// <param name="Delegate9">The Func Delegate to apply to records from the 9th result set.</param>
        /// <param name="Delegate10">The Func Delegate to apply to records from the 10th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> ExecuteReader<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7,
            Func<SqlReader, T8> Delegate8,
            Func<SqlReader, T9> Delegate9,
            Func<SqlReader, T10> Delegate10);

        /// <summary>
        /// Executes the Sql action applying the specified Action Delegate to the first result set.
        /// </summary>
        /// <param name="Action">The Action Delegate to apply to records from the result set.</param>
        void ExecuteReader(Action<SqlReader> Action);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <param name="Action8">The Action Delegate to apply to records from the 8th result set.</param>
        void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7,
            Action<SqlReader> Action8);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <param name="Action8">The Action Delegate to apply to records from the 8th result set.</param>
        /// <param name="Action9">The Action Delegate to apply to records from the 9th result set.</param>
        void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7,
            Action<SqlReader> Action8,
            Action<SqlReader> Action9);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <param name="Action8">The Action Delegate to apply to records from the 8th result set.</param>
        /// <param name="Action9">The Action Delegate to apply to records from the 9th result set.</param>
        /// <param name="Action10">The Action Delegate to apply to records from the 10th result set.</param>
        void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7,
            Action<SqlReader> Action8,
            Action<SqlReader> Action9,
            Action<SqlReader> Action10);

        #endregion

        #region Asynchronous Execution

        /// <summary>
        /// Simple Sql action execution returning the number of records affected.
        /// </summary>
        /// <returns>Awaitable Task returning the number of records affected.</returns>
        Task<int> ExecuteAsync();

        /// <summary>
        /// Executes the Sql action returning the value from the first column of the first row of the first result set.
        /// </summary>
        /// <typeparam name="T">The type of the returned value.</typeparam>
        /// <returns>Awaitable Task returning the value from the first column of the first row of the first result set.</returns>
        Task<T> ExecuteScalarAsync<T>();

        /// <summary>
        /// Executes the Sql action applying the specified Func Delegate to the first result set.
        /// </summary>
        /// <typeparam name="T">Object type returned by the Func Delegate.</typeparam>
        /// <param name="Delegate">The Func Delegate to apply to records from the result set.</param>
        /// <returns>Awaitable Task returning an IEnumerable of output records.</returns>
        Task<IEnumerable<T>> ExecuteReaderAsync<T>(Func<SqlReader, T> Delegate);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        Task<MultipleResultSets<T1, T2>> ExecuteReaderAsync<T1, T2>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        Task<MultipleResultSets<T1, T2, T3>> ExecuteReaderAsync<T1, T2, T3>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        Task<MultipleResultSets<T1, T2, T3, T4>> ExecuteReaderAsync<T1, T2, T3, T4>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        Task<MultipleResultSets<T1, T2, T3, T4, T5>> ExecuteReaderAsync<T1, T2, T3, T4, T5>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        Task<MultipleResultSets<T1, T2, T3, T4, T5, T6>> ExecuteReaderAsync<T1, T2, T3, T4, T5, T6>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        Task<MultipleResultSets<T1, T2, T3, T4, T5, T6, T7>> ExecuteReaderAsync<T1, T2, T3, T4, T5, T6, T7>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <param name="Delegate8">The Func Delegate to apply to records from the 8th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        Task<MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8>> ExecuteReaderAsync<T1, T2, T3, T4, T5, T6, T7, T8>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7,
            Func<SqlReader, T8> Delegate8);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
        /// <typeparam name="T9">Object type returned by the 9th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <param name="Delegate8">The Func Delegate to apply to records from the 8th result set.</param>
        /// <param name="Delegate9">The Func Delegate to apply to records from the 9th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        Task<MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9>> ExecuteReaderAsync<T1, T2, T3, T4, T5, T6, T7, T8, T9>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7,
            Func<SqlReader, T8> Delegate8,
            Func<SqlReader, T9> Delegate9);

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
        /// <typeparam name="T9">Object type returned by the 9th Func Delegate.</typeparam>
        /// <typeparam name="T10">Object type returned by the 10th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <param name="Delegate8">The Func Delegate to apply to records from the 8th result set.</param>
        /// <param name="Delegate9">The Func Delegate to apply to records from the 9th result set.</param>
        /// <param name="Delegate10">The Func Delegate to apply to records from the 10th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        Task<MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>> ExecuteReaderAsync<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7,
            Func<SqlReader, T8> Delegate8,
            Func<SqlReader, T9> Delegate9,
            Func<SqlReader, T10> Delegate10);

        /// <summary>
        /// Executes the Sql action applying the specified Action Delegate to the first result set.
        /// </summary>
        /// <param name="Action">The Action Delegate to apply to records from the result set.</param>
        /// <returns>Awaitable Task.</returns>
        Task ExecuteReaderAsync(Action<SqlReader> Action);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <returns>Awaitable Task.</returns>
        Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <returns>Awaitable Task.</returns>
        Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <returns>Awaitable Task.</returns>
        Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <returns>Awaitable Task.</returns>
        Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <returns>Awaitable Task.</returns>
        Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <returns>Awaitable Task.</returns>
        Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <param name="Action8">The Action Delegate to apply to records from the 8th result set.</param>
        /// <returns>Awaitable Task.</returns>
        Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7,
            Action<SqlReader> Action8);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <param name="Action8">The Action Delegate to apply to records from the 8th result set.</param>
        /// <param name="Action9">The Action Delegate to apply to records from the 9th result set.</param>
        /// <returns>Awaitable Task.</returns>
        Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7,
            Action<SqlReader> Action8,
            Action<SqlReader> Action9);

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <param name="Action8">The Action Delegate to apply to records from the 8th result set.</param>
        /// <param name="Action9">The Action Delegate to apply to records from the 9th result set.</param>
        /// <param name="Action10">The Action Delegate to apply to records from the 10th result set.</param>
        /// <returns>Awaitable Task.</returns>
        Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7,
            Action<SqlReader> Action8,
            Action<SqlReader> Action9,
            Action<SqlReader> Action10);

        #endregion
    }
}
