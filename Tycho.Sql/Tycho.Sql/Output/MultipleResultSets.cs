﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tycho.Sql.Output
{
    /// <summary>
    /// Output object returned by Tycho Sql actions when interfacing with multiple result sets.
    /// </summary>
    /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
    /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
    public class MultipleResultSets<T1, T2>
    {
        /// <summary>
        /// Gets the IEnumerable of output records for the 1st result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 1st result set.
        /// </value>
        public IEnumerable<T1> Set1 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 2nd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 2nd result set.
        /// </value>
        public IEnumerable<T2> Set2 { get; internal set; }
    }

    /// <summary>
    /// Output object returned by Tycho Sql actions when interfacing with multiple result sets.
    /// </summary>
    /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
    /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
    /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
    public class MultipleResultSets<T1, T2, T3>
    {
        /// <summary>
        /// Gets the IEnumerable of output records for the 1st result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 1st result set.
        /// </value>
        public IEnumerable<T1> Set1 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 2nd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 2nd result set.
        /// </value>
        public IEnumerable<T2> Set2 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 3rd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 3rd result set.
        /// </value>
        public IEnumerable<T3> Set3 { get; internal set; }
    }

    /// <summary>
    /// Output object returned by Tycho Sql actions when interfacing with multiple result sets.
    /// </summary>
    /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
    /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
    /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
    /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
    public class MultipleResultSets<T1, T2, T3, T4>
    {
        /// <summary>
        /// Gets the IEnumerable of output records for the 1st result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 1st result set.
        /// </value>
        public IEnumerable<T1> Set1 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 2nd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 2nd result set.
        /// </value>
        public IEnumerable<T2> Set2 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 3rd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 3rd result set.
        /// </value>
        public IEnumerable<T3> Set3 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 4th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 4th result set.
        /// </value>
        public IEnumerable<T4> Set4 { get; internal set; }
    }

    /// <summary>
    /// Output object returned by Tycho Sql actions when interfacing with multiple result sets.
    /// </summary>
    /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
    /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
    /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
    /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
    /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
    public class MultipleResultSets<T1, T2, T3, T4, T5>
    {
        /// <summary>
        /// Gets the IEnumerable of output records for the 1st result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 1st result set.
        /// </value>
        public IEnumerable<T1> Set1 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 2nd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 2nd result set.
        /// </value>
        public IEnumerable<T2> Set2 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 3rd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 3rd result set.
        /// </value>
        public IEnumerable<T3> Set3 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 4th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 4th result set.
        /// </value>
        public IEnumerable<T4> Set4 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 5th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 5th result set.
        /// </value>
        public IEnumerable<T5> Set5 { get; internal set; }
    }

    /// <summary>
    /// Output object returned by Tycho Sql actions when interfacing with multiple result sets.
    /// </summary>
    /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
    /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
    /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
    /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
    /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
    /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
    public class MultipleResultSets<T1, T2, T3, T4, T5, T6>
    {
        /// <summary>
        /// Gets the IEnumerable of output records for the 1st result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 1st result set.
        /// </value>
        public IEnumerable<T1> Set1 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 2nd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 2nd result set.
        /// </value>
        public IEnumerable<T2> Set2 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 3rd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 3rd result set.
        /// </value>
        public IEnumerable<T3> Set3 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 4th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 4th result set.
        /// </value>
        public IEnumerable<T4> Set4 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 5th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 5th result set.
        /// </value>
        public IEnumerable<T5> Set5 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 6th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 6th result set.
        /// </value>
        public IEnumerable<T6> Set6 { get; internal set; }
    }

    /// <summary>
    /// Output object returned by Tycho Sql actions when interfacing with multiple result sets.
    /// </summary>
    /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
    /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
    /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
    /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
    /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
    /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
    /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
    public class MultipleResultSets<T1, T2, T3, T4, T5, T6, T7>
    {
        /// <summary>
        /// Gets the IEnumerable of output records for the 1st result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 1st result set.
        /// </value>
        public IEnumerable<T1> Set1 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 2nd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 2nd result set.
        /// </value>
        public IEnumerable<T2> Set2 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 3rd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 3rd result set.
        /// </value>
        public IEnumerable<T3> Set3 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 4th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 4th result set.
        /// </value>
        public IEnumerable<T4> Set4 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 5th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 5th result set.
        /// </value>
        public IEnumerable<T5> Set5 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 6th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 6th result set.
        /// </value>
        public IEnumerable<T6> Set6 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 7th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 7th result set.
        /// </value>
        public IEnumerable<T7> Set7 { get; internal set; }
    }

    /// <summary>
    /// Output object returned by Tycho Sql actions when interfacing with multiple result sets.
    /// </summary>
    /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
    /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
    /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
    /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
    /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
    /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
    /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
    /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
    public class MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8>
    {
        /// <summary>
        /// Gets the IEnumerable of output records for the 1st result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 1st result set.
        /// </value>
        public IEnumerable<T1> Set1 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 2nd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 2nd result set.
        /// </value>
        public IEnumerable<T2> Set2 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 3rd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 3rd result set.
        /// </value>
        public IEnumerable<T3> Set3 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 4th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 4th result set.
        /// </value>
        public IEnumerable<T4> Set4 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 5th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 5th result set.
        /// </value>
        public IEnumerable<T5> Set5 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 6th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 6th result set.
        /// </value>
        public IEnumerable<T6> Set6 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 7th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 7th result set.
        /// </value>
        public IEnumerable<T7> Set7 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 8th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 8th result set.
        /// </value>
        public IEnumerable<T8> Set8 { get; internal set; }
    }

    /// <summary>
    /// Output object returned by Tycho Sql actions when interfacing with multiple result sets.
    /// </summary>
    /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
    /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
    /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
    /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
    /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
    /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
    /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
    /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
    /// <typeparam name="T9">Object type returned by the 9th Func Delegate.</typeparam>
    public class MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9>
    {
        /// <summary>
        /// Gets the IEnumerable of output records for the 1st result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 1st result set.
        /// </value>
        public IEnumerable<T1> Set1 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 2nd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 2nd result set.
        /// </value>
        public IEnumerable<T2> Set2 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 3rd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 3rd result set.
        /// </value>
        public IEnumerable<T3> Set3 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 4th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 4th result set.
        /// </value>
        public IEnumerable<T4> Set4 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 5th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 5th result set.
        /// </value>
        public IEnumerable<T5> Set5 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 6th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 6th result set.
        /// </value>
        public IEnumerable<T6> Set6 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 7th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 7th result set.
        /// </value>
        public IEnumerable<T7> Set7 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 8th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 8th result set.
        /// </value>
        public IEnumerable<T8> Set8 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 9th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 9th result set.
        /// </value>
        public IEnumerable<T9> Set9 { get; internal set; }
    }

    /// <summary>
    /// Output object returned by Tycho Sql actions when interfacing with multiple result sets.
    /// </summary>
    /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
    /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
    /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
    /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
    /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
    /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
    /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
    /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
    /// <typeparam name="T9">Object type returned by the 9th Func Delegate.</typeparam>
    /// <typeparam name="T10">Object type returned by the 10th Func Delegate.</typeparam>
    public class MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>
    {
        /// <summary>
        /// Gets the IEnumerable of output records for the 1st result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 1st result set.
        /// </value>
        public IEnumerable<T1> Set1 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 2nd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 2nd result set.
        /// </value>
        public IEnumerable<T2> Set2 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 3rd result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 3rd result set.
        /// </value>
        public IEnumerable<T3> Set3 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 4th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 4th result set.
        /// </value>
        public IEnumerable<T4> Set4 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 5th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 5th result set.
        /// </value>
        public IEnumerable<T5> Set5 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 6th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 6th result set.
        /// </value>
        public IEnumerable<T6> Set6 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 7th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 7th result set.
        /// </value>
        public IEnumerable<T7> Set7 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 8th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 8th result set.
        /// </value>
        public IEnumerable<T8> Set8 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 9th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 9th result set.
        /// </value>
        public IEnumerable<T9> Set9 { get; internal set; }
        /// <summary>
        /// Gets the IEnumerable of output records for the 10th result set.
        /// </summary>
        /// <value>
        /// IEnumerable of output records for the 10th result set.
        /// </value>
        public IEnumerable<T10> Set10 { get; internal set; }
    }
}
