﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tycho.Sql
{
    /// <summary>
    /// Enumeration of C# native data types
    /// </summary>
    internal enum NativeType
    {
        Unknown,
        String,
        DateTime,
        TimeSpan,
        DateTimeOffset,
        Float,
        Double,
        Decimal,
        Long,
        Int,
        Short,
        Byte,
        Bool,
        Guid,
        SqlMoney,
        ByteStream,
    }
}