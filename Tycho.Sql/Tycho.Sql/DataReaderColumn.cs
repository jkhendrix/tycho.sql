﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tycho.Sql
{
    /// <summary>
    /// Represents an output column in a DataReader object
    /// </summary>
    internal class DataReaderColumn
    {
        /// <summary>
        /// Gets the ordinal of the column.
        /// </summary>
        /// <value>
        /// The ordinal.
        /// </value>
        public int Ordinal { get; private set; }

        /// <summary>
        /// Gets the Sql type of the data.
        /// </summary>
        /// <value>
        /// The Sql type of the data.
        /// </value>
        public SqlDbType DataType { get; private set; }

        /// <summary>
        /// Gets the C# native type of the data.
        /// </summary>
        /// <value>
        /// The C# type of the data.
        /// </value>
        public NativeType NativeType { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataReaderColumn"/> class.
        /// </summary>
        /// <param name="ordinal">The ordinal.</param>
        /// <param name="typeName">Name of the type.</param>
        /// <exception cref="ArgumentException">
        /// </exception>
        public DataReaderColumn(int ordinal, string typeName)
        {
            Ordinal = ordinal;

            if (Enum.TryParse<SqlDbType>(typeName, true, out var dataType))
                DataType = dataType;
            else if (typeName.ToUpperInvariant() == "SQL_VARIANT")
                DataType = SqlDbType.Variant;
            else 
                throw new ArgumentException(string.Format("Unknown column type ({0})", typeName), typeName);
            NativeType = ResolveNativeType();
            if (NativeType == NativeType.Unknown)
                throw new ArgumentException(string.Format("Unable to resolve native data type for {0}", typeName), typeName);
        }

        private NativeType ResolveNativeType()
        {
            NativeType result = NativeType.Unknown;

            switch (DataType)
            {
                case SqlDbType.VarChar:
                case SqlDbType.NVarChar:
                case SqlDbType.Char:
                case SqlDbType.NChar:
                case SqlDbType.Text:
                case SqlDbType.NText:
                case SqlDbType.Xml:
                    result = NativeType.String;
                    break;
                case SqlDbType.DateTime:
                case SqlDbType.DateTime2:
                case SqlDbType.SmallDateTime:
                case SqlDbType.Date:
                    result = NativeType.DateTime;
                    break;
                case SqlDbType.Time:
                    result = NativeType.TimeSpan;
                    break;
                case SqlDbType.DateTimeOffset:
                    result = NativeType.DateTimeOffset;
                    break;
                case SqlDbType.Real:
                    result = NativeType.Float;
                    break;
                case SqlDbType.Float:
                    result = NativeType.Double;
                    break;
                case SqlDbType.Decimal:
                    result = NativeType.Decimal;
                    break;
                case SqlDbType.BigInt:
                    result = NativeType.Long;
                    break;
                case SqlDbType.Int:
                    result = NativeType.Int;
                    break;
                case SqlDbType.SmallInt:
                    result = NativeType.Short;
                    break;
                case SqlDbType.TinyInt:
                    result = NativeType.Byte;
                    break;
                case SqlDbType.Bit:
                    result = NativeType.Bool;
                    break;
                case SqlDbType.Money:
                case SqlDbType.SmallMoney:
                    result = NativeType.SqlMoney;
                    break;
                case SqlDbType.UniqueIdentifier:
                    result = NativeType.Guid;
                    break;
                case SqlDbType.Binary:
                case SqlDbType.VarBinary:
                case SqlDbType.Image:
                case SqlDbType.Udt:
                case SqlDbType.Variant:
                    result = NativeType.ByteStream;
                    break;
            }

            return result;
        }
    }
}
