﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Tycho.Sql.Interfaces;

namespace Tycho.Sql
{
    /// <summary>
    ///     <para>The Sql class is the starting point from which all Sql commands are initiated.  It provides an efficient fluent API for handling almost any DB call.</para>
    /// </summary>
    /// <remarks>
    ///     <para>Using the constructor you can specify a default Timeout value (in seconds) for all Sql commands executed.  If not specified, the default is 30s.</para>
    ///     <para>You can also specify a default Exception handler for Sql commands executed using the object instance.  This exception handler is defined as either a <c>Func&lt;Exception, bool&gt;</c> or an async <c>Func&lt;Exception, Task&lt;bool&gt;&gt;</c> allowing your code to examine the details of the caught exception before returning either <c>true</c> to automatically re-execute the Sql or <c>false</c> to throw the Exception.  If unspecified, the default Exception handler simply returns <c>false</c> to throw the Exception out of Tycho.Sql.</para>
    /// </remarks>
    /// <seealso cref="Tycho.Sql.Interfaces.ISql" />
    public class Sql : ISql
    {
        #region Public Properties

        /// <summary>
        /// Gets the connection string used by this instance of Tycho Sql.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        /// <remarks>
        /// If a connection string name from a config file is used as a constructor parameter, it is resolved into a connection string value once and stored here for reference purposes.
        /// </remarks>
        public string ConnectionString { get; private set; }

        /// <summary>
        /// Gets the default timeout (in seconds) for actions executed by this instance of Tycho Sql.
        /// </summary>
        /// <value>
        /// The default timeout (in seconds).
        /// </value>
        /// <remarks>
        /// If not specified in the constructor, the default timeout is 30s.
        /// </remarks>
        public int DefaultTimeout { get; private set; }

        /// <summary>
        /// Gets the default exception handler delegate for actions executed by this instance of Tycho Sql.
        /// </summary>
        /// <value>
        /// The default exception handler.
        /// </value>
        /// <remarks>
        ///     <para>If this Func delegate returns <c>true</c> then the failed Sql command is automatically re-executed.  If it returns <c>false</c> then the Exception is thrown out of this object to be caught by your application.</para>
        ///     <para>If not specified in the constructor, the default Exception handler simply returns <c>false</c> to throw the Exception.</para>
        /// </remarks>
        public Func<Exception, Task<bool>> DefaultExceptionHandler { get; private set; }

        #endregion

        #region Privates Properties

        // the currently configured default exception handler.
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
#pragma warning disable IDE1006 // Naming Styles
        private Func<Exception, Task<bool>> _defaultExceptionHandler => async (ex) => false;
#pragma warning restore IDE1006 // Naming Styles
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously

        // an internal cache of all table-parameter types encountered by this instance.
        private static readonly Dictionary<Type, TableParameter> _mappedTableTypes = new Dictionary<Type, TableParameter>();

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Sql"/> class using the specified connection string.
        /// </summary>
        /// <param name="ConnectionStringOrName">Either a full connection string, or the Name of a connection string from a config file.</param>
        /// <exception cref="ArgumentException">Argument ConnectionStringOrName cannot be null or only whitespace.</exception>
        /// <example>
        ///     <para>The following example demonstrates how to call the <see cref="Sql"/> constructor.:</para>
        ///     <code language="cs">
        ///     var ConnectionStringOrName = "DefaultConnectionString";
        ///     Sql x = new Sql(ConnectionStringOrName);
        ///     </code>
        /// </example>
        public Sql(string ConnectionStringOrName)
        {
            if (string.IsNullOrWhiteSpace(ConnectionStringOrName))
                throw new ArgumentException("Argument ConnectionStringOrName cannot be null or only whitespace.", "ConnectionStringOrName");

            ConnectionString = ResolveConnectionString(ConnectionStringOrName);
            this.DefaultExceptionHandler = _defaultExceptionHandler;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sql"/> class using the specified connection string and setting a default timeout for all sql calls.
        /// </summary>
        /// <param name="ConnectionStringOrName">Either a full connection string, or the Name of a connection string from a config file.</param>
        /// <param name="DefaultTimeout">The default timeout (in seconds).</param>
        /// <exception cref="ArgumentException">
        /// Argument ConnectionStringOrName cannot be null or only whitespace.
        /// or
        /// Argument DefaultTimeout cannot be &lt; 0.
        /// </exception>
        /// <example>
        ///     <para>The following example demonstrates how to call the <see cref="Sql"/> constructor.:</para>
        ///     <code language="cs">
        ///     var ConnectionStringOrName = "DefaultConnectionString";
        ///     var DefaultTimeout = 300;   // seconds
        ///     Sql x = new Sql(ConnectionStringOrName, DefaultTimeout);
        ///     </code>
        /// </example>
        public Sql(string ConnectionStringOrName, int DefaultTimeout)
        {
            if (string.IsNullOrWhiteSpace(ConnectionStringOrName))
                throw new ArgumentException("Argument ConnectionStringOrName cannot be null or only whitespace.", "ConnectionStringOrName");

            if (DefaultTimeout < 0)
                throw new ArgumentException("Argument DefaultTimeout cannot be < 0.", "DefaultTimeout");

            ConnectionString = ResolveConnectionString(ConnectionStringOrName);
            this.DefaultTimeout = DefaultTimeout;
            this.DefaultExceptionHandler = _defaultExceptionHandler;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sql"/> class using the specified connection string and defining a default exception handler.
        /// </summary>
        /// <param name="ConnectionStringOrName">Either a full connection string, or the Name of a connection string from a config file.</param>
        /// <param name="DefaultExceptionHandler">A Func delegate to a synchronous default exception handler.</param>
        /// <exception cref="ArgumentException">Argument ConnectionStringOrName cannot be null or only whitespace.</exception>
        /// <example>
        ///     <para>The following example demonstrates how to call the <see cref="Sql"/> constructor.:</para>
        ///     <code language="cs">
        ///     var ConnectionStringOrName = "DefaultConnectionString";
        ///     Sql x = new Sql(ConnectionStringOrName, (ex) => 
        ///     {
        ///         // examine the exception details
        ///         // return true to re-run the failed sql call, or false to throw the exception
        ///         return false;
        ///     });
        ///     </code>
        /// </example>
        public Sql(string ConnectionStringOrName, Func<Exception, bool> DefaultExceptionHandler)
        {
            if (string.IsNullOrWhiteSpace(ConnectionStringOrName))
                throw new ArgumentException("Argument ConnectionStringOrName cannot be null or only whitespace.", "ConnectionStringOrName");

            ConnectionString = ResolveConnectionString(ConnectionStringOrName);
            this.DefaultExceptionHandler = DefaultExceptionHandler == null
                ? _defaultExceptionHandler
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
                : async (ex) => DefaultExceptionHandler(ex);
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sql"/> class using the specified connection string and defining a default async exception handler.
        /// </summary>
        /// <param name="ConnectionStringOrName">Either a full connection string, or the Name of a connection string from a config file.</param>
        /// <param name="DefaultExceptionHandler">A Func delegate to an async default exception handler.</param>
        /// <exception cref="ArgumentException">Argument ConnectionStringOrName cannot be null or only whitespace.</exception>
        /// <example>
        ///     <para>The following example demonstrates how to call the <see cref="Sql"/> constructor.:</para>
        ///     <code language="cs">
        ///     var ConnectionStringOrName = "DefaultConnectionString";
        ///     Sql x = new Sql(ConnectionStringOrName, async (ex) => 
        ///     {
        ///         // examine the exception details
        ///         // return true to re-run the failed sql call, or false to throw the exception
        ///         return false;
        ///     });
        ///     </code>
        /// </example>
        public Sql(string ConnectionStringOrName, Func<Exception, Task<bool>> DefaultExceptionHandler)
        {
            if (string.IsNullOrWhiteSpace(ConnectionStringOrName))
                throw new ArgumentException("Argument ConnectionStringOrName cannot be null or only whitespace.", "ConnectionStringOrName");

            ConnectionString = ResolveConnectionString(ConnectionStringOrName);
            this.DefaultExceptionHandler = DefaultExceptionHandler ?? _defaultExceptionHandler;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sql"/> class using the specified connection string, setting a default timeout for all sql calls, and defining a default exception handler.
        /// </summary>
        /// <param name="ConnectionStringOrName">Either a full connection string, or the Name of a connection string from a config file.</param>
        /// <param name="DefaultTimeout">The default timeout (in seconds).</param>
        /// <param name="DefaultExceptionHandler">A Func delegate to a synchronous default exception handler.</param>
        /// <exception cref="ArgumentException">
        /// Argument ConnectionStringOrName cannot be null or only whitespace.
        /// or
        /// Argument DefaultTimeout cannot be &lt; 0.
        /// </exception>
        /// <example>
        ///     <para>The following example demonstrates how to call the <see cref="Sql"/> constructor.:</para>
        ///     <code language="cs">
        ///     var ConnectionStringOrName = "DefaultConnectionString";
        ///     var DefaultTimeout = 300;   // seconds
        ///     Sql x = new Sql(ConnectionStringOrName, DefaultTimeout, (ex) => 
        ///     {
        ///         // examine the exception details
        ///         // return true to re-run the failed sql call, or false to throw the exception
        ///         return false;
        ///     });
        ///     </code>
        /// </example>
        public Sql(string ConnectionStringOrName, int DefaultTimeout, Func<Exception, bool> DefaultExceptionHandler)
        {
            if (string.IsNullOrWhiteSpace(ConnectionStringOrName))
                throw new ArgumentException("Argument ConnectionStringOrName cannot be null or only whitespace.", "ConnectionStringOrName");

            if (DefaultTimeout < 0)
                throw new ArgumentException("Argument DefaultTimeout cannot be < 0.", "DefaultTimeout");

            ConnectionString = ResolveConnectionString(ConnectionStringOrName);
            this.DefaultTimeout = DefaultTimeout;
            this.DefaultExceptionHandler = DefaultExceptionHandler == null
                ? _defaultExceptionHandler
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
                : async (ex) => DefaultExceptionHandler(ex);
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sql"/> class using the specified connection string, setting a default timeout for all sql calls, and defining a default async exception handler.
        /// </summary>
        /// <param name="ConnectionStringOrName">Either a full connection string, or the Name of a connection string from a config file.</param>
        /// <param name="DefaultTimeout">The default timeout (in seconds).</param>
        /// <param name="DefaultExceptionHandler">A Func delegate to an async default exception handler.</param>
        /// <exception cref="ArgumentException">
        /// Argument ConnectionStringOrName cannot be null or only whitespace.
        /// or
        /// Argument DefaultTimeout cannot be &lt; 0.
        /// </exception>
        /// <example>
        ///     <para>The following example demonstrates how to call the <see cref="Sql"/> constructor.:</para>
        ///     <code language="cs">
        ///     var ConnectionStringOrName = "DefaultConnectionString";
        ///     var DefaultTimeout = 300;   // seconds
        ///     Sql x = new Sql(ConnectionStringOrName, DefaultTimeout, async (ex) => 
        ///     {
        ///         // examine the exception details
        ///         // return true to re-run the failed sql call, or false to throw the exception
        ///         return false;
        ///     });
        ///     </code>
        /// </example>
        public Sql(string ConnectionStringOrName, int DefaultTimeout, Func<Exception, Task<bool>> DefaultExceptionHandler)
        {
            if (string.IsNullOrWhiteSpace(ConnectionStringOrName))
                throw new ArgumentException("Argument ConnectionStringOrName cannot be null or only whitespace.", "ConnectionStringOrName");

            if (DefaultTimeout < 0)
                throw new ArgumentException("Argument DefaultTimeout cannot be < 0.", "DefaultTimeout");

            ConnectionString = ResolveConnectionString(ConnectionStringOrName);
            this.DefaultTimeout = DefaultTimeout;
            this.DefaultExceptionHandler = DefaultExceptionHandler ?? _defaultExceptionHandler;
        }

        #endregion

        #region ISql API

        /// <summary>
        /// Begins a Stored Procedure call to the database
        /// </summary>
        /// <param name="Name">The Stored Procedure name.</param>
        /// <param name="Schema">The Schema in which the Stored Procedure resides (default "dbo").</param>
        /// <returns>
        /// An ISqlActionConfiguring to allow fluent configuration of the Stored Procedure call.
        /// </returns>
        public ISqlActionConfiguring Procedure(string Name, string Schema)
        {
            return new SqlAction
            {
                _sql = this,
                _text = Name,
                _schema = Schema,
                _isProcedure = true,
            };
        }

        /// <summary>
        /// Begins a Stored Procedure call to the database on the default (dbo) schema.
        /// </summary>
        /// <param name="Name">The Stored Procedure name.</param>
        /// <returns>
        /// An ISqlActionConfiguring to allow fluent configuration of the Stored Procedure call.
        /// </returns>
        public ISqlActionConfiguring Procedure(string Name)
            => Procedure(Name, "dbo");

        /// <summary>
        /// Begins a Sql Statement call to the database.
        /// </summary>
        /// <param name="Text">The text of the Sql Statement.</param>
        /// <returns>
        /// An ISqlActionConfiguring to allow fluent configuration of the Stored Procedure call.
        /// </returns>
        public ISqlActionConfiguring Statement(string Text)
        {
            return new SqlAction
            {
                _sql = this,
                _text = Text,
                _schema = null,
                _isProcedure = false,
            };
        }

        #endregion

        #region Internal API

        // resolves named connection string to their config file values
        internal string ResolveConnectionString(string ConnectionStringOrName)
        {
            string result;
            try
            {
                // check for matching connecting string name in config file
                result = ConfigurationManager.ConnectionStrings[ConnectionStringOrName].ConnectionString;
            }
            catch
            {
                // assume the parameter value is the actual connection string if it does not exist in the config file
                result = ConnectionStringOrName;
            }
            return result;
        }

        // creates or returns a cached mapping of table parameter object columns.
        internal TableParameter GetTableParameterDefinition<T>()
        {
            // get map of table columns.  reflection can be expensive, so cache these results for re-use.
            var tableBaseType = typeof(T);
            if (!_mappedTableTypes.TryGetValue(tableBaseType, out TableParameter result))
            {
                result = new TableParameter();

                // use reflection to get the user defined table type name
                var tableParameterAttribute = (TableParameterAttribute)(tableBaseType.GetCustomAttributes(typeof(TableParameterAttribute), true)
                    .FirstOrDefault());
                result.HasTableParameter = tableParameterAttribute != null;
                result.TypeName = tableParameterAttribute?.TypeName;

                // use reflection to get (and order) the list of columns
                result.Columns = tableBaseType.GetProperties()
                    .Select(p => new
                    {
                        prop = p,
                        attr = (TableParameterColumnAttribute)p.GetCustomAttributes(typeof(TableParameterColumnAttribute), true).FirstOrDefault(),
                    })
                    .Where(t => t.attr != null)
                    .Select(t => new TableColumn
                    {
                        attr = t.attr,
                        prop = t.prop,
                        GetValue = (o) => t.prop.GetValue(o, null),
                    })
                    .ToList();

                // add to cache
                _mappedTableTypes[tableBaseType] = result;
            }

            return result;
        }

        #endregion

    }
}
