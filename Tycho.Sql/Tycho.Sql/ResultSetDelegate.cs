﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tycho.Sql
{
    /// <summary>
    /// Internal mapping of a result set ordinal to a delegate handler.
    /// </summary>
    internal class ResultSetDelegate
    {
        internal int ResultSet;
        internal Delegate Delegate;

        public ResultSetDelegate(int ResultSet, Delegate Delegate)
        {
            this.ResultSet = ResultSet;
            this.Delegate = Delegate;
        }
    }
}
