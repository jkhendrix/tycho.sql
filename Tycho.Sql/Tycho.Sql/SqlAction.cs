﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Tycho.Sql.Interfaces;
using Tycho.Sql.Output;

namespace Tycho.Sql
{
    /// <summary>
    /// A Fluent style configuration and execution engine initiated by a Sql object.
    /// </summary>
    /// <seealso cref="Tycho.Sql.Interfaces.ISqlActionConfiguring" />
    /// <seealso cref="Tycho.Sql.Interfaces.ISqlActionConfigured" />
    /// <seealso cref="Tycho.Sql.Sql.Procedure(string, string)" />
    /// <seealso cref="Tycho.Sql.Sql.Statement(string)" />
    /// <remarks>
    ///     <para>
    ///         A SqlAction initiated from a Tycho Sql object instance by calling either <see cref="Sql.Statement(string)"/> or <see cref="Sql.Procedure(string, string)"/>.  It begins as an <see cref="ISqlActionConfiguring"/> and evolves to a <see cref="ISqlActionConfigured"/> as it is used.  
    ///     </para>
    ///     <para>
    ///         <see cref="ISqlActionConfiguring"/> defines the following fluent steps:
    ///         <list type="definition">
    ///             <item>
    ///                 <term><see cref="SetTimeout(int)">SetTimeout</see></term>
    ///                 <description>Used to override the default (configured in the Sql object) for this call.</description>
    ///             </item>
    ///             <item>
    ///                 <term><see cref="OnException(Func{Exception, bool})">OnException</see></term>
    ///                 <description>Used to override the default (configured in the Sql object) exception handler for this call.  Overloads are available to support definition of an async handler.</description>
    ///             </item>
    ///             <item>
    ///                 <term><see cref="OnBeforeCommandExecute(Action{SqlCommand})">OnBeforeCommandExecute</see></term>
    ///                 <description>Allows you to tap into the execution pipeline to examine the raw SqlCommand object while it is being executed.  While it cannot be prevented, we STRONGLY discourage modification of the SqlCommand object.  It is provided to you in an asynchronous context while it is actively being used to execute your command in the database.  Overloads are available to support definition of an async handler.</description>
    ///             </item>
    ///         </list>
    ///     </para>
    ///     <para>
    ///         <see cref="ISqlActionConfigured"/> defines the following fluent steps:
    ///         <list type="definition">
    ///             <item>
    ///                 <term><see cref="AddParameter(string, object, SqlDbType)">AddParameter</see></term>
    ///                 <description>Add a parameter to your Sql call.</description>
    ///             </item>
    ///             <item>
    ///                 <term><see cref="AddOutputParameter{T}(string, SqlDbType, Action{T}, T)">AddOutputParameter</see></term>
    ///                 <description>Add an OUTPUT parameter to your Sql call.  The output value will be consumed by a custom Action&lt;T&gt; allowing you to easily use it in your application context.</description>
    ///             </item>
    ///             <item>
    ///                 <term><see cref="AddTableParameter{T}(string, IEnumerable{T})">AddTableParameter</see></term>
    ///                 <description>Add a Table Parameter to your Sql call.  While deceptively simple looking, this is really one of the most powerful features available in Tycho Sql.</description>
    ///             </item>
    ///             <item>
    ///                 <term><see cref="Execute">Execute</see>, <see cref="ExecuteAsync">ExecuteAsync</see></term>
    ///                 <description>Execute a Sql call returning the number of records affected.</description>
    ///             </item>
    ///             <item>
    ///                 <term><see cref="ExecuteScalar{T}">ExecuteScalar</see>, <see cref="ExecuteScalarAsync{T}">ExecuteScalarAsync</see></term>
    ///                 <description>Execute a Sql call returning the value from the 1st column of the 1st record.</description>
    ///             </item>
    ///             <item>
    ///                 <term><see cref="ExecuteReader(Action{SqlReader})">ExecuteReader</see>, <see cref="ExecuteReaderAsync(Action{SqlReader})">ExecuteReaderAsync</see></term>
    ///                 <description>Execute a Sql call and return data to your application.  You have MANY overloads to help customize this to your needs.</description>
    ///             </item>
    ///         </list>
    ///         Notice that the Execute methods all provide async versions?  Under the hood, Tycho Sql has 2 pipelines defined for Sql command execution, 1 fully synchronous, and 1 fully asynchronous.  The decision of which to use is made by this final fluent call to one of the available Execute methods.
    ///     </para>
    /// </remarks>
    public class SqlAction : ISqlActionConfiguring, ISqlActionConfigured
    {
        internal Sql _sql;
        internal string _database = null;
        internal string _schema = null;
        internal string _text;
        internal bool _isProcedure;
        internal int? _timeout = null;
        internal Func<Exception, Task<bool>> _exceptionHandler = null;
        internal Func<SqlCommand, Task> _beforeCommandExecuteHandler = null;

        private readonly List<SqlParameter> _parameters = new List<SqlParameter>();
        private readonly Dictionary<string, Action<object>> _outputCallbacks = new Dictionary<string, Action<object>>();

        internal SqlAction() { }

        /// <summary>
        /// Sets the command timeout for the Sql Action.  If unspecified, the default timeout is 30 seconds.  A value of 0 represents infinite wait time.
        /// </summary>
        /// <param name="Timeout">The timeout in seconds.</param>
        /// <returns>A fluent API driven ISqlActionConfiguring object.</returns>
        /// <exception cref="System.ArgumentException">Argument Timeout cannot be &lt; 0.</exception>
        public ISqlActionConfiguring SetTimeout(int Timeout)
        {
            if (Timeout < 0)
                throw new ArgumentException("Argument Timeout cannot be < 0.", "Timeout");

            _timeout = Timeout;
            return this;
        }

        /// <summary>
        /// Hook to define a synchronous exception handler.  If the exception handler returns TRUE, the sql action is re-executed.
        /// </summary>
        /// <param name="Handler">The handler.</param>
        /// <returns>A fluent API driven ISqlActionConfiguring object.</returns>
        /// <remarks>
        /// A Sql Action can have only one configured exception handler.  Calling this method multiple times will replace the previously defined exception handler.  If undefined, exceptions will bubble up to be handled by your application.
        /// </remarks>
        public ISqlActionConfiguring OnException(Func<Exception, bool> Handler)
        {
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
            _exceptionHandler = async (ex) => Handler(ex);
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
            return this;
        }

        /// <summary>
        /// Hook to define an asynchronous exception handler.  If the exception handler returns TRUE, the sql action is re-executed.
        /// </summary>
        /// <param name="Handler">The handler.</param>
        /// <returns>A fluent API driven ISqlActionConfiguring object.</returns>
        /// <remarks>
        /// A Sql Action can have only one configured exception handler.  Calling this method multiple times will replace the previously defined exception handler.  If undefined, exceptions will bubble up to be handled by your application.
        /// </remarks>
        public ISqlActionConfiguring OnException(Func<Exception, Task<bool>> Handler)
        {
            _exceptionHandler = Handler;
            return this;
        }

        /// <summary>
        /// Action to be called immediately before executing the underlying SqlCommand.
        /// </summary>
        /// <param name="Handler">The handler.</param>
        /// <returns>A fluent API driven ISqlActionConfiguring object.</returns>
        /// <remarks>Note that this handler is called withing an async task, so it may execute in a separate thread.</remarks>
        public ISqlActionConfiguring OnBeforeCommandExecute(Action<SqlCommand> Handler)
        {
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
            _beforeCommandExecuteHandler = async (s) => Handler(s);
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
            return this;
        }

        /// <summary>
        /// Asynchronous handler to be called immediately before executing the underlying SqlCommand.
        /// </summary>
        /// <param name="Handler">The handler.</param>
        /// <returns>A fluent API driven ISqlActionConfiguring object.</returns>
        public ISqlActionConfiguring OnBeforeCommandExecute(Func<SqlCommand, Task> Handler)
        {
            _beforeCommandExecuteHandler = Handler;
            return this;
        }

        /// <summary>
        /// Adds a parameter to the Sql Action.
        /// </summary>
        /// <param name="Name">The name.</param>
        /// <param name="Value">The value.</param>
        /// <param name="Type">The type.</param>
        /// <returns>A fluent API driven ISqlConfigured object.</returns>
        /// <exception cref="System.ArgumentException">Argument Name cannot be null or only whitespace. - Name</exception>
        public ISqlActionConfigured AddParameter(string Name, object Value, SqlDbType Type)
        {
            if (string.IsNullOrWhiteSpace(Name))
                throw new ArgumentException("Argument Name cannot be null or only whitespace.", "Name");

            _parameters.Add(new SqlParameter
            {
                ParameterName = Name,
                Value = Value ?? DBNull.Value,
                SqlDbType = Type,
            });
            return this;
        }

        /// <summary>
        /// Adds an INPUT/OUTPUT parameter to a Sql call.
        /// </summary>
        /// <typeparam name="T">The type of the output parameter.</typeparam>
        /// <param name="Name">The name of the parameter.</param>
        /// <param name="Type">The SqlDbType.</param>
        /// <param name="OutputDelegate">An Action delegate that will be called directly with the output value.</param>
        /// <returns>Fluent ISqlActionConfigured.</returns>
        public ISqlActionConfigured AddOutputParameter<T>(string Name, SqlDbType Type, Action<T> OutputDelegate)
            => AddOutputParameter(Name, Type, OutputDelegate, default);

        /// <summary>
        /// Adds an output parameter to the Sql Action.
        /// </summary>
        /// <typeparam name="T">Generic type of output variable.</typeparam>
        /// <param name="Name">The name.</param>
        /// <param name="Type">The type.</param>
        /// <param name="OutputDelegate">A delegate Action that receives the output value.</param>
        /// <param name="InitialValue">The initial value of the parameter that is passed into the Sql command.</param>
        /// <returns>A fluent API driven ISqlConfigured object.</returns>
        /// <exception cref="System.ArgumentException">Argument Name cannot be null or only whitespace.</exception>
        public ISqlActionConfigured AddOutputParameter<T>(string Name, SqlDbType Type, Action<T> OutputDelegate, T InitialValue)
        {
            if (string.IsNullOrWhiteSpace(Name))
                throw new ArgumentException("Argument Name cannot be null or only whitespace.", "Name");

            _parameters.Add(new SqlParameter
            {
                ParameterName = Name,
                Value = (InitialValue as object) ?? DBNull.Value,
                SqlDbType = Type,
                Direction = ParameterDirection.InputOutput,
            });

            _outputCallbacks[Name] = (value) =>
            {
                T convertedValue = value is DBNull 
                    ? default 
                    : (T)Convert.ChangeType(value, Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T));
                OutputDelegate(convertedValue);
            };

            return this;
        }

        /// <summary>
        /// Adds a sized INPUT/OUTPUT parameter to a Sql call.  This is usually used for CHAR/VARCHAR parameters.
        /// </summary>
        /// <typeparam name="T">The type of the output parameter.</typeparam>
        /// <param name="Name">The name of the parameter.</param>
        /// <param name="Type">The SqlDbType.</param>
        /// <param name="Size">The size of the parameter, e.g.: VARCHAR(20) = 20.</param>
        /// <param name="OutputDelegate">An Action delegate that will be called directly with the output value.</param>
        /// <returns>
        /// Fluent ISqlActionConfigured.
        /// </returns>
        public ISqlActionConfigured AddOutputParameter<T>(string Name, SqlDbType Type, int Size, Action<T> OutputDelegate)
            => AddOutputParameter(Name, Type, Size, OutputDelegate, default);

        /// <summary>
        /// Adds an output parameter to the Sql Action.
        /// </summary>
        /// <typeparam name="T">Generic type of output variable.</typeparam>
        /// <param name="Name">The name.</param>
        /// <param name="Type">The type.</param>
        /// <param name="Size">The size.</param>
        /// <param name="OutputDelegate">The output callback.</param>
        /// <param name="InitialValue">The initial value of the parameter that is passed into the Sql command.</param>
        /// <returns>A fluent API driven ISqlConfigured object.</returns>
        /// <exception cref="System.ArgumentException">Argument Name cannot be null or only whitespace.</exception>
        public ISqlActionConfigured AddOutputParameter<T>(string Name, SqlDbType Type, int Size, Action<T> OutputDelegate, T InitialValue)
        {
            if (string.IsNullOrWhiteSpace(Name))
                throw new ArgumentException("Argument Name cannot be null or only whitespace.", "Name");

            _parameters.Add(new SqlParameter
            {
                ParameterName = Name,
                Value = (InitialValue as object) ?? DBNull.Value,
                SqlDbType = Type,
                Direction = ParameterDirection.InputOutput,
                Size = Size,
            });

            _outputCallbacks[Name] = (value) =>
            {
                T convertedValue = value is DBNull
                    ? default
                    : (T)Convert.ChangeType(value, Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T));
                OutputDelegate(convertedValue);
            };

            return this;
        }

        /// <summary>
        /// Adds a precise numeric INPUT/OUTPUT parameter to a Sql Call.  This is useful for passing very specific floating point values both IN and OUT of the DB.
        /// </summary>
        /// <typeparam name="T">The type of the output parameter.</typeparam>
        /// <param name="Name">The name of the parameter.</param>
        /// <param name="Type">The SqlDbType.</param>
        /// <param name="Precision">The precision of the floating point number.</param>
        /// <param name="Scale">The scale of the floating point number.</param>
        /// <param name="OutputDelegate">An Action delegate that will be called directly with the output value.</param>
        /// <returns>
        /// Fluent ISqlActionConfigured.
        /// </returns>
        public ISqlActionConfigured AddOutputParameter<T>(string Name, SqlDbType Type, byte Precision, byte Scale, Action<T> OutputDelegate)
            => AddOutputParameter(Name, Type, Precision, Scale, OutputDelegate, default);

        /// <summary>
        /// Adds an output parameter to the Sql Action.
        /// </summary>
        /// <typeparam name="T">Generic type of output variable.</typeparam>
        /// <param name="Name">The name.</param>
        /// <param name="Type">The type.</param>
        /// <param name="Precision">The precision.</param>
        /// <param name="Scale">The scale.</param>
        /// <param name="OutputDelegate">The output callback.</param>
        /// <param name="InitialValue">The initial value of the parameter that is passed into the Sql command.</param>
        /// <returns>A fluent API driven ISqlConfigured object.</returns>
        /// <exception cref="System.ArgumentException">Argument Name cannot be null or only whitespace.</exception>
        public ISqlActionConfigured AddOutputParameter<T>(string Name, SqlDbType Type, byte Precision, byte Scale, Action<T> OutputDelegate, T InitialValue)
        {
            if (string.IsNullOrWhiteSpace(Name))
                throw new ArgumentException("Argument Name cannot be null or only whitespace.", "Name");

            _parameters.Add(new SqlParameter
            {
                ParameterName = Name,
                Value = (InitialValue as object) ?? DBNull.Value,
                SqlDbType = Type,
                Direction = ParameterDirection.InputOutput,
                Precision = Precision,
                Scale = Scale,
            });

            _outputCallbacks[Name] = (value) =>
            {
                T convertedValue;
                if (value is DBNull)
                    convertedValue = default;
                else
                {
                    Type t = Nullable.GetUnderlyingType(typeof(T));
                    if (t == null)
                        convertedValue = (T)Convert.ChangeType(value, typeof(T));
                    else
                        convertedValue = (T)Convert.ChangeType(value, t);
                }
                OutputDelegate(convertedValue);
            };

            return this;
        }

        /// <summary>
        /// Adds a table parameter to the Sql Action.
        /// </summary>
        /// <typeparam name="T">A DTO which has been decorated with [TableParameter] and [TableParameterColumn] attributes.</typeparam>
        /// <param name="Name">The name.</param>
        /// <param name="TableData">The table data.</param>
        /// <returns>A fluent API driven ISqlConfigured object.</returns>
        /// <exception cref="System.ArgumentException">
        /// Argument Name cannot be null or only whitespace. - Name
        /// or
        /// Argument TableType cannot be null or only whitespace. - TableType
        /// or
        /// Type of underlying TableData contains no properties decorated with the [TableParameterColumn] attribute. - TableData
        /// </exception>
        /// <exception cref="System.ArgumentNullException">TableData - Argument TableData cannot be null.</exception>
        /// <exception cref="TableValueNullException"></exception>
        public ISqlActionConfigured AddTableParameter<T>(string Name, IEnumerable<T> TableData) where T : class
        {
            if (string.IsNullOrWhiteSpace(Name))
                throw new ArgumentException("Argument Name cannot be null or only whitespace.", "Name");
            if (TableData == null)
                throw new ArgumentNullException("TableData", "Argument TableData cannot be null.");

            // get map of the table columns from the underlying object type
            var tableDefinition = _sql.GetTableParameterDefinition<T>();
            if (!tableDefinition.HasTableParameter)
                throw new ArgumentException("Class definition for TableData generic type is missing the TableParameterAttribute.");
            if (string.IsNullOrWhiteSpace(tableDefinition.TypeName))
                throw new ArgumentException("TableParameterAttribute for TableData generic type cannot define a null or whitespace TypeName.");
            if (!tableDefinition.Columns.Any())
                throw new ArgumentException("Class definition for TableData generic type is missing properties decorated with TableParameterColumnAttribute.");

            // build DataTable
            var dataTable = new DataTable();
            foreach (var c in tableDefinition.Columns)
            {
                // there are 3 possibilities there
                // 1) the column is an actual value-type
                // 2) the column is a Nullable value-type
                // 3) the column is an object type (like a string)

                // get the underlying type
                Type underlyingType = Nullable.GetUnderlyingType(c.prop.PropertyType);

                // determine if the column type is nullable
                bool typeIsNullable = !c.prop.PropertyType.IsValueType || underlyingType != null;

                // determine the column type
                Type columnType = underlyingType ?? c.prop.PropertyType;

                // add column to data table
                var col = dataTable.Columns.Add(c.attr.ColumnName, columnType);

                // set column attributes
                if (typeIsNullable)
                {
                    col.AllowDBNull = c.attr.IsNullable;
                    col.DefaultValue = col.AllowDBNull ? null : new object();
                }
                else
                {
                    col.AllowDBNull = false;
                    col.DefaultValue = Activator.CreateInstance(c.prop.PropertyType);
                }
            }

            // add data to table
            if (TableData.Count() > 0)
            {
                var columnCount = tableDefinition.Columns.Count();
                foreach (var row in TableData)
                {
                    DataRow newRow = dataTable.NewRow();

                    for (int x = 0; x < tableDefinition.Columns.Count; x++) 
                    {
                        var col = tableDefinition.Columns[x];
                        var value = col.GetValue(row);
                        var valueIsNull = !col.prop.PropertyType.IsValueType || Nullable.GetUnderlyingType(col.prop.PropertyType) != null
                            ? value == null
                            : false;

                        if (valueIsNull && !col.attr.IsNullable)
                            throw new TableValueNullException(string.Format("A value in TableData for a 'Not Null' column ({0}) has a null value.", col.prop.Name));

                        if (valueIsNull)
                            newRow[x] = DBNull.Value;
                        else if (col.prop.PropertyType == typeof(string))
                            newRow[x] = ((string)value).TruncateTo(col.attr.MaxLength);
                        else
                            newRow[x] = value;
                    }

                    dataTable.Rows.Add(newRow);
                }
            }

            // add to _parameters
            _parameters.Add(new SqlParameter
            {
                ParameterName = Name,
                Value = dataTable,
                SqlDbType = SqlDbType.Structured,
                TypeName = tableDefinition.TypeName,
            });

            return this;
        }

        private SqlCommand SqlCommand(SqlConnection connection)
        {
            string commandText;
            CommandType commandType;

            // determine command text & type
            if (_isProcedure)
            {
                commandText = string.Format("[{0}].[{1}]", _schema.Trim('[',']'), _text.Trim('[', ']'));
                commandType = CommandType.StoredProcedure;
            }
            else
            {
                commandText = _text;
                commandType = CommandType.Text;
            }

            // create result
            SqlCommand result = new SqlCommand(commandText, connection)
            {
                CommandType = commandType,
                CommandTimeout = _timeout ?? _sql.DefaultTimeout,
            };

            // add parameters
            if (_parameters.Any())
            {
                // due to the retry capability, we MUST duplicate these parameters at execution time
                result.Parameters.AddRange(
                    _parameters.Select(p => new SqlParameter
                    {
                        ParameterName = p.ParameterName,
                        Value = p.Value,
                        SqlDbType = p.SqlDbType,
                        Direction = p.Direction,
                        TypeName = p.TypeName,
                        Size = p.Size,
                        Precision = p.Precision,
                        Scale = p.Scale,
                    })
                    .ToArray());
            }

            return result;
        }

        #region Synchronous Privates

        private T CommandWrapper<T>(Func<SqlCommand, T> Action)
        {
            return ConnectionWrapper(connection =>
            {
                using (var command = SqlCommand(connection))
                {
                    // start any before-execution task that was defined
                    var beforeExecutionTask = _beforeCommandExecuteHandler?.Invoke(command);

                    // open the db connection if it was not already open
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    // execution action
                    var result = Action(command);

                    // execute any callbacks defined for your action (output parameters)
                    foreach (var callback in _outputCallbacks)
                    {
                        callback.Value(command.Parameters[callback.Key].Value);
                    }

                    // wait for any before-execution task to complete
                    beforeExecutionTask?.Wait();

                    return result;
                }
            });
        }

        private T ConnectionWrapper<T>(Func<SqlConnection, T> Delegate)
        {
            // there are 2 ways out of this endless loop:
            //  1) return on a successful Action()
            //  2) throw if the exception handler returns false
            while (true)
            {
                try
                {
                    using (var connection = new SqlConnection(_sql.ConnectionString))
                    {
                        return Delegate(connection);
                    }
                }
                catch (Exception ex)
                {
                    var exHandler = _exceptionHandler ?? _sql.DefaultExceptionHandler;
                    var t = exHandler(ex);
                    t.Wait();
                    if (!t.Result)
                        throw ex;
                }
            }
        }

        #endregion

        #region Asynchronous Privates

        private async Task<T> CommandWrapperAsync<T>(Func<SqlCommand, Task<T>> Action)
        {
            return await ConnectionWrapperAsync(async connection =>
            {
                using (var command = SqlCommand(connection))
                {
                    // start any before-execution task that was defined
                    var beforeExecutionTask = _beforeCommandExecuteHandler?.Invoke(command);

                    // open the db connection if it was not already open
                    if (connection.State == ConnectionState.Closed)
                        await connection.OpenAsync();

                    // execution action
                    var result = await Action(command);

                    // execute any callbacks defined for your action (output parameters)
                    foreach (var callback in _outputCallbacks)
                    {
                        callback.Value(command.Parameters[callback.Key].Value);
                    }

                    // wait for any before-execution task to complete
                    if (beforeExecutionTask != null)
                        await beforeExecutionTask;

                    return result;
                }
            });
        }

        private async Task<T> ConnectionWrapperAsync<T>(Func<SqlConnection, Task<T>> Action)
        {
            // there are 2 ways out of this endless loop:
            //  1) return on a successful Action()
            //  2) throw if the exception handler returns false
            while (true)
            {
                try
                {
                    using (var connection = new SqlConnection(_sql.ConnectionString))
                    {
                        return await Action(connection);
                    }
                }
                catch (Exception ex)
                {
                    var exHandler = _exceptionHandler ?? _sql.DefaultExceptionHandler;
                    if (!(await exHandler(ex)))
                        throw ex;
                }
            }
        }

        #endregion

        #region Synchronous Execution

        private Dictionary<int, IEnumerable<object>> Read(params ResultSetDelegate[] delegates)
        {
            return CommandWrapper(command =>
            {
                var result = new Dictionary<int, IEnumerable<object>>();

                // execute the command returning a SqlReader to process
                using (var reader = new SqlReader(command.ExecuteReader()))
                {
                    // build a queue out of the delegate list
                    var delegateQueue = new Queue<ResultSetDelegate>(delegates);

                    do
                    {
                        // Early terminate if we are beyond the defined result set delegates.  This can happen if the defined sql returned more data sets than the application was prepared for.
                        if (!delegateQueue.Any()) break;

                        // reset column definitions for this result set
                        reader.ResetColumns();

                        // pop a delegate off the queue
                        var @delegate = delegateQueue.Dequeue();
                        var @items = new List<object>();
                        result[@delegate.ResultSet] = @items;

                        // read all the records in the result set
                        while (reader.Read())
                        {
                            // add delegate output to result set
                            items.Add(@delegate.Delegate.Method.Invoke(@delegate.Delegate.Target, new object[] { reader }));
                        }

                        // move to next result set
                    } while (reader.NextResult());
                }

                return result;
            });
        }

        /// <summary>
        /// Simple Sql action execution returning the number of records affected.
        /// </summary>
        /// <returns>
        /// Integer representing the number of records affected.
        /// </returns>
        /// <remarks>
        /// The return value simply reports what Sql returns.  If your Sql sets NOCOUNT on, it will return 0.
        /// </remarks>
        public int Execute()
        {
            return CommandWrapper(command => command.ExecuteNonQuery());
        }

        /// <summary>
        /// Executes the Sql action returning the value from the first column of the first row of the first result set.
        /// </summary>
        /// <typeparam name="T">The type of the returned value.</typeparam>
        /// <returns>
        /// The value from the first column of the first row of the first result set.
        /// </returns>
        public T ExecuteScalar<T>()
        {
            return CommandWrapper(command => (T)command.ExecuteScalar());
        }

        /// <summary>
        /// Executes the Sql action applying the specified Func Delegate to the first result set.
        /// </summary>
        /// <typeparam name="T">Object type returned by the Func Delegate.</typeparam>
        /// <param name="Delegate">The Func Delegate to apply to records from the result set.</param>
        /// <returns>
        /// An IEnumerable of output records.
        /// </returns>
        public IEnumerable<T> ExecuteReader<T>(Func<SqlReader, T> Delegate)
        {
            var result = Read(new ResultSetDelegate(1, Delegate));
            return result[1].Cast<T>();
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public MultipleResultSets<T1, T2> ExecuteReader<T1, T2>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2)
        {
            var result = Read(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2)
            );
            return new MultipleResultSets<T1, T2>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public MultipleResultSets<T1, T2, T3> ExecuteReader<T1, T2, T3>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3)
        {
            var result = Read(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3)
            );
            return new MultipleResultSets<T1, T2, T3>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public MultipleResultSets<T1, T2, T3, T4> ExecuteReader<T1, T2, T3, T4>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4)
        {
            var result = Read(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4)
            );
            return new MultipleResultSets<T1, T2, T3, T4>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
                Set4 = result[4].Cast<T4>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public MultipleResultSets<T1, T2, T3, T4, T5> ExecuteReader<T1, T2, T3, T4, T5>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5)
        {
            var result = Read(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4),
                new ResultSetDelegate(5, Delegate5)
            );
            return new MultipleResultSets<T1, T2, T3, T4, T5>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
                Set4 = result[4].Cast<T4>(),
                Set5 = result[5].Cast<T5>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public MultipleResultSets<T1, T2, T3, T4, T5, T6> ExecuteReader<T1, T2, T3, T4, T5, T6>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6)
        {
            var result = Read(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4),
                new ResultSetDelegate(5, Delegate5),
                new ResultSetDelegate(6, Delegate6)
            );
            return new MultipleResultSets<T1, T2, T3, T4, T5, T6>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
                Set4 = result[4].Cast<T4>(),
                Set5 = result[5].Cast<T5>(),
                Set6 = result[6].Cast<T6>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public MultipleResultSets<T1, T2, T3, T4, T5, T6, T7> ExecuteReader<T1, T2, T3, T4, T5, T6, T7>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7)
        {
            var result = Read(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4),
                new ResultSetDelegate(5, Delegate5),
                new ResultSetDelegate(6, Delegate6),
                new ResultSetDelegate(7, Delegate7)
            );
            return new MultipleResultSets<T1, T2, T3, T4, T5, T6, T7>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
                Set4 = result[4].Cast<T4>(),
                Set5 = result[5].Cast<T5>(),
                Set6 = result[6].Cast<T6>(),
                Set7 = result[7].Cast<T7>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <param name="Delegate8">The Func Delegate to apply to records from the 8th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8> ExecuteReader<T1, T2, T3, T4, T5, T6, T7, T8>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7,
            Func<SqlReader, T8> Delegate8)
        {
            var result = Read(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4),
                new ResultSetDelegate(5, Delegate5),
                new ResultSetDelegate(6, Delegate6),
                new ResultSetDelegate(7, Delegate7),
                new ResultSetDelegate(8, Delegate8)
            );
            return new MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
                Set4 = result[4].Cast<T4>(),
                Set5 = result[5].Cast<T5>(),
                Set6 = result[6].Cast<T6>(),
                Set7 = result[7].Cast<T7>(),
                Set8 = result[8].Cast<T8>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
        /// <typeparam name="T9">Object type returned by the 9th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <param name="Delegate8">The Func Delegate to apply to records from the 8th result set.</param>
        /// <param name="Delegate9">The Func Delegate to apply to records from the 9th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9> ExecuteReader<T1, T2, T3, T4, T5, T6, T7, T8, T9>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7,
            Func<SqlReader, T8> Delegate8,
            Func<SqlReader, T9> Delegate9)
        {
            var result = Read(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4),
                new ResultSetDelegate(5, Delegate5),
                new ResultSetDelegate(6, Delegate6),
                new ResultSetDelegate(7, Delegate7),
                new ResultSetDelegate(8, Delegate8),
                new ResultSetDelegate(9, Delegate9)
            );
            return new MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
                Set4 = result[4].Cast<T4>(),
                Set5 = result[5].Cast<T5>(),
                Set6 = result[6].Cast<T6>(),
                Set7 = result[7].Cast<T7>(),
                Set8 = result[8].Cast<T8>(),
                Set9 = result[9].Cast<T9>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
        /// <typeparam name="T9">Object type returned by the 9th Func Delegate.</typeparam>
        /// <typeparam name="T10">Object type returned by the 10th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <param name="Delegate8">The Func Delegate to apply to records from the 8th result set.</param>
        /// <param name="Delegate9">The Func Delegate to apply to records from the 9th result set.</param>
        /// <param name="Delegate10">The Func Delegate to apply to records from the 10th result set.</param>
        /// <returns>
        /// A MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> ExecuteReader<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7,
            Func<SqlReader, T8> Delegate8,
            Func<SqlReader, T9> Delegate9,
            Func<SqlReader, T10> Delegate10)
        {
            var result = Read(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4),
                new ResultSetDelegate(5, Delegate5),
                new ResultSetDelegate(6, Delegate6),
                new ResultSetDelegate(7, Delegate7),
                new ResultSetDelegate(8, Delegate8),
                new ResultSetDelegate(9, Delegate9),
                new ResultSetDelegate(10, Delegate10)
            );
            return new MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>
            {
                Set1  = result[1].Cast<T1>(),
                Set2  = result[2].Cast<T2>(),
                Set3  = result[3].Cast<T3>(),
                Set4  = result[4].Cast<T4>(),
                Set5  = result[5].Cast<T5>(),
                Set6  = result[6].Cast<T6>(),
                Set7  = result[7].Cast<T7>(),
                Set8  = result[8].Cast<T8>(),
                Set9  = result[9].Cast<T9>(),
                Set10 = result[10].Cast<T10>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying the specified Action Delegate to the first result set.
        /// </summary>
        /// <param name="Action">The Action Delegate to apply to records from the result set.</param>
        public void ExecuteReader(Action<SqlReader> Action)
        {
            Read(new ResultSetDelegate(1, Action));
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        public void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2)
        {
            Read(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(1, Action2)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        public void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3)
        {
            Read(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(1, Action2),
                new ResultSetDelegate(1, Action3)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        public void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4)
        {
            Read(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(1, Action2),
                new ResultSetDelegate(1, Action3),
                new ResultSetDelegate(1, Action4)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        public void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5)
        {
            Read(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(1, Action2),
                new ResultSetDelegate(1, Action3),
                new ResultSetDelegate(1, Action4),
                new ResultSetDelegate(1, Action5)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        public void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6)
        {
            Read(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(1, Action2),
                new ResultSetDelegate(1, Action3),
                new ResultSetDelegate(1, Action4),
                new ResultSetDelegate(1, Action5),
                new ResultSetDelegate(1, Action6)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        public void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7)
        {
            Read(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(1, Action2),
                new ResultSetDelegate(1, Action3),
                new ResultSetDelegate(1, Action4),
                new ResultSetDelegate(1, Action5),
                new ResultSetDelegate(1, Action6),
                new ResultSetDelegate(1, Action7)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <param name="Action8">The Action Delegate to apply to records from the 8th result set.</param>
        public void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7,
            Action<SqlReader> Action8)
        {
            Read(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(1, Action2),
                new ResultSetDelegate(1, Action3),
                new ResultSetDelegate(1, Action4),
                new ResultSetDelegate(1, Action5),
                new ResultSetDelegate(1, Action6),
                new ResultSetDelegate(1, Action7),
                new ResultSetDelegate(1, Action8)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <param name="Action8">The Action Delegate to apply to records from the 8th result set.</param>
        /// <param name="Action9">The Action Delegate to apply to records from the 9th result set.</param>
        public void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7,
            Action<SqlReader> Action8,
            Action<SqlReader> Action9)
        {
            Read(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(1, Action2),
                new ResultSetDelegate(1, Action3),
                new ResultSetDelegate(1, Action4),
                new ResultSetDelegate(1, Action5),
                new ResultSetDelegate(1, Action6),
                new ResultSetDelegate(1, Action7),
                new ResultSetDelegate(1, Action8),
                new ResultSetDelegate(1, Action9)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <param name="Action8">The Action Delegate to apply to records from the 8th result set.</param>
        /// <param name="Action9">The Action Delegate to apply to records from the 9th result set.</param>
        /// <param name="Action10">The Action Delegate to apply to records from the 10th result set.</param>
        public void ExecuteReader(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7,
            Action<SqlReader> Action8,
            Action<SqlReader> Action9,
            Action<SqlReader> Action10)
        {
            Read(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(1, Action2),
                new ResultSetDelegate(1, Action3),
                new ResultSetDelegate(1, Action4),
                new ResultSetDelegate(1, Action5),
                new ResultSetDelegate(1, Action6),
                new ResultSetDelegate(1, Action7),
                new ResultSetDelegate(1, Action8),
                new ResultSetDelegate(1, Action9),
                new ResultSetDelegate(1, Action10)
            );
        }

        #endregion

        #region Asynchronous Execution

        private async Task<Dictionary<int, IEnumerable<object>>> ReadAsync(params ResultSetDelegate[] delegates)
        {
            return await CommandWrapperAsync(async command =>
            {
                var result = new Dictionary<int, IEnumerable<object>>();

                // execute the command returning a SqlReader to process
                using (var reader = new SqlReader(await command.ExecuteReaderAsync()))
                {
                    // build a queue out of the delegate list
                    var delegateQueue = new Queue<ResultSetDelegate>(delegates);

                    do
                    {
                        // Early terminate if we are beyond the defined result set delegates.  This can happen if the defined sql returned more data sets than the application was prepared for.
                        if (!delegateQueue.Any()) break;

                        // reset column definitions for this result set
                        reader.ResetColumns();

                        // pop a delegate off the queue
                        var @delegate = delegateQueue.Dequeue();
                        var @items = new List<object>();
                        result[@delegate.ResultSet] = @items;

                        // read all the records in the result set
                        while (await reader.ReadAsync())
                        {
                            // add delegate output to result set
                            items.Add(@delegate.Delegate.Method.Invoke(@delegate.Delegate.Target, new object[] { reader }));
                        }

                        // move to next result set
                    } while (await reader.NextResultAsync());
                }

                return result;
            });
        }

        /// <summary>
        /// Simple Sql action execution returning the number of records affected.
        /// </summary>
        /// <returns>
        /// Awaitable Task returning the number of records affected.
        /// </returns>
        /// <remarks>
        /// The return value simply reports what Sql returns.  If your Sql sets NOCOUNT on, it will return 0.
        /// </remarks>
        public async Task<int> ExecuteAsync()
        {
            return await CommandWrapperAsync(command => command.ExecuteNonQueryAsync());
        }

        /// <summary>
        /// Executes the Sql action returning the value from the first column of the first row of the first result set.
        /// </summary>
        /// <typeparam name="T">The type of the returned value.</typeparam>
        /// <returns>
        /// Awaitable Task returning the value from the first column of the first row of the first result set.
        /// </returns>
        public async Task<T> ExecuteScalarAsync<T>()
        {
            return await CommandWrapperAsync(async command => (T)await command.ExecuteScalarAsync());
        }

        /// <summary>
        /// Executes the Sql action applying the specified Func Delegate to the first result set.
        /// </summary>
        /// <typeparam name="T">Object type returned by the Func Delegate.</typeparam>
        /// <param name="Delegate">The Func Delegate to apply to records from the result set.</param>
        /// <returns>
        /// Awaitable Task returning an IEnumerable of output records.
        /// </returns>
        public async Task<IEnumerable<T>> ExecuteReaderAsync<T>(Func<SqlReader, T> Delegate)
        {
            var result = await ReadAsync(
                new ResultSetDelegate(1, Delegate)
            );
            return result[1].Cast<T>();
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public async Task<MultipleResultSets<T1, T2>> ExecuteReaderAsync<T1, T2>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2)
        {
            var result = await ReadAsync(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2)
            );
            return new MultipleResultSets<T1, T2>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public async Task<MultipleResultSets<T1, T2, T3>> ExecuteReaderAsync<T1, T2, T3>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3)
        {
            var result = await ReadAsync(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3)
            );
            return new MultipleResultSets<T1, T2, T3>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public async Task<MultipleResultSets<T1, T2, T3, T4>> ExecuteReaderAsync<T1, T2, T3, T4>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4)
        {
            var result = await ReadAsync(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4)
            );
            return new MultipleResultSets<T1, T2, T3, T4>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
                Set4 = result[4].Cast<T4>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public async Task<MultipleResultSets<T1, T2, T3, T4, T5>> ExecuteReaderAsync<T1, T2, T3, T4, T5>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5)
        {
            var result = await ReadAsync(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4),
                new ResultSetDelegate(5, Delegate5)
            );
            return new MultipleResultSets<T1, T2, T3, T4, T5>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
                Set4 = result[4].Cast<T4>(),
                Set5 = result[5].Cast<T5>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public async Task<MultipleResultSets<T1, T2, T3, T4, T5, T6>> ExecuteReaderAsync<T1, T2, T3, T4, T5, T6>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6)
        {
            var result = await ReadAsync(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4),
                new ResultSetDelegate(5, Delegate5),
                new ResultSetDelegate(6, Delegate6)
            );
            return new MultipleResultSets<T1, T2, T3, T4, T5, T6>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
                Set4 = result[4].Cast<T4>(),
                Set5 = result[5].Cast<T5>(),
                Set6 = result[6].Cast<T6>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public async Task<MultipleResultSets<T1, T2, T3, T4, T5, T6, T7>> ExecuteReaderAsync<T1, T2, T3, T4, T5, T6, T7>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7)
        {
            var result = await ReadAsync(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4),
                new ResultSetDelegate(5, Delegate5),
                new ResultSetDelegate(6, Delegate6),
                new ResultSetDelegate(7, Delegate7)
            );
            return new MultipleResultSets<T1, T2, T3, T4, T5, T6, T7>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
                Set4 = result[4].Cast<T4>(),
                Set5 = result[5].Cast<T5>(),
                Set6 = result[6].Cast<T6>(),
                Set7 = result[7].Cast<T7>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <param name="Delegate8">The Func Delegate to apply to records from the 8th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public async Task<MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8>> ExecuteReaderAsync<T1, T2, T3, T4, T5, T6, T7, T8>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7,
            Func<SqlReader, T8> Delegate8)
        {
            var result = await ReadAsync(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4),
                new ResultSetDelegate(5, Delegate5),
                new ResultSetDelegate(6, Delegate6),
                new ResultSetDelegate(7, Delegate7),
                new ResultSetDelegate(8, Delegate8)
            );
            return new MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
                Set4 = result[4].Cast<T4>(),
                Set5 = result[5].Cast<T5>(),
                Set6 = result[6].Cast<T6>(),
                Set7 = result[7].Cast<T7>(),
                Set8 = result[8].Cast<T8>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
        /// <typeparam name="T9">Object type returned by the 9th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <param name="Delegate8">The Func Delegate to apply to records from the 8th result set.</param>
        /// <param name="Delegate9">The Func Delegate to apply to records from the 9th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public async Task<MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9>> ExecuteReaderAsync<T1, T2, T3, T4, T5, T6, T7, T8, T9>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7,
            Func<SqlReader, T8> Delegate8,
            Func<SqlReader, T9> Delegate9)
        {
            var result = await ReadAsync(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4),
                new ResultSetDelegate(5, Delegate5),
                new ResultSetDelegate(6, Delegate6),
                new ResultSetDelegate(7, Delegate7),
                new ResultSetDelegate(8, Delegate8),
                new ResultSetDelegate(9, Delegate9)
            );
            return new MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
                Set4 = result[4].Cast<T4>(),
                Set5 = result[5].Cast<T5>(),
                Set6 = result[6].Cast<T6>(),
                Set7 = result[7].Cast<T7>(),
                Set8 = result[8].Cast<T8>(),
                Set9 = result[9].Cast<T9>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying specified Func Delegates to MULTIPLE result sets.
        /// </summary>
        /// <typeparam name="T1">Object type returned by the 1st Func Delegate.</typeparam>
        /// <typeparam name="T2">Object type returned by the 2nd Func Delegate.</typeparam>
        /// <typeparam name="T3">Object type returned by the 3rd Func Delegate.</typeparam>
        /// <typeparam name="T4">Object type returned by the 4th Func Delegate.</typeparam>
        /// <typeparam name="T5">Object type returned by the 5th Func Delegate.</typeparam>
        /// <typeparam name="T6">Object type returned by the 6th Func Delegate.</typeparam>
        /// <typeparam name="T7">Object type returned by the 7th Func Delegate.</typeparam>
        /// <typeparam name="T8">Object type returned by the 8th Func Delegate.</typeparam>
        /// <typeparam name="T9">Object type returned by the 9th Func Delegate.</typeparam>
        /// <typeparam name="T10">Object type returned by the 10th Func Delegate.</typeparam>
        /// <param name="Delegate1">The Func Delegate to apply to records from the 1st result set.</param>
        /// <param name="Delegate2">The Func Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Delegate3">The Func Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Delegate4">The Func Delegate to apply to records from the 4th result set.</param>
        /// <param name="Delegate5">The Func Delegate to apply to records from the 5th result set.</param>
        /// <param name="Delegate6">The Func Delegate to apply to records from the 6th result set.</param>
        /// <param name="Delegate7">The Func Delegate to apply to records from the 7th result set.</param>
        /// <param name="Delegate8">The Func Delegate to apply to records from the 8th result set.</param>
        /// <param name="Delegate9">The Func Delegate to apply to records from the 9th result set.</param>
        /// <param name="Delegate10">The Func Delegate to apply to records from the 10th result set.</param>
        /// <returns>
        /// Awaitable Task returning a MultipleResulSets object allowing access to all IEnumerable result sets.
        /// </returns>
        public async Task<MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>> ExecuteReaderAsync<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(
            Func<SqlReader, T1> Delegate1,
            Func<SqlReader, T2> Delegate2,
            Func<SqlReader, T3> Delegate3,
            Func<SqlReader, T4> Delegate4,
            Func<SqlReader, T5> Delegate5,
            Func<SqlReader, T6> Delegate6,
            Func<SqlReader, T7> Delegate7,
            Func<SqlReader, T8> Delegate8,
            Func<SqlReader, T9> Delegate9,
            Func<SqlReader, T10> Delegate10)
        {
            var result = await ReadAsync(
                new ResultSetDelegate(1, Delegate1),
                new ResultSetDelegate(2, Delegate2),
                new ResultSetDelegate(3, Delegate3),
                new ResultSetDelegate(4, Delegate4),
                new ResultSetDelegate(5, Delegate5),
                new ResultSetDelegate(6, Delegate6),
                new ResultSetDelegate(7, Delegate7),
                new ResultSetDelegate(8, Delegate8),
                new ResultSetDelegate(9, Delegate9),
                new ResultSetDelegate(10, Delegate10)
            );
            return new MultipleResultSets<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>
            {
                Set1 = result[1].Cast<T1>(),
                Set2 = result[2].Cast<T2>(),
                Set3 = result[3].Cast<T3>(),
                Set4 = result[4].Cast<T4>(),
                Set5 = result[5].Cast<T5>(),
                Set6 = result[6].Cast<T6>(),
                Set7 = result[7].Cast<T7>(),
                Set8 = result[8].Cast<T8>(),
                Set9 = result[9].Cast<T9>(),
                Set10 = result[10].Cast<T10>(),
            };
        }

        /// <summary>
        /// Executes the Sql action applying the specified Action Delegate to the first result set.
        /// </summary>
        /// <param name="Action">The Action Delegate to apply to records from the result set.</param>
        /// <returns>
        /// Awaitable Task.
        /// </returns>
        public async Task ExecuteReaderAsync(Action<SqlReader> Action)
        {
            await ReadAsync(
                new ResultSetDelegate(1, Action)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <returns>
        /// Awaitable Task.
        /// </returns>
        public async Task ExecuteReaderAsync(
                    Action<SqlReader> Action1,
                    Action<SqlReader> Action2)
        {
            await ReadAsync(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(2, Action2)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <returns>
        /// Awaitable Task.
        /// </returns>
        public async Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3)
        {
            await ReadAsync(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(2, Action2),
                new ResultSetDelegate(3, Action3)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <returns>
        /// Awaitable Task.
        /// </returns>
        public async Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4)
        {
            await ReadAsync(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(2, Action2),
                new ResultSetDelegate(3, Action3),
                new ResultSetDelegate(4, Action4)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <returns>
        /// Awaitable Task.
        /// </returns>
        public async Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5)
        {
            await ReadAsync(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(2, Action2),
                new ResultSetDelegate(3, Action3),
                new ResultSetDelegate(4, Action4),
                new ResultSetDelegate(5, Action5)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <returns>
        /// Awaitable Task.
        /// </returns>
        public async Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6)
        {
            await ReadAsync(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(2, Action2),
                new ResultSetDelegate(3, Action3),
                new ResultSetDelegate(4, Action4),
                new ResultSetDelegate(5, Action5),
                new ResultSetDelegate(6, Action6)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <returns>
        /// Awaitable Task.
        /// </returns>
        public async Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7)
        {
            await ReadAsync(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(2, Action2),
                new ResultSetDelegate(3, Action3),
                new ResultSetDelegate(4, Action4),
                new ResultSetDelegate(5, Action5),
                new ResultSetDelegate(6, Action6),
                new ResultSetDelegate(7, Action7)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <param name="Action8">The Action Delegate to apply to records from the 8th result set.</param>
        /// <returns>
        /// Awaitable Task.
        /// </returns>
        public async Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7,
            Action<SqlReader> Action8)
        {
            await ReadAsync(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(2, Action2),
                new ResultSetDelegate(3, Action3),
                new ResultSetDelegate(4, Action4),
                new ResultSetDelegate(5, Action5),
                new ResultSetDelegate(6, Action6),
                new ResultSetDelegate(7, Action7),
                new ResultSetDelegate(8, Action8)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <param name="Action8">The Action Delegate to apply to records from the 8th result set.</param>
        /// <param name="Action9">The Action Delegate to apply to records from the 9th result set.</param>
        /// <returns>
        /// Awaitable Task.
        /// </returns>
        public async Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7,
            Action<SqlReader> Action8,
            Action<SqlReader> Action9)
        {
            await ReadAsync(
                new ResultSetDelegate(1, Action1),
                new ResultSetDelegate(2, Action2),
                new ResultSetDelegate(3, Action3),
                new ResultSetDelegate(4, Action4),
                new ResultSetDelegate(5, Action5),
                new ResultSetDelegate(6, Action6),
                new ResultSetDelegate(7, Action7),
                new ResultSetDelegate(8, Action8),
                new ResultSetDelegate(9, Action9)
            );
        }

        /// <summary>
        /// Executes the Sql action applying specified Action Delegates to MULTIPLE result sets.
        /// </summary>
        /// <param name="Action1">The Action Delegate to apply to records from the 1st result set.</param>
        /// <param name="Action2">The Action Delegate to apply to records from the 2nd result set.</param>
        /// <param name="Action3">The Action Delegate to apply to records from the 3rd result set.</param>
        /// <param name="Action4">The Action Delegate to apply to records from the 4th result set.</param>
        /// <param name="Action5">The Action Delegate to apply to records from the 5th result set.</param>
        /// <param name="Action6">The Action Delegate to apply to records from the 6th result set.</param>
        /// <param name="Action7">The Action Delegate to apply to records from the 7th result set.</param>
        /// <param name="Action8">The Action Delegate to apply to records from the 8th result set.</param>
        /// <param name="Action9">The Action Delegate to apply to records from the 9th result set.</param>
        /// <param name="Action10">The Action Delegate to apply to records from the 10th result set.</param>
        /// <returns>
        /// Awaitable Task.
        /// </returns>
        public async Task ExecuteReaderAsync(
            Action<SqlReader> Action1,
            Action<SqlReader> Action2,
            Action<SqlReader> Action3,
            Action<SqlReader> Action4,
            Action<SqlReader> Action5,
            Action<SqlReader> Action6,
            Action<SqlReader> Action7,
            Action<SqlReader> Action8,
            Action<SqlReader> Action9,
            Action<SqlReader> Action10)
        {
            await ReadAsync(
                new ResultSetDelegate(1,  Action1),
                new ResultSetDelegate(2,  Action2),
                new ResultSetDelegate(3,  Action3),
                new ResultSetDelegate(4,  Action4),
                new ResultSetDelegate(5,  Action5),
                new ResultSetDelegate(6,  Action6),
                new ResultSetDelegate(7,  Action7),
                new ResultSetDelegate(8,  Action8),
                new ResultSetDelegate(9,  Action9),
                new ResultSetDelegate(10, Action10)
            );
        }

        #endregion

        //#region code from stackoverflow 
        //// efficient conversion of byte array to hex courtesy of stackoverflow
        //// https://stackoverflow.com/questions/311165/how-do-you-convert-a-byte-array-to-a-hexadecimal-string-and-vice-versa

        //private static readonly uint[] _lookup32 = CreateLookup32();

        //private static uint[] CreateLookup32()
        //{
        //    var result = new uint[256];
        //    for (int i = 0; i < 256; i++)
        //    {
        //        string s = i.ToString("X2");
        //        result[i] = s[0] + ((uint)s[1] << 16);
        //    }
        //    return result;
        //}

        //private static string ByteArrayToHexViaLookup32(byte[] bytes)
        //{
        //    var lookup32 = _lookup32;
        //    var result = new char[bytes.Length * 2];
        //    for (int i = 0; i < bytes.Length; i++)
        //    {
        //        var val = lookup32[bytes[i]];
        //        result[2 * i] = (char)val;
        //        result[2 * i + 1] = (char)(val >> 16);
        //    }
        //    return new string(result);
        //}

        //#endregion
    }
}
