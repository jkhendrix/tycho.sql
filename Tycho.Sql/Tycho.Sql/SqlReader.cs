﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Tycho.Sql
{
    /// <summary>
    /// The Tycho abstraction of a standard SqlDataReader object.  This object provides C#-type-centric access methods which automatically handle most normal type conversions.  Access to the underlying SqlDataReader is still provided for those complex scenarios which are bound to arise.
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    /// <remarks>
    /// <para>
    /// While this SqlReader does grant you some flexibility in automatic conversion of data types, it is probably best practice if these data extraction calls are at least on similar types.  For instance, you "can" extract a VARCHAR(20) as an Int32 value ... and Tycho Sql will make its best attempt to "do the right thing", but if the DB ever returns a VARCHAR containing non-numeric data, your application should be prepared to receive a "default value" from the <codeInline>GetInt32()</codeInline> call.
    /// </para>
    /// <para>
    /// In this scenario it would make a more stable application to extract that VARCHAR as a String in your application, then handle the type conversion yourself.  
    /// </para>
    /// </remarks>
    public class SqlReader : IDisposable
    {
        /// <summary>
        /// Get accessor to the underlying SqlDataReader object.
        /// </summary>
        /// <value>
        /// The data reader.
        /// </value>
        public SqlDataReader DataReader { get; private set; }
        internal Dictionary<string, DataReaderColumn> ColumnLookup { get; private set; }

        internal SqlReader(SqlDataReader dr)
        {
            DataReader = dr;
        }

        /// <summary>
        /// Reset the column definitions for a SqlReader.  This is useful when switching between result sets.
        /// </summary>
        internal void ResetColumns()
        {
            ColumnLookup = new Dictionary<string, DataReaderColumn>();
            for (int x = 0; x < DataReader.FieldCount; x++)
            {
                ColumnLookup[DataReader.GetName(x).ToLower()] = new DataReaderColumn(x, DataReader.GetDataTypeName(x));
            }
        }

        /// <summary>
        /// Synchronous read of all the data from the current record.
        /// </summary>
        /// <returns></returns>
        internal bool Read()
        {
            return DataReader.Read();
        }

        /// <summary>
        /// Asynchronous read of all the data from the current record.
        /// </summary>
        /// <returns></returns>
        internal async Task<bool> ReadAsync()
        {
            return await DataReader.ReadAsync();
        }

        /// <summary>
        /// Synchronous move to next record.
        /// </summary>
        /// <returns></returns>
        internal bool NextResult()
        {
            return DataReader.NextResult();
        }

        /// <summary>
        /// Asynchronous move to next record.
        /// </summary>
        /// <returns></returns>
        internal async Task<bool> NextResultAsync()
        {
            return await DataReader.NextResultAsync();
        }

        #region Value Extraction

        /// <summary>
        /// Get the specified column value as a string.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>A String.</returns>
        public string GetString(string columnName)
            => GetString(columnName, null);

        /// <summary>
        /// Get the specified column value as a string.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <param name="format">A formatting string used if the underlying Sql data type is not a native string type.</param>
        /// <returns>A String.</returns>
        /// <remarks>
        /// Most Sql native data types can be extracted as Strings.  To assist your application, a format provider is available as an optional parameter.  Judicious testing is recommended to insure format correctness.
        /// </remarks>
        public string GetString(string columnName, string format)
        {
            string result = null;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.String:
                            result = DataReader.GetString(col.Ordinal);
                            break;
                        case NativeType.DateTime:
                            result = DataReader.GetDateTime(col.Ordinal).ToString(format);
                            break;
                        case NativeType.TimeSpan:
                            result = DataReader.GetTimeSpan(col.Ordinal).ToString(format);
                            break;
                        case NativeType.DateTimeOffset:
                            result = DataReader.GetDateTimeOffset(col.Ordinal).ToString(format);
                            break;
                        case NativeType.Float:
                            result = DataReader.GetFloat(col.Ordinal).ToString(format);
                            break;
                        case NativeType.Double:
                            result = DataReader.GetDouble(col.Ordinal).ToString(format);
                            break;
                        case NativeType.Decimal:
                            result = DataReader.GetDecimal(col.Ordinal).ToString(format);
                            break;
                        case NativeType.Long:
                            result = DataReader.GetInt64(col.Ordinal).ToString(format);
                            break;
                        case NativeType.Int:
                            result = DataReader.GetInt32(col.Ordinal).ToString(format);
                            break;
                        case NativeType.Short:
                            result = DataReader.GetInt16(col.Ordinal).ToString(format);
                            break;
                        case NativeType.Byte:
                            result = DataReader.GetByte(col.Ordinal).ToString(format);
                            break;
                        case NativeType.Bool:
                            result = DataReader.GetBoolean(col.Ordinal) ? "1" : "0";
                            break;
                        case NativeType.Guid:
                            result = DataReader.GetGuid(col.Ordinal).ToString(format);
                            break;
                        case NativeType.SqlMoney:
                            result = DataReader.GetSqlMoney(col.Ordinal).ToString();
                            break;
                        case NativeType.ByteStream:
                            // no conversion - return default value
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get the specified column value as an Int64.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>A nullable Int64.</returns>
        /// <remarks>
        /// Most numeric and string Sql native data types can be extracted as integer values.
        /// </remarks>
        public long? GetInt64(string columnName)
        {
            long? result = null;
            string tmp;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.String:
                            tmp = DataReader.GetString(col.Ordinal);
                            if (long.TryParse(tmp.SubstringBefore("."), out var tmpConverted))
                                result = tmpConverted;
                            break;
                        case NativeType.Float:
                            result = Convert.ToInt64(DataReader.GetFloat(col.Ordinal));
                            break;
                        case NativeType.Double:
                            result = Convert.ToInt64(DataReader.GetDouble(col.Ordinal));
                            break;
                        case NativeType.Decimal:
                            result = Convert.ToInt64(DataReader.GetDecimal(col.Ordinal));
                            break;
                        case NativeType.Long:
                            result = DataReader.GetInt64(col.Ordinal);
                            break;
                        case NativeType.Int:
                            result = DataReader.GetInt32(col.Ordinal);
                            break;
                        case NativeType.Short:
                            result = DataReader.GetInt16(col.Ordinal);
                            break;
                        case NativeType.Byte:
                            result = DataReader.GetByte(col.Ordinal);
                            break;
                        case NativeType.Bool:
                            result = DataReader.GetBoolean(col.Ordinal) ? 1 : 0;
                            break;
                        case NativeType.SqlMoney:
                            result = DataReader.GetSqlMoney(col.Ordinal).ToInt64();
                            break;
                        case NativeType.Guid:
                        case NativeType.DateTime:
                        case NativeType.TimeSpan:
                        case NativeType.DateTimeOffset:
                        case NativeType.ByteStream:
                            // no conversion - return default value
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get the specified column value as an Int32.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>A nullable Int32.</returns>
        /// <remarks>
        /// Most numeric and string Sql native data types can be extracted as integer values.
        /// </remarks>
        public int? GetInt32(string columnName)
        {
            int? result = null;
            string tmp;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.String:
                            tmp = DataReader.GetString(col.Ordinal);
                            if (int.TryParse(tmp.SubstringBefore("."), out var tmpConverted))
                                result = tmpConverted;
                            break;
                        case NativeType.Float:
                            result = Convert.ToInt32(DataReader.GetFloat(col.Ordinal));
                            break;
                        case NativeType.Double:
                            result = Convert.ToInt32(DataReader.GetDouble(col.Ordinal));
                            break;
                        case NativeType.Decimal:
                            result = Convert.ToInt32(DataReader.GetDecimal(col.Ordinal));
                            break;
                        case NativeType.Long:
                            result = Convert.ToInt32(DataReader.GetInt64(col.Ordinal));
                            break;
                        case NativeType.Int:
                            result = DataReader.GetInt32(col.Ordinal);
                            break;
                        case NativeType.Short:
                            result = DataReader.GetInt16(col.Ordinal);
                            break;
                        case NativeType.Byte:
                            result = DataReader.GetByte(col.Ordinal);
                            break;
                        case NativeType.Bool:
                            result = DataReader.GetBoolean(col.Ordinal) ? 1 : 0;
                            break;
                        case NativeType.SqlMoney:
                            result = DataReader.GetSqlMoney(col.Ordinal).ToInt32();
                            break;
                        case NativeType.Guid:
                        case NativeType.DateTime:
                        case NativeType.TimeSpan:
                        case NativeType.DateTimeOffset:
                        case NativeType.ByteStream:
                            // no conversion - return default value
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get the specified column value as an Int16.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>A nullable Int16.</returns>
        /// <remarks>
        /// Most numeric and string Sql native data types can be extracted as integer values.
        /// </remarks>
        public short? GetInt16(string columnName)
        {
            short? result = null;
            string tmp;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.String:
                            tmp = DataReader.GetString(col.Ordinal);
                            if (short.TryParse(tmp.SubstringBefore("."), out var tmpConverted))
                                result = tmpConverted;
                            break;
                        case NativeType.Float:
                            result = Convert.ToInt16(DataReader.GetFloat(col.Ordinal));
                            break;
                        case NativeType.Double:
                            result = Convert.ToInt16(DataReader.GetDouble(col.Ordinal));
                            break;
                        case NativeType.Decimal:
                            result = Convert.ToInt16(DataReader.GetDecimal(col.Ordinal));
                            break;
                        case NativeType.Long:
                            result = Convert.ToInt16(DataReader.GetInt64(col.Ordinal));
                            break;
                        case NativeType.Int:
                            result = Convert.ToInt16(DataReader.GetInt32(col.Ordinal));
                            break;
                        case NativeType.Short:
                            result = DataReader.GetInt16(col.Ordinal);
                            break;
                        case NativeType.Byte:
                            result = DataReader.GetByte(col.Ordinal);
                            break;
                        case NativeType.Bool:
                            result = Convert.ToInt16(DataReader.GetBoolean(col.Ordinal) ? 1 : 0);
                            break;
                        case NativeType.SqlMoney:
                            result = Convert.ToInt16(DataReader.GetSqlMoney(col.Ordinal).ToInt32());
                            break;
                        case NativeType.Guid:
                        case NativeType.DateTime:
                        case NativeType.TimeSpan:
                        case NativeType.DateTimeOffset:
                        case NativeType.ByteStream:
                            // no conversion - return default value
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get the specified column value as a Double.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>A nullable Double.</returns>
        /// <remarks>
        /// Most numeric and string Sql native data types can be extracted as floating point values.
        /// </remarks>
        public double? GetDouble(string columnName)
        {
            double? result = null;
            string tmp;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.String:
                            tmp = DataReader.GetString(col.Ordinal);
                            if (double.TryParse(tmp, out var tmpConverted))
                                result = tmpConverted;
                            break;
                        case NativeType.Float:
                            result = DataReader.GetFloat(col.Ordinal);
                            break;
                        case NativeType.Double:
                            result = DataReader.GetDouble(col.Ordinal);
                            break;
                        case NativeType.Decimal:
                            result = Convert.ToDouble(DataReader.GetDecimal(col.Ordinal));
                            break;
                        case NativeType.Long:
                            result = DataReader.GetInt64(col.Ordinal);
                            break;
                        case NativeType.Int:
                            result = DataReader.GetInt32(col.Ordinal);
                            break;
                        case NativeType.Short:
                            result = DataReader.GetInt16(col.Ordinal);
                            break;
                        case NativeType.Byte:
                            result = DataReader.GetByte(col.Ordinal);
                            break;
                        case NativeType.Bool:
                            result = DataReader.GetBoolean(col.Ordinal) ? 1 : 0;
                            break;
                        case NativeType.SqlMoney:
                            result = DataReader.GetSqlMoney(col.Ordinal).ToSqlDouble().Value;
                            break;
                        case NativeType.Guid:
                        case NativeType.DateTime:
                        case NativeType.TimeSpan:
                        case NativeType.DateTimeOffset:
                        case NativeType.ByteStream:
                            // no conversion - return default value
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get the specified column value as a Float.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>A nullable Float.</returns>
        /// <remarks>
        /// Most numeric and string Sql native data types can be extracted as floating point values.
        /// </remarks>
        public float? GetFloat(string columnName)
        {
            float? result = null;
            string tmp;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.String:
                            tmp = DataReader.GetString(col.Ordinal);
                            if (float.TryParse(tmp, out var tmpConverted))
                                result = tmpConverted;
                            break;
                        case NativeType.Float:
                            result = DataReader.GetFloat(col.Ordinal);
                            break;
                        case NativeType.Double:
                            result = Convert.ToSingle(DataReader.GetDouble(col.Ordinal));
                            break;
                        case NativeType.Decimal:
                            result = Convert.ToSingle(DataReader.GetDecimal(col.Ordinal));
                            break;
                        case NativeType.Long:
                            result = DataReader.GetInt64(col.Ordinal);
                            break;
                        case NativeType.Int:
                            result = DataReader.GetInt32(col.Ordinal);
                            break;
                        case NativeType.Short:
                            result = DataReader.GetInt16(col.Ordinal);
                            break;
                        case NativeType.Byte:
                            result = DataReader.GetByte(col.Ordinal);
                            break;
                        case NativeType.Bool:
                            result = DataReader.GetBoolean(col.Ordinal) ? 1 : 0;
                            break;
                        case NativeType.SqlMoney:
                            result = DataReader.GetSqlMoney(col.Ordinal).ToSqlSingle().Value;
                            break;
                        case NativeType.Guid:
                        case NativeType.DateTime:
                        case NativeType.TimeSpan:
                        case NativeType.DateTimeOffset:
                        case NativeType.ByteStream:
                            // no conversion - return default value
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get the specified column value as a DateTime.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>A nullable DateTime.</returns>
        /// <remarks>
        /// Most date/time types and string Sql native data types can be extracted as DateTime values.
        /// </remarks>
        public DateTime? GetDateTime(string columnName)
        {
            DateTime? result = null;
            string tmp;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.String:
                            tmp = DataReader.GetString(col.Ordinal);
                            if (DateTime.TryParse(tmp, out var tmpConverted))
                                result = tmpConverted;
                            break;
                        case NativeType.DateTime:
                            result = DataReader.GetDateTime(col.Ordinal);
                            break;
                        case NativeType.TimeSpan:
                            result = new DateTime() + DataReader.GetTimeSpan(col.Ordinal);
                            break;
                        case NativeType.DateTimeOffset:
                            var tmpOffset = DataReader.GetDateTimeOffset(col.Ordinal);
                            result = new DateTime(
                                tmpOffset.Year,
                                tmpOffset.Month,
                                tmpOffset.Day,
                                tmpOffset.Hour,
                                tmpOffset.Minute,
                                tmpOffset.Second,
                                tmpOffset.Millisecond);
                            break;
                        case NativeType.Float:
                        case NativeType.Double:
                        case NativeType.Decimal:
                        case NativeType.Long:
                        case NativeType.Int:
                        case NativeType.Short:
                        case NativeType.Byte:
                        case NativeType.Bool:
                        case NativeType.SqlMoney:
                        case NativeType.Guid:
                        case NativeType.ByteStream:
                            // no conversion - return default value
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get the specified column value as a DateTimeOffset.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>A nullable DateTimeOffset.</returns>
        public DateTimeOffset? GetDateTimeOffset(string columnName)
        {
            DateTimeOffset? result = null;
            string tmp;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.String:
                            tmp = DataReader.GetString(col.Ordinal);
                            if (DateTimeOffset.TryParse(tmp, out var tmpConverted))
                                result = tmpConverted;
                            break;
                        case NativeType.DateTime:
                            result = DataReader.GetDateTime(col.Ordinal);
                            break;
                        case NativeType.TimeSpan:
                            result = new DateTimeOffset() + DataReader.GetTimeSpan(col.Ordinal);
                            break;
                        case NativeType.DateTimeOffset:
                            result = DataReader.GetDateTimeOffset(col.Ordinal);
                            break;
                        case NativeType.Float:
                        case NativeType.Double:
                        case NativeType.Decimal:
                        case NativeType.Long:
                        case NativeType.Int:
                        case NativeType.Short:
                        case NativeType.Byte:
                        case NativeType.Bool:
                        case NativeType.SqlMoney:
                        case NativeType.Guid:
                        case NativeType.ByteStream:
                            // no conversion - return default value
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get the specified column value as a TimeSpan.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>A nullable TimeSpan.</returns>
        public TimeSpan? GetTimeSpan(string columnName)
        {
            TimeSpan? result = null;
            string tmp;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.String:
                            tmp = DataReader.GetString(col.Ordinal);
                            if (TimeSpan.TryParse(tmp, out var tmpConverted))
                                result = tmpConverted;
                            break;
                        case NativeType.DateTime:
                            result = DataReader.GetDateTime(col.Ordinal).TimeOfDay;
                            break;
                        case NativeType.TimeSpan:
                            result = DataReader.GetTimeSpan(col.Ordinal);
                            break;
                        case NativeType.DateTimeOffset:
                            result = DataReader.GetDateTimeOffset(col.Ordinal).TimeOfDay;
                            break;
                        case NativeType.Float:
                        case NativeType.Double:
                        case NativeType.Decimal:
                        case NativeType.Long:
                        case NativeType.Int:
                        case NativeType.Short:
                        case NativeType.Byte:
                        case NativeType.Bool:
                        case NativeType.SqlMoney:
                        case NativeType.Guid:
                        case NativeType.ByteStream:
                            // no conversion - return default value
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get the specified column value as a Boolean.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>A nullable bool.</returns>
        public bool? GetBoolean(string columnName)
        {
            bool? result = null;
            string tmp;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.String:
                            tmp = DataReader.GetString(col.Ordinal);
                            if (Int64.TryParse(tmp.SubstringBefore("."), out var tmpConverted))
                                result = tmpConverted != 0;
                            break;
                        case NativeType.Float:
                            result = DataReader.GetFloat(col.Ordinal) != 0;
                            break;
                        case NativeType.Double:
                            result = DataReader.GetDouble(col.Ordinal) != 0;
                            break;
                        case NativeType.Decimal:
                            result = DataReader.GetDecimal(col.Ordinal) != 0;
                            break;
                        case NativeType.Long:
                            result = DataReader.GetInt64(col.Ordinal) != 0;
                            break;
                        case NativeType.Int:
                            result = DataReader.GetInt32(col.Ordinal) != 0;
                            break;
                        case NativeType.Short:
                            result = DataReader.GetInt16(col.Ordinal) != 0;
                            break;
                        case NativeType.Byte:
                            result = DataReader.GetByte(col.Ordinal) != 0;
                            break;
                        case NativeType.Bool:
                            result = DataReader.GetBoolean(col.Ordinal);
                            break;
                        case NativeType.SqlMoney:
                            result = DataReader.GetSqlMoney(col.Ordinal).ToSqlInt64().Value != 0;
                            break;
                        case NativeType.Guid:
                        case NativeType.DateTime:
                        case NativeType.TimeSpan:
                        case NativeType.DateTimeOffset:
                        case NativeType.ByteStream:
                            // no conversion - return default value
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get the specified column value as a Guid.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>A nullable Guid.</returns>
        public Guid? GetGuid(string columnName)
        {
            Guid? result = null;
            string tmp;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.String:
                            tmp = DataReader.GetString(col.Ordinal);
                            if (Guid.TryParse(tmp.SubstringBefore("."), out var tmpConverted))
                                result = tmpConverted;
                            break;
                        case NativeType.Guid:
                            result = DataReader.GetGuid(col.Ordinal);
                            break;
                        case NativeType.Float:
                        case NativeType.Double:
                        case NativeType.Decimal:
                        case NativeType.Long:
                        case NativeType.Int:
                        case NativeType.Short:
                        case NativeType.Byte:
                        case NativeType.Bool:
                        case NativeType.SqlMoney:
                        case NativeType.DateTime:
                        case NativeType.TimeSpan:
                        case NativeType.DateTimeOffset:
                        case NativeType.ByteStream:
                            // no conversion - return default value
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get the specified column value as an Enum value.  Note that this is a case insensitive comparison.
        /// </summary>
        /// <typeparam name="T">An Enum type to return.</typeparam>
        /// <param name="columnName">The field name.</param>
        /// <param name="defaultValue">The default value returned if the value is null.</param>
        /// <returns>An enumerable value of the defined generic type T.  If the DB value could not be traslated for any reason, the default value is returned.</returns>
        public T GetEnum<T>(string columnName, T defaultValue) where T: Enum
            => GetEnum(columnName, defaultValue, true);

        /// <summary>
        /// Get the specified column value as an Enum value.
        /// </summary>
        /// <typeparam name="T">An Enum type to return.</typeparam>
        /// <param name="columnName">The field name.</param>
        /// <param name="defaultValue">The default value returned if the value is null.</param>
        /// <param name="ignoreCase">A flag to allow the Enum parser to ignore case.</param>
        /// <returns>An enumerable value of the defined generic type T.  If the DB value could not be traslated for any reason, the default value is returned.</returns>
        public T GetEnum<T>(string columnName, T defaultValue, bool ignoreCase) where T : Enum
        {
            T result = defaultValue;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    try
                    {
                        switch (col.NativeType)
                        {
                            case NativeType.String:
                                var tmpString = DataReader.GetString(col.Ordinal);
                                if (!string.IsNullOrWhiteSpace(tmpString))
                                    result = (T)Enum.Parse(typeof(T), tmpString, ignoreCase);
                                break;
                            case NativeType.Long:
                                result = (T)Enum.ToObject(typeof(T), DataReader.GetInt64(col.Ordinal));
                                break;
                            case NativeType.Int:
                                result = (T)Enum.ToObject(typeof(T), DataReader.GetInt32(col.Ordinal));
                                break;
                            case NativeType.Short:
                                result = (T)Enum.ToObject(typeof(T), DataReader.GetInt16(col.Ordinal));
                                break;
                            case NativeType.Byte:
                                result = (T)Enum.ToObject(typeof(T), DataReader.GetByte(col.Ordinal));
                                break;
                            case NativeType.Guid:
                            case NativeType.Float:
                            case NativeType.Double:
                            case NativeType.Decimal:
                            case NativeType.Bool:
                            case NativeType.SqlMoney:
                            case NativeType.DateTime:
                            case NativeType.TimeSpan:
                            case NativeType.DateTimeOffset:
                            case NativeType.ByteStream:
                                // no conversion - return default value
                                break;
                        }
                    }
                    catch
                    {
                        // there are a number of things that could throw in this block, catch them all and ignore
                    }
                }
            }

            // In C# you can have any value in an underlying Enum type (int), 
            // so we must validate the result is defined in the Enum before returning.
            if (!Enum.IsDefined(typeof(T), result))
                result = defaultValue;

            return result;
        }

        /// <summary>
        /// Get the specified column value as an XmlDocument.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>A XmlDocument object.  If the DB value could not be translated for any reason, null is returned.</returns>
        public XmlDocument GetXmlDocument(string columnName)
        {
            XmlDocument result = null;
            string tmp;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.String:
                            tmp = DataReader.GetString(col.Ordinal);
                            if (!string.IsNullOrWhiteSpace(tmp))
                            {
                                result = new XmlDocument();
                                try { result.LoadXml(tmp); }
                                catch { result = null; }
                            }
                            break;
                        case NativeType.Guid:
                        case NativeType.Float:
                        case NativeType.Double:
                        case NativeType.Decimal:
                        case NativeType.Long:
                        case NativeType.Int:
                        case NativeType.Short:
                        case NativeType.Byte:
                        case NativeType.Bool:
                        case NativeType.SqlMoney:
                        case NativeType.DateTime:
                        case NativeType.TimeSpan:
                        case NativeType.DateTimeOffset:
                        case NativeType.ByteStream:
                            // no conversion - return default value
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get the specified column value as an XDocument.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>An XDocument object.  If the DB value could not be translated for any reason, null is returned.</returns>
        public XDocument GetXDocument(string columnName)
        {
            XDocument result = null;
            string tmp;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.String:
                            tmp = DataReader.GetString(col.Ordinal);
                            if (!string.IsNullOrWhiteSpace(tmp))
                            {
                                try { result = XDocument.Parse(tmp); }
                                catch { result = null; }
                            }
                            break;
                        case NativeType.Guid:
                        case NativeType.Float:
                        case NativeType.Double:
                        case NativeType.Decimal:
                        case NativeType.Long:
                        case NativeType.Int:
                        case NativeType.Short:
                        case NativeType.Byte:
                        case NativeType.Bool:
                        case NativeType.SqlMoney:
                        case NativeType.DateTime:
                        case NativeType.TimeSpan:
                        case NativeType.DateTimeOffset:
                        case NativeType.ByteStream:
                            // no conversion - return default value
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get the specified column value as a Byte Array.  This is only useful if the Sql native data type is Binary, VarBinary, Image, or Variant.
        /// </summary>
        /// <param name="columnName">The field name.</param>
        /// <returns>A byte array of binary data, or null.</returns>
        public byte[] GetByteArray(string columnName)
        {
            byte[] result = null;

            if (ColumnLookup.TryGetValue(columnName.ToLowerInvariant(), out var col))
            {
                if (!DataReader.IsDBNull(col.Ordinal))
                {
                    switch (col.NativeType)
                    {
                        case NativeType.ByteStream:
                            using (var readerStream = DataReader.GetStream(col.Ordinal))
                            {
                                result = new byte[(int)readerStream.Length];
                                readerStream.Read(result, 0, (int)readerStream.Length);
                            }
                            break;
                        case NativeType.String:
                        case NativeType.Guid:
                        case NativeType.Float:
                        case NativeType.Double:
                        case NativeType.Decimal:
                        case NativeType.Long:
                        case NativeType.Int:
                        case NativeType.Short:
                        case NativeType.Byte:
                        case NativeType.Bool:
                        case NativeType.SqlMoney:
                        case NativeType.DateTime:
                        case NativeType.TimeSpan:
                        case NativeType.DateTimeOffset:
                            // no conversion - return default value
                            break;
                    }

                }
            }

            return result;
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (DataReader != null)
            {
                DataReader.Dispose();
                DataReader = null;
            }
        }

        #endregion
    }
}
