﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Tycho.Sql
{
    internal class TableColumn 
    {
        public TableParameterColumnAttribute attr;
        public PropertyInfo prop;
        public Func<object, object> GetValue;
    }
}
