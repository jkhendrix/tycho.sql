﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Tycho.Sql.Interfaces;
using Tycho.Sql.Tests.DTOs;

namespace Tycho.Sql.Tests
{
    [TestClass, ExcludeFromCodeCoverage]
    public class SqlTests
    {
        [TestMethod, TestCategory("Unit")]
        public void Sql_Constructor_Configures_ConnectionString_WhenFoundInConfigFile()
        {
            // setup
            string connectionStringName = "ConnectionStringFromConfigFile";
            string expectedValue = "Test Value";

            // act
            var sql = new Sql(connectionStringName);

            // assert
            Assert.AreEqual(expectedValue, sql.ConnectionString);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void Sql_Constructor_Configures_ConnectionString_WhenNotFoundInConfigFile()
        {
            // setup
            string expectedValue = "Pre-Configured Connection String Value";

            // act
            var sql = new Sql(expectedValue);

            // assert
            Assert.AreEqual(expectedValue, sql.ConnectionString);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void Sql_Constructor_ConnectionString_ThrowsWhenConnectionStringNullOrWhitespace()
        {
            // setup
            bool caughtException = false;

            // act
            try { var sql = new Sql(null); }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument ConnectionStringOrName cannot be null"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void Sql_Constructor_Configures_ConnectionString_DefaultTimeout()
        {
            // setup
            var expectedConnectionString = "Test Value";
            var expectedDefaultTimeout = 125;

            // act
            var sql = new Sql(expectedConnectionString, expectedDefaultTimeout);

            // assert
            Assert.AreEqual(expectedConnectionString, sql.ConnectionString);
            Assert.AreEqual(expectedDefaultTimeout, sql.DefaultTimeout);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void Sql_Constructor_ConnectionString_DefaultTimeout_ThrowsWhenConnectionStringNullOrWhitespace()
        {
            // setup
            bool caughtException = false;

            // act
            try { var sql = new Sql(null, 10); }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument ConnectionStringOrName cannot be null"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void Sql_Constructor_ConnectionString_DefaultTimeout_ThrowsWhenDefaultTimeoutLessThanZero()
        {
            // setup
            bool caughtException = false;

            // act
            try { var sql = new Sql("Blah", -10); }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument DefaultTimeout cannot be < 0"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task Sql_Constructor_Configures_ConnectionString_DefaultExceptionHandler_WhenSynchronous()
        {
            // setup
            var expectedConnectionString = "Test Value";
            var expectedExceptionHandlerResult = true;
            bool expectedHandler(Exception ex) => expectedExceptionHandlerResult;

            // act
            var sql = new Sql(expectedConnectionString, expectedHandler);

            // assert
            Assert.AreEqual(expectedConnectionString, sql.ConnectionString);
            // note - under the hood, the Sql service turns the synchronous handler asynchronous
            Assert.IsTrue(await sql.DefaultExceptionHandler(new Exception()));

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void Sql_Constructor_ConnectionString_DefaultExceptionHandler_WhenSynchronous_ThrowsWhenConnectionStringNullOrWhitespace()
        {
            // setup
            bool caughtException = false;
            var expectedExceptionHandlerResult = true;
            bool expectedHandler(Exception ex) => expectedExceptionHandlerResult;

            // act
            try { var sql = new Sql(null, expectedHandler); }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument ConnectionStringOrName cannot be null"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task Sql_Constructor_ConnectionString_DefaultExceptionHandler_WhenSynchronous_ReturnsFalseHandlerWhenPassedNullDelegate()
        {
            // setup
            bool expectedHandlerResult = false;

            // act
            var sql = new Sql("Blah", (Func<Exception, bool>)null);
            var actualHandlerResult = await sql.DefaultExceptionHandler(new Exception());

            // assert
            Assert.AreEqual(expectedHandlerResult, actualHandlerResult);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task Sql_Constructor_Configures_ConnectionString_DefaultExceptionHandler_WhenAsynchronous()
        {
            // setup
            var expectedConnectionString = "Test Value";
            var expectedExceptionHandlerResult = true;
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
            async Task<bool> expectedHandler(Exception ex) => expectedExceptionHandlerResult;
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously

            // act
            var sql = new Sql(expectedConnectionString, expectedHandler);

            // assert
            Assert.AreEqual(expectedConnectionString, sql.ConnectionString);
            Assert.IsTrue(await sql.DefaultExceptionHandler(new Exception()));

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task Sql_Constructor_ConnectionString_DefaultExceptionHandler_WhenAsynchronous_ThrowsWhenConnectionStringNullOrWhitespace()
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            // setup
            bool caughtException = false;
            var expectedExceptionHandlerResult = true;
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
            async Task<bool> expectedHandler(Exception ex) => expectedExceptionHandlerResult;
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously

            // act
            try { var sql = new Sql(null, expectedHandler); }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument ConnectionStringOrName cannot be null"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task Sql_Constructor_ConnectionString_DefaultExceptionHandler_WhenAsynchronous_ReturnsFalseHandlerWhenPassedNullDelegate()
        {
            // setup
            bool expectedHandlerResult = false;

            // act
            var sql = new Sql("Blah", (Func<Exception, Task<bool>>)null);
            var actualHandlerResult = await sql.DefaultExceptionHandler(new Exception());

            // assert
            Assert.AreEqual(expectedHandlerResult, actualHandlerResult);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task Sql_Constructor_Configures_ConnectionString_DefaultTimeout_DefaultExceptionHandler_WhenSynchronous()
        {
            // setup
            var expectedConnectionString = "Test Value";
            var expectedDefaultTimeout = 125;
            var expectedExceptionHandlerResult = true;
            bool expectedHandler(Exception ex) => expectedExceptionHandlerResult;

            // act
            var sql = new Sql(expectedConnectionString, expectedDefaultTimeout, expectedHandler);

            // assert
            Assert.AreEqual(expectedConnectionString, sql.ConnectionString);
            Assert.AreEqual(expectedDefaultTimeout, sql.DefaultTimeout);
            // note - under the hood, the Sql service turns the synchronous handler asynchronous
            Assert.IsTrue(await sql.DefaultExceptionHandler(new Exception()));

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void Sql_Constructor_ConnectionString_DefaultTimeout_DefaultExceptionHandler_WhenSynchronous_ThrowsWhenConnectionStringNullOrWhitespace()
        {
            // setup
            bool caughtException = false;
            var expectedExceptionHandlerResult = true;
            bool expectedHandler(Exception ex) => expectedExceptionHandlerResult;

            // act
            try { var sql = new Sql(null, 10, expectedHandler); }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument ConnectionStringOrName cannot be null"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void Sql_Constructor_ConnectionString_DefaultTimeout_DefaultExceptionHandler_WhenSynchronous_ThrowsWhenDefaultTimeoutLessThanZero()
        {
            // setup
            bool caughtException = false;
            var expectedExceptionHandlerResult = true;
            bool expectedHandler(Exception ex) => expectedExceptionHandlerResult;

            // act
            try { var sql = new Sql("Blah", -10, expectedHandler); }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument DefaultTimeout cannot be < 0"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task Sql_Constructor_ConnectionString_DefaultTimeout_DefaultExceptionHandler_WhenSynchronous_ReturnsFalseHandlerWhenPassedNullDelegate()
        {
            // setup
            bool expectedHandlerResult = false;

            // act
            var sql = new Sql("Blah", 10, (Func<Exception, bool>)null);
            var actualHandlerResult = await sql.DefaultExceptionHandler(new Exception());

            // assert
            Assert.AreEqual(expectedHandlerResult, actualHandlerResult);

            // cleanup
        }
                     
        [TestMethod, TestCategory("Unit")]
        public async Task Sql_Constructor_Configures_ConnectionString_DefaultTimeout_DefaultExceptionHandler_WhenAsynchronous()
        {
            // setup
            var expectedConnectionString = "Test Value";
            var expectedDefaultTimeout = 125;
            var expectedExceptionHandlerResult = true;
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
            async Task<bool> expectedHandler(Exception ex) => expectedExceptionHandlerResult;
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously

            // act
            var sql = new Sql(expectedConnectionString, expectedDefaultTimeout, expectedHandler);

            // assert
            Assert.AreEqual(expectedConnectionString, sql.ConnectionString);
            Assert.AreEqual(expectedDefaultTimeout, sql.DefaultTimeout);
            // note - under the hood, the Sql service turns the synchronous handler asynchronous
            Assert.IsTrue(await sql.DefaultExceptionHandler(new Exception()));

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task Sql_Constructor_ConnectionString_DefaultTimeout_DefaultExceptionHandler_WhenAsynchronous_ThrowsWhenConnectionStringNullOrWhitespace()
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            // setup
            bool caughtException = false;
            var expectedExceptionHandlerResult = true;
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
            async Task<bool> expectedHandler(Exception ex) => expectedExceptionHandlerResult;
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously

            // act
            try { var sql = new Sql(null, 10, expectedHandler); }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument ConnectionStringOrName cannot be null"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task Sql_Constructor_ConnectionString_DefaultTimeout_DefaultExceptionHandler_WhenAsynchronous_ThrowsWhenDefaultTimeoutLessThanZero()
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            // setup
            bool caughtException = false;
            var expectedExceptionHandlerResult = true;
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
            async Task<bool> expectedHandler(Exception ex) => expectedExceptionHandlerResult;
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously

            // act
            try { var sql = new Sql("Blah", -10, expectedHandler); }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument DefaultTimeout cannot be < 0"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task Sql_Constructor_ConnectionString_DefaultTimeout_DefaultExceptionHandler_WhenAsynchronous_ReturnsFalseHandlerWhenPassedNullDelegate()
        {
            // setup
            bool expectedHandlerResult = false;

            // act
            var sql = new Sql("Blah", 10, (Func<Exception, Task<bool>>)null);
            var actualHandlerResult = await sql.DefaultExceptionHandler(new Exception());

            // assert
            Assert.AreEqual(expectedHandlerResult, actualHandlerResult);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void Sql_Procedure_ReturnsISqlActionConfiguring()
        {
            // setup
            var sql = new Sql("No Connection");

            // act
            var action = sql.Procedure("ProcedureName", "xyz");

            // assert
            Assert.IsTrue(action is ISqlActionConfiguring);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void Sql_Statement_ReturnsISqlActionConfiguring()
        {
            // setup
            var sql = new Sql("No Connection");

            // act
            var action = sql.Statement("Some Sql Statement");

            // assert
            Assert.IsTrue(action is ISqlActionConfiguring);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void Sql_GetTableColumns_WorksWhenGenericTypeIsNotDecorated()
        {
            // setup
            var sql = new Sql("ConnectionString");

            // act
            var columns = sql.GetTableParameterDefinition<TableTypeWithNoTableParameter>();

            // assert
            Assert.AreEqual(0, columns.Columns.Count());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void Sql_GetTableColumns_WorksWhenGenericTypeIsDecorated()
        {
            // setup
            var sql = new Sql("ConnectionString");
            var expectedColumnName1 = "SomeName";
            var expectedColumnName2 = "SomeOtherName";
            var expectedColumnName3 = "ThirdColumn";
            var expectedCount = 4;
            var expectedNullableCount = 2;
            var expectedWithMaxLengthMoreThan10 = 1;

            // act
            var columns = sql.GetTableParameterDefinition<TableTypeWithParameters>().Columns.ToList();

            // assert
            Assert.AreEqual(expectedCount, columns.Count());
            Assert.AreEqual(expectedNullableCount, columns.Count(c => c.attr.IsNullable));
            Assert.AreEqual(expectedWithMaxLengthMoreThan10, columns.Count(c => c.attr.MaxLength > 0));

            Assert.AreEqual(expectedColumnName1, columns[0].attr.ColumnName);
            Assert.AreEqual(expectedColumnName2, columns[1].attr.ColumnName);
            Assert.AreEqual(expectedColumnName3, columns[2].attr.ColumnName);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task Sql_WorksWithMoq()
        {
            // setup
            var expected = 123;

            var configuredMock = new Mock<ISqlActionConfigured>();

            // setup configured parameters
            configuredMock.Setup(c => c.AddOutputParameter(It.IsAny<string>(), It.IsAny<SqlDbType>(), It.IsAny<Action<It.IsAnyType>>()))
                .Returns(configuredMock.Object);
            configuredMock.Setup(c => c.AddOutputParameter(It.IsAny<string>(), It.IsAny<SqlDbType>(), It.IsAny<int>(), It.IsAny<Action<It.IsAnyType>>()))
                .Returns(configuredMock.Object);
            configuredMock.Setup(c => c.AddOutputParameter(It.IsAny<string>(), It.IsAny<SqlDbType>(), It.IsAny<byte>(), It.IsAny<byte>(), It.IsAny<Action<It.IsAnyType>>()))
                .Returns(configuredMock.Object);
            configuredMock.Setup(c => c.AddOutputParameter(It.IsAny<string>(), It.IsAny<SqlDbType>(), It.IsAny<Action<It.IsAnyType>>(), It.IsAny<It.IsAnyType>()))
                .Returns(configuredMock.Object);
            configuredMock.Setup(c => c.AddOutputParameter(It.IsAny<string>(), It.IsAny<SqlDbType>(), It.IsAny<int>(), It.IsAny<Action<It.IsAnyType>>(), It.IsAny<It.IsAnyType>()))
                .Returns(configuredMock.Object);
            configuredMock.Setup(c => c.AddOutputParameter(It.IsAny<string>(), It.IsAny<SqlDbType>(), It.IsAny<byte>(), It.IsAny<byte>(), It.IsAny<Action<It.IsAnyType>>(), It.IsAny<It.IsAnyType>()))
                .Returns(configuredMock.Object);
            configuredMock.Setup(c => c.AddParameter(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<SqlDbType>()))
                .Returns(configuredMock.Object);
            configuredMock.Setup(c => c.AddTableParameter(It.IsAny<string>(), It.IsAny<IEnumerable<It.IsAnyType>>()))
                .Returns(configuredMock.Object);

            // setup configured execution
            configuredMock.Setup(c => c.Execute())
                .Returns(expected);
            configuredMock.Setup(c => c.ExecuteAsync())
                .Returns(Task.FromResult(expected));

            var configuringMock = new Mock<ISqlActionConfiguring>();

            // setup configuring
            configuringMock.Setup(c => c.SetTimeout(It.IsAny<int>()))
                .Returns(configuringMock.Object);
            configuringMock.Setup(c => c.OnException(It.IsAny<Func<Exception, bool>>()))
                .Returns(configuringMock.Object);
            configuringMock.Setup(c => c.OnException(It.IsAny<Func<Exception, Task<bool>>>()))
                .Returns(configuringMock.Object);
            configuringMock.Setup(c => c.OnBeforeCommandExecute(It.IsAny<Action<SqlCommand>>()))
                .Returns(configuringMock.Object);
            configuringMock.Setup(c => c.OnBeforeCommandExecute(It.IsAny<Func<SqlCommand, Task>>()))
                .Returns(configuringMock.Object);

            configuringMock.Setup(c => c.AddOutputParameter(It.IsAny<string>(), It.IsAny<SqlDbType>(), It.IsAny<Action<It.IsAnyType>>()))
                .Returns(configuredMock.Object);
            configuringMock.Setup(c => c.AddOutputParameter(It.IsAny<string>(), It.IsAny<SqlDbType>(), It.IsAny<int>(), It.IsAny<Action<It.IsAnyType>>()))
                .Returns(configuredMock.Object);
            configuringMock.Setup(c => c.AddOutputParameter(It.IsAny<string>(), It.IsAny<SqlDbType>(), It.IsAny<byte>(), It.IsAny<byte>(), It.IsAny<Action<It.IsAnyType>>()))
                .Returns(configuredMock.Object);
            configuringMock.Setup(c => c.AddOutputParameter(It.IsAny<string>(), It.IsAny<SqlDbType>(), It.IsAny<Action<It.IsAnyType>>(), It.IsAny<It.IsAnyType>()))
                .Returns(configuredMock.Object);
            configuringMock.Setup(c => c.AddOutputParameter(It.IsAny<string>(), It.IsAny<SqlDbType>(), It.IsAny<int>(), It.IsAny<Action<It.IsAnyType>>(), It.IsAny<It.IsAnyType>()))
                .Returns(configuredMock.Object);
            configuringMock.Setup(c => c.AddOutputParameter(It.IsAny<string>(), It.IsAny<SqlDbType>(), It.IsAny<byte>(), It.IsAny<byte>(), It.IsAny<Action<It.IsAnyType>>(), It.IsAny<It.IsAnyType>()))
                .Returns(configuredMock.Object);
            configuringMock.Setup(c => c.AddParameter(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<SqlDbType>()))
                .Returns(configuredMock.Object);
            configuringMock.Setup(c => c.AddTableParameter(It.IsAny<string>(), It.IsAny<IEnumerable<It.IsAnyType>>()))
                .Returns(configuredMock.Object);

            var sqlMock = new Mock<ISql>();
            sqlMock.Setup(s => s.Procedure(It.IsAny<string>()))
                .Returns(configuringMock.Object);
            sqlMock.Setup(s => s.Procedure(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(configuringMock.Object);
            sqlMock.Setup(s => s.Statement(It.IsAny<string>()))
                .Returns(configuringMock.Object);

            var sql = sqlMock.Object;

            // act
            var actual1 = sql.Procedure("some procedure name")
                .SetTimeout(100)
                .OnException((e) => false)
                .OnBeforeCommandExecute((s) => { })
                .AddParameter("Param1", 200, SqlDbType.Int)
                .Execute();

            var actual2 = await sql.Procedure("some procedure name")
                .SetTimeout(100)
                .OnException((e) => false)
                .OnBeforeCommandExecute((s) => { })
                .AddParameter("Param1", 200, SqlDbType.Int)
                .ExecuteAsync();

            // assert
            Assert.AreEqual(expected, actual1);
            Assert.AreEqual(expected, actual2);

            // cleanup
        }
    }
}
