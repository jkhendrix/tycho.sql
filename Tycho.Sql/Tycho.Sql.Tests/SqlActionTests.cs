﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Tycho.Sql.Tests.DTOs;

namespace Tycho.Sql.Tests
{
    [TestClass, ExcludeFromCodeCoverage]
    public class SqlActionTests
    {
        [ClassInitialize]
#pragma warning disable IDE0060 // Remove unused parameter
        public static void Initialize(TestContext c)
#pragma warning restore IDE0060 // Remove unused parameter
        {
            var sql = new Sql("TychoSqlTests");

            // create types
            sql.Statement(@"
                IF TYPE_ID('[dbo].[SomeType]') IS NULL
                    EXEC ('
                        CREATE TYPE [dbo].[SomeType] AS TABLE ( 
                            [SomeName] [varchar](100) NULL,
                            [SomeOtherName] [bit] NOT NULL,
                            [ThirdColumn] [varchar] (20) NOT NULL,
                            [NullColumn] [int] NULL
                        );'
                    )")
                .Execute();

            // create procedures
            sql.Statement(@"
                IF NOT EXISTS ( 
                    SELECT 1 FROM sysobjects WHERE id = object_id('[dbo].[TestSP1]') and OBJECTPROPERTY(id, 'IsProcedure') = 1 ) 
                    EXEC('CREATE PROCEDURE [dbo].[TestSP1] AS SELECT 1;')")
                .Execute();

            // tables
            sql.Statement(@"
                IF OBJECT_ID ('SimpleTable', 'U') IS NULL 
                    CREATE TABLE [dbo].[SimpleTable](
                        [col1] [int] NULL
                    ) ON [PRIMARY]")
                .Execute();
        }

        [ClassCleanup]
        public static void Teardown()
        {
            var sql = new Sql("TychoSqlTests");

            // drop tables
            sql.Statement(@"
                IF OBJECT_ID ('SimpleTable', 'U') IS NOT NULL 
                    DROP TABLE [dbo].[SimpleTable]")
                .Execute();

            // drop procedures
            sql.Statement(@"
                IF EXISTS ( 
                    SELECT 1 FROM sysobjects WHERE id = object_id('[dbo].[TestSP1]') and OBJECTPROPERTY(id, 'IsProcedure') = 1 ) 
                    EXEC('DROP PROCEDURE [dbo].[TestSP1]');")
                .Execute();

            // drop types
            sql.Statement(@"
                IF TYPE_ID('[dbo].[SomeType]') IS NOT NULL
                    EXEC('DROP TYPE [dbo].[SomeType]');")
                .Execute();
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_SetTimeout_ThrowsIfTimeoutLessThanZero()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;

            // act
            try
            {
                sql.Statement("SELECT 1")
                    .SetTimeout(-1);
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument Timeout cannot be < 0"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_AddParameter_ThrowsIfNameIsNullOrWhitespace()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;

            // act
            try 
            { 
                sql.Statement("SELECT 1")
                    .AddParameter(null, "value", SqlDbType.VarChar); 
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument Name cannot be null or"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_AddOutputParameter_ThrowsIfNameIsNullOrWhitespace()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;

            // act
            try
            {
                sql.Statement("SELECT 1")
                    .AddOutputParameter<int>(null, SqlDbType.Int, (value) => { });
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument Name cannot be null or"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_AddOutputParameter_WithSize_ThrowsIfNameIsNullOrWhitespace()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;

            // act
            try
            {
                sql.Statement("SELECT 1")
                    .AddOutputParameter<int>(null, SqlDbType.Int, 10, (value) => { });
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument Name cannot be null or"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_AddOutputParameter_WithPrecisionAndScale_ThrowsIfNameIsNullOrWhitespace()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;

            // act
            try
            {
                sql.Statement("SELECT 1")
                    .AddOutputParameter<int>(null, SqlDbType.Int, 10, 10, (value) => { });
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument Name cannot be null or"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_AddTableParameter_ThrowsIfNameIsNullOrWhitespace()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;
            var tableValues = new List<TableTypeWithParameters>();

            // act
            try
            {
                sql.Statement("SELECT 1")
                    .AddTableParameter(null, tableValues);
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument Name cannot be null or"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_AddTableParameter_ThrowsIfTableDataIsNull()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;

            // act
            try
            {
                sql.Statement("SELECT 1")
                    .AddTableParameter("Blah", (IEnumerable<string>)null);
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Argument TableData cannot be null"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_AddTableParameter_ThrowsIfTableDataGenericTypeHasNoTableParameterAttribute()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;
            var tableValues = new List<TableTypeWithNoTableParameter>();

            // act
            try
            {
                sql.Statement("SELECT 1")
                    .AddTableParameter("Name", tableValues);
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Class definition for TableData generic type is missing the TableParameterAttribute"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_AddTableParameter_ThrowsIfTableDataGenericTypeHasMisconfiguredTableParameterAttribute()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;
            var tableValues = new List<TableTypeWithMisconfiguredTableParameter>();

            // act
            try
            {
                sql.Statement("SELECT 1")
                    .AddTableParameter("Name", tableValues);
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("TableParameterAttribute for TableData generic type cannot define a null or whitespace TypeName"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_AddTableParameter_ThrowsIfTableDataGenericTypeHasNoTableParameterColumnAttributes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;
            var tableValues = new List<TableTypeWithNoTableParameterColumns>();

            // act
            try
            {
                sql.Statement("SELECT 1")
                    .AddTableParameter("Name", tableValues);
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Class definition for TableData generic type is missing properties decorated with TableParameterColumnAttribute"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_AddTableParameter_ThrowsIfNotNullColumnContainsNullValue()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;
            var tableValues = new List<TableTypeWithParameters>()
            {
                new TableTypeWithParameters{ Id = "id", BooleanValue = false, ThirdValue = "third" },
                new TableTypeWithParameters{ Id = "id", BooleanValue = false, ThirdValue = null },  // ThirdValue is NOT NULL
            };

            // act
            try
            {
                sql.Statement("SELECT 1")
                    .AddTableParameter("Name", tableValues);
            }
            catch (TableValueNullException ex)
            {
                if (ex.Message.StartsWith("A value in TableData for a 'Not Null' column (ThirdValue) has a null value"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_AddTableParameter_TruncatesStringValuesLongerThanMaxLength()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            string expectedValue = "12345678901234567890";
            string actualValue = null;
            var tableValues = new List<TableTypeWithParameters>()
            {
                new TableTypeWithParameters{ Id = "id", BooleanValue = false, ThirdValue = expectedValue + "abcdeabcde" },
            };

            // act
            sql.Statement("SELECT * FROM @Name")
                .AddTableParameter("@Name", tableValues)
                .ExecuteReader(dbo => 
                {
                    actualValue = dbo.GetString("ThirdColumn");
                });

            // assert
            Assert.AreEqual(expectedValue, actualValue);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_Read_TerminatesEarlyIfItRunsOutOfDelegates()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            int expectedValue = 1;
            int actualValue = 0;

            // act
            sql.Statement("SELECT 1 'one'; SELECT 2 'two'; SELECT 3 'three';")
                .ExecuteReader(dbo =>
                {
                    actualValue = dbo.GetInt32("one") ?? 0;
                });

            // assert
            Assert.AreEqual(expectedValue, actualValue);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_SetTimeout_StoresValueToSqlAction()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            var expectedTimeout = 200;

            // act
            var action = (SqlAction)sql.Statement("select 1")
                .SetTimeout(expectedTimeout);

            // assert
            Assert.AreEqual(expectedTimeout, action._timeout);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_OnException_StoresValueToSqlAction_WhenHandlerIsSynchronous()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool expectedResult = true;
            bool actualResult = false;

            // act
            var action = (SqlAction)sql.Statement("select 1")
                .OnException((ex) => true);

            actualResult = await action._exceptionHandler(null);

            // assert
            Assert.AreEqual(expectedResult, actualResult);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_OnException_StoresValueToSqlAction_WhenHandlerIsAsynchronous()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool expectedResult = true;
            bool actualResult = false;

            // act
            var action = (SqlAction)sql.Statement("select 1")
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
                .OnException(async (ex) => true);
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously

            actualResult = await action._exceptionHandler(null);

            // assert
            Assert.AreEqual(expectedResult, actualResult);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_Execute_Calls_OnBeforeCommandExecute_WhenHandlerIsSynchronous()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool expectedResult = true;
            bool actualResult = false;

            // act
            sql.Statement("select 1")
                .OnBeforeCommandExecute((x) => { actualResult = true; })
                .Execute();

            // assert
            Assert.AreEqual(expectedResult, actualResult);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_Execute_Calls_OnBeforeCommandExecute_WhenHandlerIsAsynchronous()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool expectedResult = true;
            bool actualResult = false;

            // act
            sql.Statement("select 1")
                .OnBeforeCommandExecute(async (x) =>
                {
                    await Task.CompletedTask;
                    actualResult = true;
                })
                .Execute();

            // assert
            Assert.AreEqual(expectedResult, actualResult);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_AddParameter_WorksWithBothNullAndNotNullValues()
        {
            // setup
            string expectedNotNullValue = "abcd";
            string expectedNullValue = null;
            string actualNotNullValue = null;
            string actualNullValue = "not a null value";

            var sql = new Sql("TychoSqlTests");

            // act
            sql.Statement("select @notNullValue 'nnv', @nullValue 'nv'")
                .AddParameter("@notNullValue", expectedNotNullValue, SqlDbType.VarChar)
                .AddParameter("@nullValue", expectedNullValue, SqlDbType.VarChar)
                .ExecuteReader(dr =>
                {
                    actualNotNullValue = dr.GetString("nnv");
                    actualNullValue = dr.GetString("nv");
                });

            // assert
            Assert.AreEqual(expectedNotNullValue, actualNotNullValue);
            Assert.AreEqual(expectedNullValue, actualNullValue);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_Execute_AddOutputParameter_WorksWithBOthNullAndNotNullInitialValues()
        {
            // setup
            int? initialNotNullValue = 2;
            int? initialNullValue = null;
            int initialValueTypeValue = 5;
            int? initialNullOutput = 25;

            int? expectedNotNullValue = 3;
            int? expectedNullValue = 99;
            int expectedValueTypeValue = 10;
            int? expectedNullOutput = null;

            int? actualNotNullValue = null;
            int? actualNullValue = null;
            int actualValueTypeValue = 0;
            int? actualNullOutput = 9999;

            var sql = new Sql("TychoSqlTests");

            // act
            sql.Statement(@"
                    SET @notNullValue = @notNullValue + 1; 
                    SET @nullValue = ISNULL(@nullValue, 99);
                    SET @valueTypeValue = @valueTypeValue * 2;
                    SET @nullOutput = null;")
                .AddOutputParameter("@notNullValue", SqlDbType.Int, (t) => actualNotNullValue = t, initialNotNullValue)
                .AddOutputParameter("@nullValue", SqlDbType.Int, (t) => actualNullValue = t, initialNullValue)
                .AddOutputParameter("@valueTypeValue", SqlDbType.Int, (t) => actualValueTypeValue = t, initialValueTypeValue)
                .AddOutputParameter("@nullOutput", SqlDbType.Int, (t) => actualNullOutput = t, initialNullOutput)
                .Execute();

            // assert
            Assert.AreEqual(expectedNotNullValue, actualNotNullValue);
            Assert.AreEqual(expectedNullValue, actualNullValue);
            Assert.AreEqual(expectedValueTypeValue, actualValueTypeValue);
            Assert.AreEqual(expectedNullOutput, actualNullOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_Execute_AddOutputParameter_WithSize_WorksWithBOthNullAndNotNullInitialValues()
        {
            // setup
            string initialNotNullValue = "value";
            string initialNullValue = null;
            string initialNullOutput = "other value";

            string expectedNotNullValue = "value output";
            string expectedNullValue = "shotput";
            string expectedNullOutput = null;

            string actualNotNullValue = null;
            string actualNullValue = null;
            string actualNullOutput = "x";

            var sql = new Sql("TychoSqlTests");

            // act
            sql.Statement(@"
                    SET @notNullValue = CONCAT(@notNullValue, ' output'); 
                    SET @nullValue = ISNULL(@nullValue, 'shotput');
                    SET @nullOutput = null;")
                .AddOutputParameter(
                    Name: "@notNullValue",
                    Type: SqlDbType.VarChar,
                    Size: 20,
                    OutputDelegate: (t) => actualNotNullValue = t,
                    InitialValue: initialNotNullValue)
                .AddOutputParameter("@nullValue", SqlDbType.VarChar, 20, (t) => actualNullValue = t, initialNullValue)
                .AddOutputParameter("@nullOutput", SqlDbType.VarChar, 20, (t) => actualNullOutput = t, initialNullOutput)
                .Execute();

            // assert
            Assert.AreEqual(expectedNotNullValue, actualNotNullValue);
            Assert.AreEqual(expectedNullValue, actualNullValue);
            Assert.AreEqual(expectedNullOutput, actualNullOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_Execute_AddOutputParameter_WithPrecisionAndScale_WorksWithBOthNullAndNotNullInitialValues()
        {
            // setup
            double? initialNotNullValue = 2;
            double? initialNullValue = null;
            double initialValueTypeValue = 5;
            double? initialNullOutput = 25;

            double? expectedNotNullValue = 3;
            double? expectedNullValue = 99;
            double expectedValueTypeValue = 10;
            double? expectedNullOutput = null;

            double? actualNotNullValue = null;
            double? actualNullValue = null;
            double actualValueTypeValue = 0;
            double? actualNullOutput = 9999;

            var sql = new Sql("TychoSqlTests");

            // act
            sql.Statement(@"
                    SET @notNullValue = @notNullValue + 1; 
                    SET @nullValue = ISNULL(@nullValue, 99);
                    SET @valueTypeValue = @valueTypeValue * 2;
                    SET @nullOutput = null;")
                .AddOutputParameter(
                    Name: "@notNullValue", 
                    Type: SqlDbType.Float, 
                    Precision: 20, 
                    Scale: 20,
                    OutputDelegate: (t) => actualNotNullValue = t, 
                    InitialValue: initialNotNullValue)
                .AddOutputParameter(
                    Name: "@nullValue", 
                    Type: SqlDbType.Float, 
                    Precision: 20, 
                    Scale: 20,
                    OutputDelegate: (t) => actualNullValue = t, 
                    InitialValue: initialNullValue)
                .AddOutputParameter("@valueTypeValue", SqlDbType.Float, 20, 20, (t) => actualValueTypeValue = t, initialValueTypeValue)
                .AddOutputParameter("@nullOutput", SqlDbType.Float, 20, 20, (t) => actualNullOutput = t, initialNullOutput)
                .Execute();

            // assert
            Assert.AreEqual(expectedNotNullValue, actualNotNullValue);
            Assert.AreEqual(expectedNullValue, actualNullValue);
            Assert.AreEqual(expectedValueTypeValue, actualValueTypeValue);
            Assert.AreEqual(expectedNullOutput, actualNullOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_AddTableParameter_WorksWithNullTableValues()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            string expectedValue = null;
            string actualValue = "x";
            var tableValues = new List<TableTypeWithParameters>()
            {
                new TableTypeWithParameters{ Id = null, BooleanValue = false, ThirdValue = expectedValue + "abcdeabcde" },
            };

            // act
            sql.Statement("SELECT * FROM @Name")
                .AddTableParameter("@Name", tableValues)
                .ExecuteReader(dbo =>
                {
                    actualValue = dbo.GetString("SomeName");
                });

            // assert
            Assert.AreEqual(expectedValue, actualValue);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_CanCallSPInCurrentDatabase_WithDefaultSchema()
        {
            // setup [dbo].[TestSP1]
            var sql = new Sql("TychoSqlTests");
            int expectedValue = 1;

            // act
            var actualValue = sql.Procedure("TestSP1")
                .ExecuteScalar<int>();

            // assert
            Assert.AreEqual(expectedValue, actualValue);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_CanCallSPInCurrentDatabase_WithSpecifiedSchema()
        {
            // setup [dbo].[TestSP1]
            var sql = new Sql("TychoSqlTests");
            int expectedValue = 1;

            // act
            var actualValue = sql.Procedure("TestSP1", "dbo")
                .ExecuteScalar<int>();

            // assert
            Assert.AreEqual(expectedValue, actualValue);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_UsesActionTimeout()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;

            // act
            try
            {
                sql.Statement("WAITFOR DELAY '00:00:05'")
                    .SetTimeout(1)
                    .Execute();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Execution Timeout Expired"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_Execute_ReExecutesWhenExceptionHandlerReturnsTrue()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            int expectedCount = 2;
            int actualCount = 0;

            // act
            try
            {
                sql.Statement("select 1 from [TableThatDoesNotExist]")
                    .OnException((ex) =>
                    {
                        if (actualCount < expectedCount)
                        {
                            actualCount++;
                            return true;
                        }
                        else
                            return false;
                    })
                    .Execute();
            }
            catch
            {
                // this will eventually throw an exception that is irrelevant to the test
            }


            // assert
            Assert.AreEqual(expectedCount, actualCount);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteAsync_ReExecutesWhenExceptionHandlerReturnsTrue()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            int expectedCount = 2;
            int actualCount = 0;

            // act
            try
            {
                await sql.Statement("select 1 from [TableThatDoesNotExist]")
                    .OnException((ex) =>
                    {
                        if (actualCount < expectedCount)
                        {
                            actualCount++;
                            return true;
                        }
                        else
                            return false;
                    })
                    .ExecuteAsync();
            }
            catch
            {
                // this will eventually throw an exception that is irrelevant to the test
            }


            // assert
            Assert.AreEqual(expectedCount, actualCount);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteAsync_Calls_OnBeforeCommandExecute_WhenHandlerIsSynchronous()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool expectedResult = true;
            bool actualResult = false;

            // act
            await sql.Statement("select 1")
                .OnBeforeCommandExecute((x) => { actualResult = true; })
                .ExecuteAsync();

            // assert
            Assert.AreEqual(expectedResult, actualResult);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteAsync_Calls_OnBeforeCommandExecute_WhenHandlerIsAsynchronous()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool expectedResult = true;
            bool actualResult = false;

            // act
            await sql.Statement("select 1")
                .OnBeforeCommandExecute(async (x) =>
                {
                    await Task.CompletedTask;
                    actualResult = true;
                })
                .ExecuteAsync();

            // assert
            Assert.AreEqual(expectedResult, actualResult);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteAsync_AddOutputParameter_WorksWithBOthNullAndNotNullInitialValues()
        {
            // setup
            int? initialNotNullValue = 2;
            int? initialNullValue = null;
            int initialValueTypeValue = 5;
            int? initialNullOutput = 25;

            int? expectedNotNullValue = 3;
            int? expectedNullValue = 99;
            int expectedValueTypeValue = 10;
            int? expectedNullOutput = null;

            int? actualNotNullValue = null;
            int? actualNullValue = null;
            int actualValueTypeValue = 0;
            int? actualNullOutput = 9999;

            var sql = new Sql("TychoSqlTests");

            // act
            await sql.Statement(@"
                    SET @notNullValue = @notNullValue + 1; 
                    SET @nullValue = ISNULL(@nullValue, 99);
                    SET @valueTypeValue = @valueTypeValue * 2;
                    SET @nullOutput = null;")
                .AddOutputParameter("@notNullValue", SqlDbType.Int, (t) => actualNotNullValue = t, initialNotNullValue)
                .AddOutputParameter("@nullValue", SqlDbType.Int, (t) => actualNullValue = t, initialNullValue)
                .AddOutputParameter("@valueTypeValue", SqlDbType.Int, (t) => actualValueTypeValue = t, initialValueTypeValue)
                .AddOutputParameter("@nullOutput", SqlDbType.Int, (t) => actualNullOutput = t, initialNullOutput)
                .ExecuteAsync();

            // assert
            Assert.AreEqual(expectedNotNullValue, actualNotNullValue);
            Assert.AreEqual(expectedNullValue, actualNullValue);
            Assert.AreEqual(expectedValueTypeValue, actualValueTypeValue);
            Assert.AreEqual(expectedNullOutput, actualNullOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_Execute_UsesDefaultExceptionHandler_WhenNotSpecified()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;

            // act
            try
            {
                sql.Statement("select 1 from [TableThatDoesNotExist]")
                    .Execute();
            }
            catch (Exception ex)
            {
                // the default exception handler simply throws
                if (ex.Message.StartsWith("Invalid object name"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteAsync_UsesDefaultExceptionHandler_WhenNotSpecified()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            bool caughtException = false;

            // act
            try
            {
                await sql.Statement("select 1 from [TableThatDoesNotExist]")
                    .ExecuteAsync();
            }
            catch (Exception ex)
            {
                // the default exception handler simply throws
                if (ex.Message.StartsWith("Invalid object name"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_Execute_ReturnsRowsAffected()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            // note 
            int expectedRowsAffected = 2;

            // act
            int actualRowsAffected = sql.Statement("INSERT INTO [SimpleTable] ( [col1] ) VALUES ( 1 ), ( 2 )")
                .Execute();

            // assert
            Assert.AreEqual(expectedRowsAffected, actualRowsAffected);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteAsync_ReturnsRowsAffected()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            // note 
            int expectedRowsAffected = 2;

            // act
            int actualRowsAffected = await sql.Statement("INSERT INTO [SimpleTable] ( [col1] ) VALUES ( 1 ), ( 2 )")
                .ExecuteAsync();

            // assert
            Assert.AreEqual(expectedRowsAffected, actualRowsAffected);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteScalar_ReturnsFirstColumnOfFirstRecord()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            // note 
            int expectedOutput = 2;

            // act
            int actualOutput = sql.Statement("SELECT * FROM ( VALUES ( 2, 1 ), ( 4, 3 ) ) AS src ( v1, v2 )")
                .ExecuteScalar<int>();

            // assert
            Assert.AreEqual(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteScalarAsync_ReturnsFirstColumnOfFirstRecord()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            // note 
            int expectedOutput = 2;

            // act
            int actualOutput = await sql.Statement("SELECT * FROM ( VALUES ( 2, 1 ), ( 4, 3 ) ) AS src ( v1, v2 )")
                .ExecuteScalarAsync<int>();

            // assert
            Assert.AreEqual(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_1_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };

            // act
            var actuals = sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_1_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };

            // act
            var actuals = await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_2_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };

            // act
            var actuals = sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_2_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            // note 
            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };

            // act
            var actuals = await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_3_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };

            // act
            var actuals = sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_3_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };

            // act
            var actuals = await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_4_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };

            // act
            var actuals = sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_4_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };

            // act
            var actuals = await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_5_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };
            List<int> expectedOutput5 = new List<int> { 5 };

            // act
            var actuals = sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());
            CollectionAssert.AreEquivalent(expectedOutput5, actuals.Set5.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_5_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };
            List<int> expectedOutput5 = new List<int> { 5 };

            // act
            var actuals = await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());
            CollectionAssert.AreEquivalent(expectedOutput5, actuals.Set5.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_6_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };
            List<int> expectedOutput5 = new List<int> { 5 };
            List<int> expectedOutput6 = new List<int> { 6 };

            // act
            var actuals = sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());
            CollectionAssert.AreEquivalent(expectedOutput5, actuals.Set5.ToList());
            CollectionAssert.AreEquivalent(expectedOutput6, actuals.Set6.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_6_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };
            List<int> expectedOutput5 = new List<int> { 5 };
            List<int> expectedOutput6 = new List<int> { 6 };

            // act
            var actuals = await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());
            CollectionAssert.AreEquivalent(expectedOutput5, actuals.Set5.ToList());
            CollectionAssert.AreEquivalent(expectedOutput6, actuals.Set6.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_7_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };
            List<int> expectedOutput5 = new List<int> { 5 };
            List<int> expectedOutput6 = new List<int> { 6 };
            List<int> expectedOutput7 = new List<int> { 7 };

            // act
            var actuals = sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());
            CollectionAssert.AreEquivalent(expectedOutput5, actuals.Set5.ToList());
            CollectionAssert.AreEquivalent(expectedOutput6, actuals.Set6.ToList());
            CollectionAssert.AreEquivalent(expectedOutput7, actuals.Set7.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_7_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };
            List<int> expectedOutput5 = new List<int> { 5 };
            List<int> expectedOutput6 = new List<int> { 6 };
            List<int> expectedOutput7 = new List<int> { 7 };

            // act
            var actuals = await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());
            CollectionAssert.AreEquivalent(expectedOutput5, actuals.Set5.ToList());
            CollectionAssert.AreEquivalent(expectedOutput6, actuals.Set6.ToList());
            CollectionAssert.AreEquivalent(expectedOutput7, actuals.Set7.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_8_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };
            List<int> expectedOutput5 = new List<int> { 5 };
            List<int> expectedOutput6 = new List<int> { 6 };
            List<int> expectedOutput7 = new List<int> { 7 };
            List<int> expectedOutput8 = new List<int> { 8 };

            // act
            var actuals = sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());
            CollectionAssert.AreEquivalent(expectedOutput5, actuals.Set5.ToList());
            CollectionAssert.AreEquivalent(expectedOutput6, actuals.Set6.ToList());
            CollectionAssert.AreEquivalent(expectedOutput7, actuals.Set7.ToList());
            CollectionAssert.AreEquivalent(expectedOutput8, actuals.Set8.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_8_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };
            List<int> expectedOutput5 = new List<int> { 5 };
            List<int> expectedOutput6 = new List<int> { 6 };
            List<int> expectedOutput7 = new List<int> { 7 };
            List<int> expectedOutput8 = new List<int> { 8 };

            // act
            var actuals = await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());
            CollectionAssert.AreEquivalent(expectedOutput5, actuals.Set5.ToList());
            CollectionAssert.AreEquivalent(expectedOutput6, actuals.Set6.ToList());
            CollectionAssert.AreEquivalent(expectedOutput7, actuals.Set7.ToList());
            CollectionAssert.AreEquivalent(expectedOutput8, actuals.Set8.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_9_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };
            List<int> expectedOutput5 = new List<int> { 5 };
            List<int> expectedOutput6 = new List<int> { 6 };
            List<int> expectedOutput7 = new List<int> { 7 };
            List<int> expectedOutput8 = new List<int> { 8 };
            List<int> expectedOutput9 = new List<int> { 9 };

            // act
            var actuals = sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());
            CollectionAssert.AreEquivalent(expectedOutput5, actuals.Set5.ToList());
            CollectionAssert.AreEquivalent(expectedOutput6, actuals.Set6.ToList());
            CollectionAssert.AreEquivalent(expectedOutput7, actuals.Set7.ToList());
            CollectionAssert.AreEquivalent(expectedOutput8, actuals.Set8.ToList());
            CollectionAssert.AreEquivalent(expectedOutput9, actuals.Set9.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_9_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };
            List<int> expectedOutput5 = new List<int> { 5 };
            List<int> expectedOutput6 = new List<int> { 6 };
            List<int> expectedOutput7 = new List<int> { 7 };
            List<int> expectedOutput8 = new List<int> { 8 };
            List<int> expectedOutput9 = new List<int> { 9 };

            // act
            var actuals = await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());
            CollectionAssert.AreEquivalent(expectedOutput5, actuals.Set5.ToList());
            CollectionAssert.AreEquivalent(expectedOutput6, actuals.Set6.ToList());
            CollectionAssert.AreEquivalent(expectedOutput7, actuals.Set7.ToList());
            CollectionAssert.AreEquivalent(expectedOutput8, actuals.Set8.ToList());
            CollectionAssert.AreEquivalent(expectedOutput9, actuals.Set9.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_10_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };
            List<int> expectedOutput5 = new List<int> { 5 };
            List<int> expectedOutput6 = new List<int> { 6 };
            List<int> expectedOutput7 = new List<int> { 7 };
            List<int> expectedOutput8 = new List<int> { 8 };
            List<int> expectedOutput9 = new List<int> { 9 };
            List<int> expectedOutput10 = new List<int> { 10 };

            // act
            var actuals = sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());
            CollectionAssert.AreEquivalent(expectedOutput5, actuals.Set5.ToList());
            CollectionAssert.AreEquivalent(expectedOutput6, actuals.Set6.ToList());
            CollectionAssert.AreEquivalent(expectedOutput7, actuals.Set7.ToList());
            CollectionAssert.AreEquivalent(expectedOutput8, actuals.Set8.ToList());
            CollectionAssert.AreEquivalent(expectedOutput9, actuals.Set9.ToList());
            CollectionAssert.AreEquivalent(expectedOutput10, actuals.Set10.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_10_Delegate()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput1 = new List<int> { 1 };
            List<int> expectedOutput2 = new List<int> { 2 };
            List<int> expectedOutput3 = new List<int> { 3 };
            List<int> expectedOutput4 = new List<int> { 4 };
            List<int> expectedOutput5 = new List<int> { 5 };
            List<int> expectedOutput6 = new List<int> { 6 };
            List<int> expectedOutput7 = new List<int> { 7 };
            List<int> expectedOutput8 = new List<int> { 8 };
            List<int> expectedOutput9 = new List<int> { 9 };
            List<int> expectedOutput10 = new List<int> { 10 };

            // act
            var actuals = await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value,
                    dr => dr.GetInt32("v").Value);

            // assert
            CollectionAssert.AreEquivalent(expectedOutput1, actuals.Set1.ToList());
            CollectionAssert.AreEquivalent(expectedOutput2, actuals.Set2.ToList());
            CollectionAssert.AreEquivalent(expectedOutput3, actuals.Set3.ToList());
            CollectionAssert.AreEquivalent(expectedOutput4, actuals.Set4.ToList());
            CollectionAssert.AreEquivalent(expectedOutput5, actuals.Set5.ToList());
            CollectionAssert.AreEquivalent(expectedOutput6, actuals.Set6.ToList());
            CollectionAssert.AreEquivalent(expectedOutput7, actuals.Set7.ToList());
            CollectionAssert.AreEquivalent(expectedOutput8, actuals.Set8.ToList());
            CollectionAssert.AreEquivalent(expectedOutput9, actuals.Set9.ToList());
            CollectionAssert.AreEquivalent(expectedOutput10, actuals.Set10.ToList());

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_1_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1 };
            List<int> actualOutput = new List<int>();

            // act
            sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_1_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1 };
            List<int> actualOutput = new List<int>();

            // act
            await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_2_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2 };
            List<int> actualOutput = new List<int>();

            // act
            sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_2_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2 };
            List<int> actualOutput = new List<int>();

            // act
            await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_3_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3 };
            List<int> actualOutput = new List<int>();

            // act
            sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_3_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3 };
            List<int> actualOutput = new List<int>();

            // act
            await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_4_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4 };
            List<int> actualOutput = new List<int>();

            // act
            sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_4_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4 };
            List<int> actualOutput = new List<int>();

            // act
            await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_5_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4, 5 };
            List<int> actualOutput = new List<int>();

            // act
            sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_5_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4, 5 };
            List<int> actualOutput = new List<int>();

            // act
            await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_6_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4, 5, 6 };
            List<int> actualOutput = new List<int>();

            // act
            sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_6_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4, 5, 6 };
            List<int> actualOutput = new List<int>();

            // act
            await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_7_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4, 5, 6, 7 };
            List<int> actualOutput = new List<int>();

            // act
            sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_7_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4, 5, 6, 7 };
            List<int> actualOutput = new List<int>();

            // act
            await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_8_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8 };
            List<int> actualOutput = new List<int>();

            // act
            sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_8_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8 };
            List<int> actualOutput = new List<int>();

            // act
            await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_9_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            List<int> actualOutput = new List<int>();

            // act
            sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_9_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            List<int> actualOutput = new List<int>();

            // act
            await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlAction_ExecuteReader_WorksWith_10_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            List<int> actualOutput = new List<int>();

            // act
            sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReader(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public async Task SqlAction_ExecuteReaderAsync_WorksWith_10_Action()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            List<int> expectedOutput = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            List<int> actualOutput = new List<int>();

            // act
            await sql.Statement(@"
                SELECT  1 as 'v';
                SELECT  2 as 'v';
                SELECT  3 as 'v';
                SELECT  4 as 'v';
                SELECT  5 as 'v';
                SELECT  6 as 'v';
                SELECT  7 as 'v';
                SELECT  8 as 'v';
                SELECT  9 as 'v';
                SELECT 10 as 'v';")
                .ExecuteReaderAsync(
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); },
                    dr => { actualOutput.Add(dr.GetInt32("v").Value); });

            // assert
            CollectionAssert.AreEquivalent(expectedOutput, actualOutput);

            // cleanup
        }

    }
}
