﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tycho.Sql.Tests
{
    [TestClass, ExcludeFromCodeCoverage]
    public class SqlReaderTests
    {
        private readonly string _predefinedStatement = @"
            SELECT
		            [String1]            = CONVERT(VARCHAR(12), 'string value')
	               ,[String2]            = CONVERT(NVARCHAR(12), 'string value')
	               ,[String3]            = CONVERT(CHAR(12), 'string value')
	               ,[String4]            = CONVERT(NCHAR(12), 'string value')
	               ,[String5]            = CONVERT(TEXT, 'string value')
	               ,[String6]            = CONVERT(NTEXT, 'string value')
	               ,[String7]            = CONVERT(XML, '<x>string value</x>')
	               ,[DateTime1]          = CONVERT(DATETIME, '11/10/2019', 101)
	               ,[DateTime2]          = CONVERT(DATETIME2, '11/10/2019', 101)
	               ,[DateTime3]          = CONVERT(SMALLDATETIME, '11/10/2019', 101)
	               ,[DateTime4]          = CONVERT(DATE, '11/10/2019', 101)
	               ,[Time]               = CONVERT(TIME, '10:10:10')
	               ,[DateTimeOffset]     = CONVERT(DATETIMEOFFSET, '10:10:10')
	               ,[Float]              = CONVERT(REAL, 12345)
	               ,[Double]             = CONVERT(FLOAT, 12345)
	               ,[Decimal]            = CONVERT(DECIMAL(10,1), 12345)
	               ,[Long]               = CONVERT(BIGINT, 12345)
	               ,[Int]                = CONVERT(INT, 12345)
	               ,[Short]              = CONVERT(SMALLINT, 12345)
	               ,[Byte]               = CONVERT(TINYINT, 123)
	               ,[Bool1]              = CONVERT(BIT, 0)
	               ,[Bool2]              = CONVERT(BIT, 1)
	               ,[Money1]             = CONVERT(MONEY, 12345)
	               ,[Money2]             = CONVERT(SMALLMONEY, 12345)
	               ,[Guid]               = CONVERT(UNIQUEIDENTIFIER, '4FBCDD83-3406-413C-850D-41B12242B4C3')
	               ,[ByteStream1]        = CONVERT(BINARY(2), 0x1112)
	               ,[ByteStream2]        = CONVERT(VARBINARY(4), 0x1112)
	               ,[ByteStream3]        = CONVERT(IMAGE, 0x1112)
	               ,[ByteStream4]        = CONVERT(SQL_VARIANT, 0x1112)
                   ,[NullString1]        = CONVERT(VARCHAR(12), null)
	               ,[NullString2]        = CONVERT(NVARCHAR(12), null)
	               ,[NullString3]        = CONVERT(CHAR(12), null)
	               ,[NullString4]        = CONVERT(NCHAR(12), null)
	               ,[NullString5]        = CONVERT(TEXT, null)
	               ,[NullString6]        = CONVERT(NTEXT, null)
	               ,[NullString7]        = CONVERT(XML, null)
	               ,[NullDateTime1]      = CONVERT(DATETIME, null)
	               ,[NullDateTime2]      = CONVERT(DATETIME2, null)
	               ,[NullDateTime3]      = CONVERT(SMALLDATETIME, null)
	               ,[NullDateTime4]      = CONVERT(DATE, null)
	               ,[NullTime]           = CONVERT(TIME, null)
	               ,[NullDateTimeOffset] = CONVERT(DATETIMEOFFSET, null)
	               ,[NullFloat]          = CONVERT(REAL, null)
	               ,[NullDouble]         = CONVERT(FLOAT, null)
	               ,[NullDecimal]        = CONVERT(DECIMAL(10,1), null)
	               ,[NullLong]           = CONVERT(BIGINT, null)
	               ,[NullInt]            = CONVERT(INT, null)
	               ,[NullShort]          = CONVERT(SMALLINT, null)
	               ,[NullByte]           = CONVERT(TINYINT, null)
	               ,[NullBool]           = CONVERT(BIT, null)
	               ,[NullMoney1]         = CONVERT(MONEY, null)
	               ,[NullMoney2]         = CONVERT(SMALLMONEY, null)
	               ,[NullGuid]           = CONVERT(UNIQUEIDENTIFIER, null)
	               ,[NullByteStream1]    = CONVERT(BINARY(2), null)
	               ,[NullByteStream2]    = CONVERT(VARBINARY(4), null)
	               ,[NullByteStream3]    = CONVERT(IMAGE, null)
	               ,[NullByteStream4]    = CONVERT(SQL_VARIANT, null)
                   ,[StringContainingInt] = CONVERT(VARCHAR(12), '12345')
                   ,[StringContainingDate] = CONVERT(VARCHAR(30), '11/10/2019')
                   ,[StringContainingTimeSpan] = CONVERT(VARCHAR(12), '10:10:10')
                   ,[StringContainingGuid] = CONVERT(VARCHAR(50), '4FBCDD83-3406-413C-850D-41B12242B4C3')
,[StringContainingEnum] = CONVERT(VARCHAR(10), 'Value2')
,[LongAsEnum] = CONVERT(BIGINT, 1)
,[IntAsEnum] = CONVERT(INT, 2)
,[ShortAsEnum] = CONVERT(SMALLINT, 3)
,[ByteAsEnum] = CONVERT(TINYINT, 4)
;";

        public enum TestEnum
        {
            Default,
            Value1,
            Value2,
            Value3,
            Value4,
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetString_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            var expectedValues = new List<string>
            {
                // not null values
                "string value",
                "string value",
                "string value",
                "string value",
                "string value",
                "string value",
                "<x>string value</x>",
                "11/10/2019 12:00:00 AM",
                "11/10/2019 12:00:00 AM",
                "11/10/2019 12:00:00 AM",
                "11/10/2019 12:00:00 AM",
                "10:10:10",
                "1/1/1900 10:10:10 AM +00:00",
                "12345",
                "12345",
                "12345.0",
                "12345",
                "12345",
                "12345",
                "123",
                "0",
                "1",
                "12345.00",
                "12345.00",
                "4fbcdd83-3406-413c-850d-41b12242b4c3",
                null,
                null,
                null,
                null,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            };
            var actualValues = new List<string>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetString("String1"));
                    actualValues.Add(dr.GetString("String2"));
                    actualValues.Add(dr.GetString("String3"));
                    actualValues.Add(dr.GetString("String4"));
                    actualValues.Add(dr.GetString("String5"));
                    actualValues.Add(dr.GetString("String6"));
                    actualValues.Add(dr.GetString("String7"));
                    actualValues.Add(dr.GetString("DateTime1"));
                    actualValues.Add(dr.GetString("DateTime2"));
                    actualValues.Add(dr.GetString("DateTime3"));
                    actualValues.Add(dr.GetString("DateTime4"));
                    actualValues.Add(dr.GetString("Time"));
                    actualValues.Add(dr.GetString("DateTimeOffset"));
                    actualValues.Add(dr.GetString("Float"));
                    actualValues.Add(dr.GetString("Double"));
                    actualValues.Add(dr.GetString("Decimal"));
                    actualValues.Add(dr.GetString("Long"));
                    actualValues.Add(dr.GetString("Int"));
                    actualValues.Add(dr.GetString("Short"));
                    actualValues.Add(dr.GetString("Byte"));
                    actualValues.Add(dr.GetString("Bool1"));
                    actualValues.Add(dr.GetString("Bool2"));
                    actualValues.Add(dr.GetString("Money1"));
                    actualValues.Add(dr.GetString("Money2"));
                    actualValues.Add(dr.GetString("Guid"));
                    actualValues.Add(dr.GetString("ByteStream1"));
                    actualValues.Add(dr.GetString("ByteStream2"));
                    actualValues.Add(dr.GetString("ByteStream3"));
                    actualValues.Add(dr.GetString("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetString("NullString1"));
                    actualValues.Add(dr.GetString("NullString2"));
                    actualValues.Add(dr.GetString("NullString3"));
                    actualValues.Add(dr.GetString("NullString4"));
                    actualValues.Add(dr.GetString("NullString5"));
                    actualValues.Add(dr.GetString("NullString6"));
                    actualValues.Add(dr.GetString("NullString7"));
                    actualValues.Add(dr.GetString("NullDateTime1"));
                    actualValues.Add(dr.GetString("NullDateTime2"));
                    actualValues.Add(dr.GetString("NullDateTime3"));
                    actualValues.Add(dr.GetString("NullDateTime4"));
                    actualValues.Add(dr.GetString("NullTime"));
                    actualValues.Add(dr.GetString("NullDateTimeOffset"));
                    actualValues.Add(dr.GetString("NullFloat"));
                    actualValues.Add(dr.GetString("NullDouble"));
                    actualValues.Add(dr.GetString("NullDecimal"));
                    actualValues.Add(dr.GetString("NullLong"));
                    actualValues.Add(dr.GetString("NullInt"));
                    actualValues.Add(dr.GetString("NullShort"));
                    actualValues.Add(dr.GetString("NullByte"));
                    actualValues.Add(dr.GetString("NullBool"));
                    actualValues.Add(dr.GetString("NullMoney1"));
                    actualValues.Add(dr.GetString("NullMoney2"));
                    actualValues.Add(dr.GetString("NullGuid"));
                    actualValues.Add(dr.GetString("NullByteStream1"));
                    actualValues.Add(dr.GetString("NullByteStream2"));
                    actualValues.Add(dr.GetString("NullByteStream3"));
                    actualValues.Add(dr.GetString("NullByteStream4"));
                });

            // assert
            CollectionAssert.AreEqual(expectedValues, actualValues);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetInt64_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            var expectedValues = new List<long?>
            {
                // not null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                12345,
                12345,
                12345,
                12345,
                12345,
                12345,
                123,
                0,
                1,
                12345,
                12345,
                null,
                null,
                null,
                null,
                null,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                12345,
            };
            var actualValues = new List<long?>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetInt64("String1"));
                    actualValues.Add(dr.GetInt64("String2"));
                    actualValues.Add(dr.GetInt64("String3"));
                    actualValues.Add(dr.GetInt64("String4"));
                    actualValues.Add(dr.GetInt64("String5"));
                    actualValues.Add(dr.GetInt64("String6"));
                    actualValues.Add(dr.GetInt64("String7"));
                    actualValues.Add(dr.GetInt64("DateTime1"));
                    actualValues.Add(dr.GetInt64("DateTime2"));
                    actualValues.Add(dr.GetInt64("DateTime3"));
                    actualValues.Add(dr.GetInt64("DateTime4"));
                    actualValues.Add(dr.GetInt64("Time"));
                    actualValues.Add(dr.GetInt64("DateTimeOffset"));
                    actualValues.Add(dr.GetInt64("Float"));
                    actualValues.Add(dr.GetInt64("Double"));
                    actualValues.Add(dr.GetInt64("Decimal"));
                    actualValues.Add(dr.GetInt64("Long"));
                    actualValues.Add(dr.GetInt64("Int"));
                    actualValues.Add(dr.GetInt64("Short"));
                    actualValues.Add(dr.GetInt64("Byte"));
                    actualValues.Add(dr.GetInt64("Bool1"));
                    actualValues.Add(dr.GetInt64("Bool2"));
                    actualValues.Add(dr.GetInt64("Money1"));
                    actualValues.Add(dr.GetInt64("Money2"));
                    actualValues.Add(dr.GetInt64("Guid"));
                    actualValues.Add(dr.GetInt64("ByteStream1"));
                    actualValues.Add(dr.GetInt64("ByteStream2"));
                    actualValues.Add(dr.GetInt64("ByteStream3"));
                    actualValues.Add(dr.GetInt64("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetInt64("NullString1"));
                    actualValues.Add(dr.GetInt64("NullString2"));
                    actualValues.Add(dr.GetInt64("NullString3"));
                    actualValues.Add(dr.GetInt64("NullString4"));
                    actualValues.Add(dr.GetInt64("NullString5"));
                    actualValues.Add(dr.GetInt64("NullString6"));
                    actualValues.Add(dr.GetInt64("NullString7"));
                    actualValues.Add(dr.GetInt64("NullDateTime1"));
                    actualValues.Add(dr.GetInt64("NullDateTime2"));
                    actualValues.Add(dr.GetInt64("NullDateTime3"));
                    actualValues.Add(dr.GetInt64("NullDateTime4"));
                    actualValues.Add(dr.GetInt64("NullTime"));
                    actualValues.Add(dr.GetInt64("NullDateTimeOffset"));
                    actualValues.Add(dr.GetInt64("NullFloat"));
                    actualValues.Add(dr.GetInt64("NullDouble"));
                    actualValues.Add(dr.GetInt64("NullDecimal"));
                    actualValues.Add(dr.GetInt64("NullLong"));
                    actualValues.Add(dr.GetInt64("NullInt"));
                    actualValues.Add(dr.GetInt64("NullShort"));
                    actualValues.Add(dr.GetInt64("NullByte"));
                    actualValues.Add(dr.GetInt64("NullBool"));
                    actualValues.Add(dr.GetInt64("NullMoney1"));
                    actualValues.Add(dr.GetInt64("NullMoney2"));
                    actualValues.Add(dr.GetInt64("NullGuid"));
                    actualValues.Add(dr.GetInt64("NullByteStream1"));
                    actualValues.Add(dr.GetInt64("NullByteStream2"));
                    actualValues.Add(dr.GetInt64("NullByteStream3"));
                    actualValues.Add(dr.GetInt64("NullByteStream4"));

                    actualValues.Add(dr.GetInt64("StringContainingInt"));
                });

            // assert
            CollectionAssert.AreEqual(expectedValues, actualValues);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetInt32_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            var expectedValues = new List<int?>
            {
                // not null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                12345,
                12345,
                12345,
                12345,
                12345,
                12345,
                123,
                0,
                1,
                12345,
                12345,
                null,
                null,
                null,
                null,
                null,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                12345,
            };
            var actualValues = new List<int?>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetInt32("String1"));
                    actualValues.Add(dr.GetInt32("String2"));
                    actualValues.Add(dr.GetInt32("String3"));
                    actualValues.Add(dr.GetInt32("String4"));
                    actualValues.Add(dr.GetInt32("String5"));
                    actualValues.Add(dr.GetInt32("String6"));
                    actualValues.Add(dr.GetInt32("String7"));
                    actualValues.Add(dr.GetInt32("DateTime1"));
                    actualValues.Add(dr.GetInt32("DateTime2"));
                    actualValues.Add(dr.GetInt32("DateTime3"));
                    actualValues.Add(dr.GetInt32("DateTime4"));
                    actualValues.Add(dr.GetInt32("Time"));
                    actualValues.Add(dr.GetInt32("DateTimeOffset"));
                    actualValues.Add(dr.GetInt32("Float"));
                    actualValues.Add(dr.GetInt32("Double"));
                    actualValues.Add(dr.GetInt32("Decimal"));
                    actualValues.Add(dr.GetInt32("Long"));
                    actualValues.Add(dr.GetInt32("Int"));
                    actualValues.Add(dr.GetInt32("Short"));
                    actualValues.Add(dr.GetInt32("Byte"));
                    actualValues.Add(dr.GetInt32("Bool1"));
                    actualValues.Add(dr.GetInt32("Bool2"));
                    actualValues.Add(dr.GetInt32("Money1"));
                    actualValues.Add(dr.GetInt32("Money2"));
                    actualValues.Add(dr.GetInt32("Guid"));
                    actualValues.Add(dr.GetInt32("ByteStream1"));
                    actualValues.Add(dr.GetInt32("ByteStream2"));
                    actualValues.Add(dr.GetInt32("ByteStream3"));
                    actualValues.Add(dr.GetInt32("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetInt32("NullString1"));
                    actualValues.Add(dr.GetInt32("NullString2"));
                    actualValues.Add(dr.GetInt32("NullString3"));
                    actualValues.Add(dr.GetInt32("NullString4"));
                    actualValues.Add(dr.GetInt32("NullString5"));
                    actualValues.Add(dr.GetInt32("NullString6"));
                    actualValues.Add(dr.GetInt32("NullString7"));
                    actualValues.Add(dr.GetInt32("NullDateTime1"));
                    actualValues.Add(dr.GetInt32("NullDateTime2"));
                    actualValues.Add(dr.GetInt32("NullDateTime3"));
                    actualValues.Add(dr.GetInt32("NullDateTime4"));
                    actualValues.Add(dr.GetInt32("NullTime"));
                    actualValues.Add(dr.GetInt32("NullDateTimeOffset"));
                    actualValues.Add(dr.GetInt32("NullFloat"));
                    actualValues.Add(dr.GetInt32("NullDouble"));
                    actualValues.Add(dr.GetInt32("NullDecimal"));
                    actualValues.Add(dr.GetInt32("NullLong"));
                    actualValues.Add(dr.GetInt32("NullInt"));
                    actualValues.Add(dr.GetInt32("NullShort"));
                    actualValues.Add(dr.GetInt32("NullByte"));
                    actualValues.Add(dr.GetInt32("NullBool"));
                    actualValues.Add(dr.GetInt32("NullMoney1"));
                    actualValues.Add(dr.GetInt32("NullMoney2"));
                    actualValues.Add(dr.GetInt32("NullGuid"));
                    actualValues.Add(dr.GetInt32("NullByteStream1"));
                    actualValues.Add(dr.GetInt32("NullByteStream2"));
                    actualValues.Add(dr.GetInt32("NullByteStream3"));
                    actualValues.Add(dr.GetInt32("NullByteStream4"));

                    actualValues.Add(dr.GetInt32("StringContainingInt"));
                });

            // assert
            CollectionAssert.AreEqual(expectedValues, actualValues);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetInt16_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            var expectedValues = new List<short?>
            {
                // not null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                12345,
                12345,
                12345,
                12345,
                12345,
                12345,
                123,
                0,
                1,
                12345,
                12345,
                null,
                null,
                null,
                null,
                null,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                12345,
            };
            var actualValues = new List<short?>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetInt16("String1"));
                    actualValues.Add(dr.GetInt16("String2"));
                    actualValues.Add(dr.GetInt16("String3"));
                    actualValues.Add(dr.GetInt16("String4"));
                    actualValues.Add(dr.GetInt16("String5"));
                    actualValues.Add(dr.GetInt16("String6"));
                    actualValues.Add(dr.GetInt16("String7"));
                    actualValues.Add(dr.GetInt16("DateTime1"));
                    actualValues.Add(dr.GetInt16("DateTime2"));
                    actualValues.Add(dr.GetInt16("DateTime3"));
                    actualValues.Add(dr.GetInt16("DateTime4"));
                    actualValues.Add(dr.GetInt16("Time"));
                    actualValues.Add(dr.GetInt16("DateTimeOffset"));
                    actualValues.Add(dr.GetInt16("Float"));
                    actualValues.Add(dr.GetInt16("Double"));
                    actualValues.Add(dr.GetInt16("Decimal"));
                    actualValues.Add(dr.GetInt16("Long"));
                    actualValues.Add(dr.GetInt16("Int"));
                    actualValues.Add(dr.GetInt16("Short"));
                    actualValues.Add(dr.GetInt16("Byte"));
                    actualValues.Add(dr.GetInt16("Bool1"));
                    actualValues.Add(dr.GetInt16("Bool2"));
                    actualValues.Add(dr.GetInt16("Money1"));
                    actualValues.Add(dr.GetInt16("Money2"));
                    actualValues.Add(dr.GetInt16("Guid"));
                    actualValues.Add(dr.GetInt16("ByteStream1"));
                    actualValues.Add(dr.GetInt16("ByteStream2"));
                    actualValues.Add(dr.GetInt16("ByteStream3"));
                    actualValues.Add(dr.GetInt16("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetInt16("NullString1"));
                    actualValues.Add(dr.GetInt16("NullString2"));
                    actualValues.Add(dr.GetInt16("NullString3"));
                    actualValues.Add(dr.GetInt16("NullString4"));
                    actualValues.Add(dr.GetInt16("NullString5"));
                    actualValues.Add(dr.GetInt16("NullString6"));
                    actualValues.Add(dr.GetInt16("NullString7"));
                    actualValues.Add(dr.GetInt16("NullDateTime1"));
                    actualValues.Add(dr.GetInt16("NullDateTime2"));
                    actualValues.Add(dr.GetInt16("NullDateTime3"));
                    actualValues.Add(dr.GetInt16("NullDateTime4"));
                    actualValues.Add(dr.GetInt16("NullTime"));
                    actualValues.Add(dr.GetInt16("NullDateTimeOffset"));
                    actualValues.Add(dr.GetInt16("NullFloat"));
                    actualValues.Add(dr.GetInt16("NullDouble"));
                    actualValues.Add(dr.GetInt16("NullDecimal"));
                    actualValues.Add(dr.GetInt16("NullLong"));
                    actualValues.Add(dr.GetInt16("NullInt"));
                    actualValues.Add(dr.GetInt16("NullShort"));
                    actualValues.Add(dr.GetInt16("NullByte"));
                    actualValues.Add(dr.GetInt16("NullBool"));
                    actualValues.Add(dr.GetInt16("NullMoney1"));
                    actualValues.Add(dr.GetInt16("NullMoney2"));
                    actualValues.Add(dr.GetInt16("NullGuid"));
                    actualValues.Add(dr.GetInt16("NullByteStream1"));
                    actualValues.Add(dr.GetInt16("NullByteStream2"));
                    actualValues.Add(dr.GetInt16("NullByteStream3"));
                    actualValues.Add(dr.GetInt16("NullByteStream4"));

                    actualValues.Add(dr.GetInt16("StringContainingInt"));
                });

            // assert
            CollectionAssert.AreEqual(expectedValues, actualValues);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetDouble_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            var expectedValues = new List<double?>
            {
                // not null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                12345,
                12345,
                12345,
                12345,
                12345,
                12345,
                123,
                0,
                1,
                12345,
                12345,
                null,
                null,
                null,
                null,
                null,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                12345,
            };
            var actualValues = new List<double?>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetDouble("String1"));
                    actualValues.Add(dr.GetDouble("String2"));
                    actualValues.Add(dr.GetDouble("String3"));
                    actualValues.Add(dr.GetDouble("String4"));
                    actualValues.Add(dr.GetDouble("String5"));
                    actualValues.Add(dr.GetDouble("String6"));
                    actualValues.Add(dr.GetDouble("String7"));
                    actualValues.Add(dr.GetDouble("DateTime1"));
                    actualValues.Add(dr.GetDouble("DateTime2"));
                    actualValues.Add(dr.GetDouble("DateTime3"));
                    actualValues.Add(dr.GetDouble("DateTime4"));
                    actualValues.Add(dr.GetDouble("Time"));
                    actualValues.Add(dr.GetDouble("DateTimeOffset"));
                    actualValues.Add(dr.GetDouble("Float"));
                    actualValues.Add(dr.GetDouble("Double"));
                    actualValues.Add(dr.GetDouble("Decimal"));
                    actualValues.Add(dr.GetDouble("Long"));
                    actualValues.Add(dr.GetDouble("Int"));
                    actualValues.Add(dr.GetDouble("Short"));
                    actualValues.Add(dr.GetDouble("Byte"));
                    actualValues.Add(dr.GetDouble("Bool1"));
                    actualValues.Add(dr.GetDouble("Bool2"));
                    actualValues.Add(dr.GetDouble("Money1"));
                    actualValues.Add(dr.GetDouble("Money2"));
                    actualValues.Add(dr.GetDouble("Guid"));
                    actualValues.Add(dr.GetDouble("ByteStream1"));
                    actualValues.Add(dr.GetDouble("ByteStream2"));
                    actualValues.Add(dr.GetDouble("ByteStream3"));
                    actualValues.Add(dr.GetDouble("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetDouble("NullString1"));
                    actualValues.Add(dr.GetDouble("NullString2"));
                    actualValues.Add(dr.GetDouble("NullString3"));
                    actualValues.Add(dr.GetDouble("NullString4"));
                    actualValues.Add(dr.GetDouble("NullString5"));
                    actualValues.Add(dr.GetDouble("NullString6"));
                    actualValues.Add(dr.GetDouble("NullString7"));
                    actualValues.Add(dr.GetDouble("NullDateTime1"));
                    actualValues.Add(dr.GetDouble("NullDateTime2"));
                    actualValues.Add(dr.GetDouble("NullDateTime3"));
                    actualValues.Add(dr.GetDouble("NullDateTime4"));
                    actualValues.Add(dr.GetDouble("NullTime"));
                    actualValues.Add(dr.GetDouble("NullDateTimeOffset"));
                    actualValues.Add(dr.GetDouble("NullFloat"));
                    actualValues.Add(dr.GetDouble("NullDouble"));
                    actualValues.Add(dr.GetDouble("NullDecimal"));
                    actualValues.Add(dr.GetDouble("NullLong"));
                    actualValues.Add(dr.GetDouble("NullInt"));
                    actualValues.Add(dr.GetDouble("NullShort"));
                    actualValues.Add(dr.GetDouble("NullByte"));
                    actualValues.Add(dr.GetDouble("NullBool"));
                    actualValues.Add(dr.GetDouble("NullMoney1"));
                    actualValues.Add(dr.GetDouble("NullMoney2"));
                    actualValues.Add(dr.GetDouble("NullGuid"));
                    actualValues.Add(dr.GetDouble("NullByteStream1"));
                    actualValues.Add(dr.GetDouble("NullByteStream2"));
                    actualValues.Add(dr.GetDouble("NullByteStream3"));
                    actualValues.Add(dr.GetDouble("NullByteStream4"));

                    actualValues.Add(dr.GetDouble("StringContainingInt"));
                });

            // assert
            CollectionAssert.AreEqual(expectedValues, actualValues);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetFloat_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            var expectedValues = new List<float?>
            {
                // not null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                12345,
                12345,
                12345,
                12345,
                12345,
                12345,
                123,
                0,
                1,
                12345,
                12345,
                null,
                null,
                null,
                null,
                null,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                12345,
            };
            var actualValues = new List<float?>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetFloat("String1"));
                    actualValues.Add(dr.GetFloat("String2"));
                    actualValues.Add(dr.GetFloat("String3"));
                    actualValues.Add(dr.GetFloat("String4"));
                    actualValues.Add(dr.GetFloat("String5"));
                    actualValues.Add(dr.GetFloat("String6"));
                    actualValues.Add(dr.GetFloat("String7"));
                    actualValues.Add(dr.GetFloat("DateTime1"));
                    actualValues.Add(dr.GetFloat("DateTime2"));
                    actualValues.Add(dr.GetFloat("DateTime3"));
                    actualValues.Add(dr.GetFloat("DateTime4"));
                    actualValues.Add(dr.GetFloat("Time"));
                    actualValues.Add(dr.GetFloat("DateTimeOffset"));
                    actualValues.Add(dr.GetFloat("Float"));
                    actualValues.Add(dr.GetFloat("Double"));
                    actualValues.Add(dr.GetFloat("Decimal"));
                    actualValues.Add(dr.GetFloat("Long"));
                    actualValues.Add(dr.GetFloat("Int"));
                    actualValues.Add(dr.GetFloat("Short"));
                    actualValues.Add(dr.GetFloat("Byte"));
                    actualValues.Add(dr.GetFloat("Bool1"));
                    actualValues.Add(dr.GetFloat("Bool2"));
                    actualValues.Add(dr.GetFloat("Money1"));
                    actualValues.Add(dr.GetFloat("Money2"));
                    actualValues.Add(dr.GetFloat("Guid"));
                    actualValues.Add(dr.GetFloat("ByteStream1"));
                    actualValues.Add(dr.GetFloat("ByteStream2"));
                    actualValues.Add(dr.GetFloat("ByteStream3"));
                    actualValues.Add(dr.GetFloat("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetFloat("NullString1"));
                    actualValues.Add(dr.GetFloat("NullString2"));
                    actualValues.Add(dr.GetFloat("NullString3"));
                    actualValues.Add(dr.GetFloat("NullString4"));
                    actualValues.Add(dr.GetFloat("NullString5"));
                    actualValues.Add(dr.GetFloat("NullString6"));
                    actualValues.Add(dr.GetFloat("NullString7"));
                    actualValues.Add(dr.GetFloat("NullDateTime1"));
                    actualValues.Add(dr.GetFloat("NullDateTime2"));
                    actualValues.Add(dr.GetFloat("NullDateTime3"));
                    actualValues.Add(dr.GetFloat("NullDateTime4"));
                    actualValues.Add(dr.GetFloat("NullTime"));
                    actualValues.Add(dr.GetFloat("NullDateTimeOffset"));
                    actualValues.Add(dr.GetFloat("NullFloat"));
                    actualValues.Add(dr.GetFloat("NullDouble"));
                    actualValues.Add(dr.GetFloat("NullDecimal"));
                    actualValues.Add(dr.GetFloat("NullLong"));
                    actualValues.Add(dr.GetFloat("NullInt"));
                    actualValues.Add(dr.GetFloat("NullShort"));
                    actualValues.Add(dr.GetFloat("NullByte"));
                    actualValues.Add(dr.GetFloat("NullBool"));
                    actualValues.Add(dr.GetFloat("NullMoney1"));
                    actualValues.Add(dr.GetFloat("NullMoney2"));
                    actualValues.Add(dr.GetFloat("NullGuid"));
                    actualValues.Add(dr.GetFloat("NullByteStream1"));
                    actualValues.Add(dr.GetFloat("NullByteStream2"));
                    actualValues.Add(dr.GetFloat("NullByteStream3"));
                    actualValues.Add(dr.GetFloat("NullByteStream4"));

                    actualValues.Add(dr.GetFloat("StringContainingInt"));
                });

            // assert
            CollectionAssert.AreEqual(expectedValues, actualValues);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetDateTime_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            var expectedValues = new List<DateTime?>
            {
                // not null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                DateTime.Parse("11/10/2019 12:00:00 AM"),
                DateTime.Parse("11/10/2019 12:00:00 AM"),
                DateTime.Parse("11/10/2019 12:00:00 AM"),
                DateTime.Parse("11/10/2019 12:00:00 AM"),
                DateTime.Parse("1/1/0001 10:10:10 AM"),
                DateTime.Parse("1/1/1900 10:10:10 AM"),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                DateTime.Parse("11/10/2019 12:00:00 AM"),
            };
            var actualValues = new List<DateTime?>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetDateTime("String1"));
                    actualValues.Add(dr.GetDateTime("String2"));
                    actualValues.Add(dr.GetDateTime("String3"));
                    actualValues.Add(dr.GetDateTime("String4"));
                    actualValues.Add(dr.GetDateTime("String5"));
                    actualValues.Add(dr.GetDateTime("String6"));
                    actualValues.Add(dr.GetDateTime("String7"));
                    actualValues.Add(dr.GetDateTime("DateTime1"));
                    actualValues.Add(dr.GetDateTime("DateTime2"));
                    actualValues.Add(dr.GetDateTime("DateTime3"));
                    actualValues.Add(dr.GetDateTime("DateTime4"));
                    actualValues.Add(dr.GetDateTime("Time"));
                    actualValues.Add(dr.GetDateTime("DateTimeOffset"));
                    actualValues.Add(dr.GetDateTime("Float"));
                    actualValues.Add(dr.GetDateTime("Double"));
                    actualValues.Add(dr.GetDateTime("Decimal"));
                    actualValues.Add(dr.GetDateTime("Long"));
                    actualValues.Add(dr.GetDateTime("Int"));
                    actualValues.Add(dr.GetDateTime("Short"));
                    actualValues.Add(dr.GetDateTime("Byte"));
                    actualValues.Add(dr.GetDateTime("Bool1"));
                    actualValues.Add(dr.GetDateTime("Bool2"));
                    actualValues.Add(dr.GetDateTime("Money1"));
                    actualValues.Add(dr.GetDateTime("Money2"));
                    actualValues.Add(dr.GetDateTime("Guid"));
                    actualValues.Add(dr.GetDateTime("ByteStream1"));
                    actualValues.Add(dr.GetDateTime("ByteStream2"));
                    actualValues.Add(dr.GetDateTime("ByteStream3"));
                    actualValues.Add(dr.GetDateTime("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetDateTime("NullString1"));
                    actualValues.Add(dr.GetDateTime("NullString2"));
                    actualValues.Add(dr.GetDateTime("NullString3"));
                    actualValues.Add(dr.GetDateTime("NullString4"));
                    actualValues.Add(dr.GetDateTime("NullString5"));
                    actualValues.Add(dr.GetDateTime("NullString6"));
                    actualValues.Add(dr.GetDateTime("NullString7"));
                    actualValues.Add(dr.GetDateTime("NullDateTime1"));
                    actualValues.Add(dr.GetDateTime("NullDateTime2"));
                    actualValues.Add(dr.GetDateTime("NullDateTime3"));
                    actualValues.Add(dr.GetDateTime("NullDateTime4"));
                    actualValues.Add(dr.GetDateTime("NullTime"));
                    actualValues.Add(dr.GetDateTime("NullDateTimeOffset"));
                    actualValues.Add(dr.GetDateTime("NullFloat"));
                    actualValues.Add(dr.GetDateTime("NullDouble"));
                    actualValues.Add(dr.GetDateTime("NullDecimal"));
                    actualValues.Add(dr.GetDateTime("NullLong"));
                    actualValues.Add(dr.GetDateTime("NullInt"));
                    actualValues.Add(dr.GetDateTime("NullShort"));
                    actualValues.Add(dr.GetDateTime("NullByte"));
                    actualValues.Add(dr.GetDateTime("NullBool"));
                    actualValues.Add(dr.GetDateTime("NullMoney1"));
                    actualValues.Add(dr.GetDateTime("NullMoney2"));
                    actualValues.Add(dr.GetDateTime("NullGuid"));
                    actualValues.Add(dr.GetDateTime("NullByteStream1"));
                    actualValues.Add(dr.GetDateTime("NullByteStream2"));
                    actualValues.Add(dr.GetDateTime("NullByteStream3"));
                    actualValues.Add(dr.GetDateTime("NullByteStream4"));

                    actualValues.Add(dr.GetDateTime("StringContainingDate"));
                });

            // assert
            CollectionAssert.AreEqual(expectedValues, actualValues);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetDateTimeOffset_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            var expectedValues = new List<DateTimeOffset?>
            {
                // not null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                DateTimeOffset.Parse("11/10/2019 12:00:00 AM"),
                DateTimeOffset.Parse("11/10/2019 12:00:00 AM"),
                DateTimeOffset.Parse("11/10/2019 12:00:00 AM"),
                DateTimeOffset.Parse("11/10/2019 12:00:00 AM"),
                DateTimeOffset.Parse("1/1/0001 10:10:10 AM +00:00"),
                DateTimeOffset.Parse("1/1/1900 10:10:10 AM +00:00"),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                DateTimeOffset.Parse("11/10/2019 12:00:00 AM"),
            };
            var actualValues = new List<DateTimeOffset?>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetDateTimeOffset("String1"));
                    actualValues.Add(dr.GetDateTimeOffset("String2"));
                    actualValues.Add(dr.GetDateTimeOffset("String3"));
                    actualValues.Add(dr.GetDateTimeOffset("String4"));
                    actualValues.Add(dr.GetDateTimeOffset("String5"));
                    actualValues.Add(dr.GetDateTimeOffset("String6"));
                    actualValues.Add(dr.GetDateTimeOffset("String7"));
                    actualValues.Add(dr.GetDateTimeOffset("DateTime1"));
                    actualValues.Add(dr.GetDateTimeOffset("DateTime2"));
                    actualValues.Add(dr.GetDateTimeOffset("DateTime3"));
                    actualValues.Add(dr.GetDateTimeOffset("DateTime4"));
                    actualValues.Add(dr.GetDateTimeOffset("Time"));
                    actualValues.Add(dr.GetDateTimeOffset("DateTimeOffset"));
                    actualValues.Add(dr.GetDateTimeOffset("Float"));
                    actualValues.Add(dr.GetDateTimeOffset("Double"));
                    actualValues.Add(dr.GetDateTimeOffset("Decimal"));
                    actualValues.Add(dr.GetDateTimeOffset("Long"));
                    actualValues.Add(dr.GetDateTimeOffset("Int"));
                    actualValues.Add(dr.GetDateTimeOffset("Short"));
                    actualValues.Add(dr.GetDateTimeOffset("Byte"));
                    actualValues.Add(dr.GetDateTimeOffset("Bool1"));
                    actualValues.Add(dr.GetDateTimeOffset("Bool2"));
                    actualValues.Add(dr.GetDateTimeOffset("Money1"));
                    actualValues.Add(dr.GetDateTimeOffset("Money2"));
                    actualValues.Add(dr.GetDateTimeOffset("Guid"));
                    actualValues.Add(dr.GetDateTimeOffset("ByteStream1"));
                    actualValues.Add(dr.GetDateTimeOffset("ByteStream2"));
                    actualValues.Add(dr.GetDateTimeOffset("ByteStream3"));
                    actualValues.Add(dr.GetDateTimeOffset("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetDateTimeOffset("NullString1"));
                    actualValues.Add(dr.GetDateTimeOffset("NullString2"));
                    actualValues.Add(dr.GetDateTimeOffset("NullString3"));
                    actualValues.Add(dr.GetDateTimeOffset("NullString4"));
                    actualValues.Add(dr.GetDateTimeOffset("NullString5"));
                    actualValues.Add(dr.GetDateTimeOffset("NullString6"));
                    actualValues.Add(dr.GetDateTimeOffset("NullString7"));
                    actualValues.Add(dr.GetDateTimeOffset("NullDateTime1"));
                    actualValues.Add(dr.GetDateTimeOffset("NullDateTime2"));
                    actualValues.Add(dr.GetDateTimeOffset("NullDateTime3"));
                    actualValues.Add(dr.GetDateTimeOffset("NullDateTime4"));
                    actualValues.Add(dr.GetDateTimeOffset("NullTime"));
                    actualValues.Add(dr.GetDateTimeOffset("NullDateTimeOffset"));
                    actualValues.Add(dr.GetDateTimeOffset("NullFloat"));
                    actualValues.Add(dr.GetDateTimeOffset("NullDouble"));
                    actualValues.Add(dr.GetDateTimeOffset("NullDecimal"));
                    actualValues.Add(dr.GetDateTimeOffset("NullLong"));
                    actualValues.Add(dr.GetDateTimeOffset("NullInt"));
                    actualValues.Add(dr.GetDateTimeOffset("NullShort"));
                    actualValues.Add(dr.GetDateTimeOffset("NullByte"));
                    actualValues.Add(dr.GetDateTimeOffset("NullBool"));
                    actualValues.Add(dr.GetDateTimeOffset("NullMoney1"));
                    actualValues.Add(dr.GetDateTimeOffset("NullMoney2"));
                    actualValues.Add(dr.GetDateTimeOffset("NullGuid"));
                    actualValues.Add(dr.GetDateTimeOffset("NullByteStream1"));
                    actualValues.Add(dr.GetDateTimeOffset("NullByteStream2"));
                    actualValues.Add(dr.GetDateTimeOffset("NullByteStream3"));
                    actualValues.Add(dr.GetDateTimeOffset("NullByteStream4"));

                    actualValues.Add(dr.GetDateTimeOffset("StringContainingDate"));
                });

            // assert
            CollectionAssert.AreEqual(expectedValues, actualValues);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetTimeSpan_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            var expectedValues = new List<TimeSpan?>
            {
                // not null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                TimeSpan.Parse("00:00:00"),
                TimeSpan.Parse("00:00:00"),
                TimeSpan.Parse("00:00:00"),
                TimeSpan.Parse("00:00:00"),
                TimeSpan.Parse("10:10:10"),
                TimeSpan.Parse("10:10:10"),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                TimeSpan.Parse("10:10:10"),
            };
            var actualValues = new List<TimeSpan?>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetTimeSpan("String1"));
                    actualValues.Add(dr.GetTimeSpan("String2"));
                    actualValues.Add(dr.GetTimeSpan("String3"));
                    actualValues.Add(dr.GetTimeSpan("String4"));
                    actualValues.Add(dr.GetTimeSpan("String5"));
                    actualValues.Add(dr.GetTimeSpan("String6"));
                    actualValues.Add(dr.GetTimeSpan("String7"));
                    actualValues.Add(dr.GetTimeSpan("DateTime1"));
                    actualValues.Add(dr.GetTimeSpan("DateTime2"));
                    actualValues.Add(dr.GetTimeSpan("DateTime3"));
                    actualValues.Add(dr.GetTimeSpan("DateTime4"));
                    actualValues.Add(dr.GetTimeSpan("Time"));
                    actualValues.Add(dr.GetTimeSpan("DateTimeOffset"));
                    actualValues.Add(dr.GetTimeSpan("Float"));
                    actualValues.Add(dr.GetTimeSpan("Double"));
                    actualValues.Add(dr.GetTimeSpan("Decimal"));
                    actualValues.Add(dr.GetTimeSpan("Long"));
                    actualValues.Add(dr.GetTimeSpan("Int"));
                    actualValues.Add(dr.GetTimeSpan("Short"));
                    actualValues.Add(dr.GetTimeSpan("Byte"));
                    actualValues.Add(dr.GetTimeSpan("Bool1"));
                    actualValues.Add(dr.GetTimeSpan("Bool2"));
                    actualValues.Add(dr.GetTimeSpan("Money1"));
                    actualValues.Add(dr.GetTimeSpan("Money2"));
                    actualValues.Add(dr.GetTimeSpan("Guid"));
                    actualValues.Add(dr.GetTimeSpan("ByteStream1"));
                    actualValues.Add(dr.GetTimeSpan("ByteStream2"));
                    actualValues.Add(dr.GetTimeSpan("ByteStream3"));
                    actualValues.Add(dr.GetTimeSpan("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetTimeSpan("NullString1"));
                    actualValues.Add(dr.GetTimeSpan("NullString2"));
                    actualValues.Add(dr.GetTimeSpan("NullString3"));
                    actualValues.Add(dr.GetTimeSpan("NullString4"));
                    actualValues.Add(dr.GetTimeSpan("NullString5"));
                    actualValues.Add(dr.GetTimeSpan("NullString6"));
                    actualValues.Add(dr.GetTimeSpan("NullString7"));
                    actualValues.Add(dr.GetTimeSpan("NullDateTime1"));
                    actualValues.Add(dr.GetTimeSpan("NullDateTime2"));
                    actualValues.Add(dr.GetTimeSpan("NullDateTime3"));
                    actualValues.Add(dr.GetTimeSpan("NullDateTime4"));
                    actualValues.Add(dr.GetTimeSpan("NullTime"));
                    actualValues.Add(dr.GetTimeSpan("NullDateTimeOffset"));
                    actualValues.Add(dr.GetTimeSpan("NullFloat"));
                    actualValues.Add(dr.GetTimeSpan("NullDouble"));
                    actualValues.Add(dr.GetTimeSpan("NullDecimal"));
                    actualValues.Add(dr.GetTimeSpan("NullLong"));
                    actualValues.Add(dr.GetTimeSpan("NullInt"));
                    actualValues.Add(dr.GetTimeSpan("NullShort"));
                    actualValues.Add(dr.GetTimeSpan("NullByte"));
                    actualValues.Add(dr.GetTimeSpan("NullBool"));
                    actualValues.Add(dr.GetTimeSpan("NullMoney1"));
                    actualValues.Add(dr.GetTimeSpan("NullMoney2"));
                    actualValues.Add(dr.GetTimeSpan("NullGuid"));
                    actualValues.Add(dr.GetTimeSpan("NullByteStream1"));
                    actualValues.Add(dr.GetTimeSpan("NullByteStream2"));
                    actualValues.Add(dr.GetTimeSpan("NullByteStream3"));
                    actualValues.Add(dr.GetTimeSpan("NullByteStream4"));

                    actualValues.Add(dr.GetTimeSpan("StringContainingTimeSpan"));
                });

            // assert
            CollectionAssert.AreEqual(expectedValues, actualValues);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetBoolean_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            var expectedValues = new List<bool?>
            {
                // not null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                true,
                true,
                true,
                true,
                true,
                true,
                true,
                false,
                true,
                true,
                true,
                null,
                null,
                null,
                null,
                null,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                true,
            };
            var actualValues = new List<bool?>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetBoolean("String1"));
                    actualValues.Add(dr.GetBoolean("String2"));
                    actualValues.Add(dr.GetBoolean("String3"));
                    actualValues.Add(dr.GetBoolean("String4"));
                    actualValues.Add(dr.GetBoolean("String5"));
                    actualValues.Add(dr.GetBoolean("String6"));
                    actualValues.Add(dr.GetBoolean("String7"));
                    actualValues.Add(dr.GetBoolean("DateTime1"));
                    actualValues.Add(dr.GetBoolean("DateTime2"));
                    actualValues.Add(dr.GetBoolean("DateTime3"));
                    actualValues.Add(dr.GetBoolean("DateTime4"));
                    actualValues.Add(dr.GetBoolean("Time"));
                    actualValues.Add(dr.GetBoolean("DateTimeOffset"));
                    actualValues.Add(dr.GetBoolean("Float"));
                    actualValues.Add(dr.GetBoolean("Double"));
                    actualValues.Add(dr.GetBoolean("Decimal"));
                    actualValues.Add(dr.GetBoolean("Long"));
                    actualValues.Add(dr.GetBoolean("Int"));
                    actualValues.Add(dr.GetBoolean("Short"));
                    actualValues.Add(dr.GetBoolean("Byte"));
                    actualValues.Add(dr.GetBoolean("Bool1"));
                    actualValues.Add(dr.GetBoolean("Bool2"));
                    actualValues.Add(dr.GetBoolean("Money1"));
                    actualValues.Add(dr.GetBoolean("Money2"));
                    actualValues.Add(dr.GetBoolean("Guid"));
                    actualValues.Add(dr.GetBoolean("ByteStream1"));
                    actualValues.Add(dr.GetBoolean("ByteStream2"));
                    actualValues.Add(dr.GetBoolean("ByteStream3"));
                    actualValues.Add(dr.GetBoolean("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetBoolean("NullString1"));
                    actualValues.Add(dr.GetBoolean("NullString2"));
                    actualValues.Add(dr.GetBoolean("NullString3"));
                    actualValues.Add(dr.GetBoolean("NullString4"));
                    actualValues.Add(dr.GetBoolean("NullString5"));
                    actualValues.Add(dr.GetBoolean("NullString6"));
                    actualValues.Add(dr.GetBoolean("NullString7"));
                    actualValues.Add(dr.GetBoolean("NullDateTime1"));
                    actualValues.Add(dr.GetBoolean("NullDateTime2"));
                    actualValues.Add(dr.GetBoolean("NullDateTime3"));
                    actualValues.Add(dr.GetBoolean("NullDateTime4"));
                    actualValues.Add(dr.GetBoolean("NullTime"));
                    actualValues.Add(dr.GetBoolean("NullDateTimeOffset"));
                    actualValues.Add(dr.GetBoolean("NullFloat"));
                    actualValues.Add(dr.GetBoolean("NullDouble"));
                    actualValues.Add(dr.GetBoolean("NullDecimal"));
                    actualValues.Add(dr.GetBoolean("NullLong"));
                    actualValues.Add(dr.GetBoolean("NullInt"));
                    actualValues.Add(dr.GetBoolean("NullShort"));
                    actualValues.Add(dr.GetBoolean("NullByte"));
                    actualValues.Add(dr.GetBoolean("NullBool"));
                    actualValues.Add(dr.GetBoolean("NullMoney1"));
                    actualValues.Add(dr.GetBoolean("NullMoney2"));
                    actualValues.Add(dr.GetBoolean("NullGuid"));
                    actualValues.Add(dr.GetBoolean("NullByteStream1"));
                    actualValues.Add(dr.GetBoolean("NullByteStream2"));
                    actualValues.Add(dr.GetBoolean("NullByteStream3"));
                    actualValues.Add(dr.GetBoolean("NullByteStream4"));

                    actualValues.Add(dr.GetBoolean("StringContainingInt"));
                });

            // assert
            CollectionAssert.AreEqual(expectedValues, actualValues);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetGuid_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            var expectedValues = new List<Guid?>
            {
                // not null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                Guid.Parse("4fbcdd83-3406-413c-850d-41b12242b4c3"),
                null,
                null,
                null,
                null,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                Guid.Parse("4fbcdd83-3406-413c-850d-41b12242b4c3"),
            };
            var actualValues = new List<Guid?>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetGuid("String1"));
                    actualValues.Add(dr.GetGuid("String2"));
                    actualValues.Add(dr.GetGuid("String3"));
                    actualValues.Add(dr.GetGuid("String4"));
                    actualValues.Add(dr.GetGuid("String5"));
                    actualValues.Add(dr.GetGuid("String6"));
                    actualValues.Add(dr.GetGuid("String7"));
                    actualValues.Add(dr.GetGuid("DateTime1"));
                    actualValues.Add(dr.GetGuid("DateTime2"));
                    actualValues.Add(dr.GetGuid("DateTime3"));
                    actualValues.Add(dr.GetGuid("DateTime4"));
                    actualValues.Add(dr.GetGuid("Time"));
                    actualValues.Add(dr.GetGuid("DateTimeOffset"));
                    actualValues.Add(dr.GetGuid("Float"));
                    actualValues.Add(dr.GetGuid("Double"));
                    actualValues.Add(dr.GetGuid("Decimal"));
                    actualValues.Add(dr.GetGuid("Long"));
                    actualValues.Add(dr.GetGuid("Int"));
                    actualValues.Add(dr.GetGuid("Short"));
                    actualValues.Add(dr.GetGuid("Byte"));
                    actualValues.Add(dr.GetGuid("Bool1"));
                    actualValues.Add(dr.GetGuid("Bool2"));
                    actualValues.Add(dr.GetGuid("Money1"));
                    actualValues.Add(dr.GetGuid("Money2"));
                    actualValues.Add(dr.GetGuid("Guid"));
                    actualValues.Add(dr.GetGuid("ByteStream1"));
                    actualValues.Add(dr.GetGuid("ByteStream2"));
                    actualValues.Add(dr.GetGuid("ByteStream3"));
                    actualValues.Add(dr.GetGuid("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetGuid("NullString1"));
                    actualValues.Add(dr.GetGuid("NullString2"));
                    actualValues.Add(dr.GetGuid("NullString3"));
                    actualValues.Add(dr.GetGuid("NullString4"));
                    actualValues.Add(dr.GetGuid("NullString5"));
                    actualValues.Add(dr.GetGuid("NullString6"));
                    actualValues.Add(dr.GetGuid("NullString7"));
                    actualValues.Add(dr.GetGuid("NullDateTime1"));
                    actualValues.Add(dr.GetGuid("NullDateTime2"));
                    actualValues.Add(dr.GetGuid("NullDateTime3"));
                    actualValues.Add(dr.GetGuid("NullDateTime4"));
                    actualValues.Add(dr.GetGuid("NullTime"));
                    actualValues.Add(dr.GetGuid("NullDateTimeOffset"));
                    actualValues.Add(dr.GetGuid("NullFloat"));
                    actualValues.Add(dr.GetGuid("NullDouble"));
                    actualValues.Add(dr.GetGuid("NullDecimal"));
                    actualValues.Add(dr.GetGuid("NullLong"));
                    actualValues.Add(dr.GetGuid("NullInt"));
                    actualValues.Add(dr.GetGuid("NullShort"));
                    actualValues.Add(dr.GetGuid("NullByte"));
                    actualValues.Add(dr.GetGuid("NullBool"));
                    actualValues.Add(dr.GetGuid("NullMoney1"));
                    actualValues.Add(dr.GetGuid("NullMoney2"));
                    actualValues.Add(dr.GetGuid("NullGuid"));
                    actualValues.Add(dr.GetGuid("NullByteStream1"));
                    actualValues.Add(dr.GetGuid("NullByteStream2"));
                    actualValues.Add(dr.GetGuid("NullByteStream3"));
                    actualValues.Add(dr.GetGuid("NullByteStream4"));

                    actualValues.Add(dr.GetGuid("StringContainingGuid"));
                });

            // assert
            CollectionAssert.AreEqual(expectedValues, actualValues);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetEnum_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");
            var expectedValues = new List<TestEnum>
            {
                // not null values
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                // null values
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                TestEnum.Default,
                // specialized values
                TestEnum.Value2,
                TestEnum.Value1,
                TestEnum.Value2,
                TestEnum.Value3,
                TestEnum.Value4,
            };
            var actualValues = new List<TestEnum>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetEnum("String1", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("String2", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("String3", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("String4", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("String5", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("String6", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("String7", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("DateTime1", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("DateTime2", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("DateTime3", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("DateTime4", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("Time", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("DateTimeOffset", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("Float", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("Double", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("Decimal", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("Long", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("Int", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("Short", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("Byte", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("Bool1", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("Bool2", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("Money1", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("Money2", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("Guid", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("ByteStream1", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("ByteStream2", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("ByteStream3", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("ByteStream4", TestEnum.Default));

                    // null values
                    actualValues.Add(dr.GetEnum("NullString1", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullString2", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullString3", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullString4", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullString5", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullString6", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullString7", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullDateTime1", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullDateTime2", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullDateTime3", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullDateTime4", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullTime", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullDateTimeOffset", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullFloat", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullDouble", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullDecimal", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullLong", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullInt", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullShort", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullByte", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullBool", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullMoney1", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullMoney2", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullGuid", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullByteStream1", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullByteStream2", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullByteStream3", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("NullByteStream4", TestEnum.Default));

                    actualValues.Add(dr.GetEnum("StringContainingEnum", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("LongAsEnum", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("IntAsEnum", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("ShortAsEnum", TestEnum.Default));
                    actualValues.Add(dr.GetEnum("ByteAsEnum", TestEnum.Default));
                });

            // assert
            CollectionAssert.AreEqual(expectedValues, actualValues);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetXmlDocument_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            var expectedXml = new XmlDocument();
            expectedXml.LoadXml("<x>string value</x>");

            var expectedValues = new List<XmlDocument>
            {
                // not null values
                null,
                null,
                null,
                null,
                null,
                null,
                expectedXml,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            };
            var actualValues = new List<XmlDocument>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetXmlDocument("String1"));
                    actualValues.Add(dr.GetXmlDocument("String2"));
                    actualValues.Add(dr.GetXmlDocument("String3"));
                    actualValues.Add(dr.GetXmlDocument("String4"));
                    actualValues.Add(dr.GetXmlDocument("String5"));
                    actualValues.Add(dr.GetXmlDocument("String6"));
                    actualValues.Add(dr.GetXmlDocument("String7"));
                    actualValues.Add(dr.GetXmlDocument("DateTime1"));
                    actualValues.Add(dr.GetXmlDocument("DateTime2"));
                    actualValues.Add(dr.GetXmlDocument("DateTime3"));
                    actualValues.Add(dr.GetXmlDocument("DateTime4"));
                    actualValues.Add(dr.GetXmlDocument("Time"));
                    actualValues.Add(dr.GetXmlDocument("DateTimeOffset"));
                    actualValues.Add(dr.GetXmlDocument("Float"));
                    actualValues.Add(dr.GetXmlDocument("Double"));
                    actualValues.Add(dr.GetXmlDocument("Decimal"));
                    actualValues.Add(dr.GetXmlDocument("Long"));
                    actualValues.Add(dr.GetXmlDocument("Int"));
                    actualValues.Add(dr.GetXmlDocument("Short"));
                    actualValues.Add(dr.GetXmlDocument("Byte"));
                    actualValues.Add(dr.GetXmlDocument("Bool1"));
                    actualValues.Add(dr.GetXmlDocument("Bool2"));
                    actualValues.Add(dr.GetXmlDocument("Money1"));
                    actualValues.Add(dr.GetXmlDocument("Money2"));
                    actualValues.Add(dr.GetXmlDocument("Guid"));
                    actualValues.Add(dr.GetXmlDocument("ByteStream1"));
                    actualValues.Add(dr.GetXmlDocument("ByteStream2"));
                    actualValues.Add(dr.GetXmlDocument("ByteStream3"));
                    actualValues.Add(dr.GetXmlDocument("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetXmlDocument("NullString1"));
                    actualValues.Add(dr.GetXmlDocument("NullString2"));
                    actualValues.Add(dr.GetXmlDocument("NullString3"));
                    actualValues.Add(dr.GetXmlDocument("NullString4"));
                    actualValues.Add(dr.GetXmlDocument("NullString5"));
                    actualValues.Add(dr.GetXmlDocument("NullString6"));
                    actualValues.Add(dr.GetXmlDocument("NullString7"));
                    actualValues.Add(dr.GetXmlDocument("NullDateTime1"));
                    actualValues.Add(dr.GetXmlDocument("NullDateTime2"));
                    actualValues.Add(dr.GetXmlDocument("NullDateTime3"));
                    actualValues.Add(dr.GetXmlDocument("NullDateTime4"));
                    actualValues.Add(dr.GetXmlDocument("NullTime"));
                    actualValues.Add(dr.GetXmlDocument("NullDateTimeOffset"));
                    actualValues.Add(dr.GetXmlDocument("NullFloat"));
                    actualValues.Add(dr.GetXmlDocument("NullDouble"));
                    actualValues.Add(dr.GetXmlDocument("NullDecimal"));
                    actualValues.Add(dr.GetXmlDocument("NullLong"));
                    actualValues.Add(dr.GetXmlDocument("NullInt"));
                    actualValues.Add(dr.GetXmlDocument("NullShort"));
                    actualValues.Add(dr.GetXmlDocument("NullByte"));
                    actualValues.Add(dr.GetXmlDocument("NullBool"));
                    actualValues.Add(dr.GetXmlDocument("NullMoney1"));
                    actualValues.Add(dr.GetXmlDocument("NullMoney2"));
                    actualValues.Add(dr.GetXmlDocument("NullGuid"));
                    actualValues.Add(dr.GetXmlDocument("NullByteStream1"));
                    actualValues.Add(dr.GetXmlDocument("NullByteStream2"));
                    actualValues.Add(dr.GetXmlDocument("NullByteStream3"));
                    actualValues.Add(dr.GetXmlDocument("NullByteStream4"));
                });

            // assert
            for(int x = 0; x < actualValues.Count; x++)
            {
                bool bothNull = expectedValues[x] == null && actualValues[x] == null;
                if (!bothNull)
                {
                    Assert.IsNotNull(expectedValues[x]);
                    Assert.IsNotNull(actualValues[x]);
                    var expected = expectedValues[x].OuterXml;
                    var actual = actualValues[x].OuterXml;
                    Assert.AreEqual(expected, actual);
                }
            }

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetXDocument_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            var expectedXml = XDocument.Parse("<x>string value</x>");

            var expectedValues = new List<XDocument>
            {
                // not null values
                null,
                null,
                null,
                null,
                null,
                null,
                expectedXml,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            };
            var actualValues = new List<XDocument>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetXDocument("String1"));
                    actualValues.Add(dr.GetXDocument("String2"));
                    actualValues.Add(dr.GetXDocument("String3"));
                    actualValues.Add(dr.GetXDocument("String4"));
                    actualValues.Add(dr.GetXDocument("String5"));
                    actualValues.Add(dr.GetXDocument("String6"));
                    actualValues.Add(dr.GetXDocument("String7"));
                    actualValues.Add(dr.GetXDocument("DateTime1"));
                    actualValues.Add(dr.GetXDocument("DateTime2"));
                    actualValues.Add(dr.GetXDocument("DateTime3"));
                    actualValues.Add(dr.GetXDocument("DateTime4"));
                    actualValues.Add(dr.GetXDocument("Time"));
                    actualValues.Add(dr.GetXDocument("DateTimeOffset"));
                    actualValues.Add(dr.GetXDocument("Float"));
                    actualValues.Add(dr.GetXDocument("Double"));
                    actualValues.Add(dr.GetXDocument("Decimal"));
                    actualValues.Add(dr.GetXDocument("Long"));
                    actualValues.Add(dr.GetXDocument("Int"));
                    actualValues.Add(dr.GetXDocument("Short"));
                    actualValues.Add(dr.GetXDocument("Byte"));
                    actualValues.Add(dr.GetXDocument("Bool1"));
                    actualValues.Add(dr.GetXDocument("Bool2"));
                    actualValues.Add(dr.GetXDocument("Money1"));
                    actualValues.Add(dr.GetXDocument("Money2"));
                    actualValues.Add(dr.GetXDocument("Guid"));
                    actualValues.Add(dr.GetXDocument("ByteStream1"));
                    actualValues.Add(dr.GetXDocument("ByteStream2"));
                    actualValues.Add(dr.GetXDocument("ByteStream3"));
                    actualValues.Add(dr.GetXDocument("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetXDocument("NullString1"));
                    actualValues.Add(dr.GetXDocument("NullString2"));
                    actualValues.Add(dr.GetXDocument("NullString3"));
                    actualValues.Add(dr.GetXDocument("NullString4"));
                    actualValues.Add(dr.GetXDocument("NullString5"));
                    actualValues.Add(dr.GetXDocument("NullString6"));
                    actualValues.Add(dr.GetXDocument("NullString7"));
                    actualValues.Add(dr.GetXDocument("NullDateTime1"));
                    actualValues.Add(dr.GetXDocument("NullDateTime2"));
                    actualValues.Add(dr.GetXDocument("NullDateTime3"));
                    actualValues.Add(dr.GetXDocument("NullDateTime4"));
                    actualValues.Add(dr.GetXDocument("NullTime"));
                    actualValues.Add(dr.GetXDocument("NullDateTimeOffset"));
                    actualValues.Add(dr.GetXDocument("NullFloat"));
                    actualValues.Add(dr.GetXDocument("NullDouble"));
                    actualValues.Add(dr.GetXDocument("NullDecimal"));
                    actualValues.Add(dr.GetXDocument("NullLong"));
                    actualValues.Add(dr.GetXDocument("NullInt"));
                    actualValues.Add(dr.GetXDocument("NullShort"));
                    actualValues.Add(dr.GetXDocument("NullByte"));
                    actualValues.Add(dr.GetXDocument("NullBool"));
                    actualValues.Add(dr.GetXDocument("NullMoney1"));
                    actualValues.Add(dr.GetXDocument("NullMoney2"));
                    actualValues.Add(dr.GetXDocument("NullGuid"));
                    actualValues.Add(dr.GetXDocument("NullByteStream1"));
                    actualValues.Add(dr.GetXDocument("NullByteStream2"));
                    actualValues.Add(dr.GetXDocument("NullByteStream3"));
                    actualValues.Add(dr.GetXDocument("NullByteStream4"));
                });

            // assert
            for (int x = 0; x < actualValues.Count; x++)
            {
                bool bothNull = expectedValues[x] == null && actualValues[x] == null;
                if (!bothNull)
                {
                    Assert.IsNotNull(expectedValues[x]);
                    Assert.IsNotNull(actualValues[x]);
                    var expected = expectedValues[x].ToString();
                    var actual = actualValues[x].ToString();
                    Assert.AreEqual(expected, actual);
                }
            }

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SqlReader_GetByteArray_WorksWithAllNativeTypes()
        {
            // setup
            var sql = new Sql("TychoSqlTests");

            byte[] expected = new byte[2] { 17, 18 };

            var expectedValues = new List<byte[]>
            {
                // not null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                expected,
                expected,
                expected,
                expected,
                // null values
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            };
            var actualValues = new List<byte[]>();

            // act
            sql.Statement(_predefinedStatement)
                .ExecuteReader(dr =>
                {
                    // not null values
                    actualValues.Add(dr.GetByteArray("String1"));
                    actualValues.Add(dr.GetByteArray("String2"));
                    actualValues.Add(dr.GetByteArray("String3"));
                    actualValues.Add(dr.GetByteArray("String4"));
                    actualValues.Add(dr.GetByteArray("String5"));
                    actualValues.Add(dr.GetByteArray("String6"));
                    actualValues.Add(dr.GetByteArray("String7"));
                    actualValues.Add(dr.GetByteArray("DateTime1"));
                    actualValues.Add(dr.GetByteArray("DateTime2"));
                    actualValues.Add(dr.GetByteArray("DateTime3"));
                    actualValues.Add(dr.GetByteArray("DateTime4"));
                    actualValues.Add(dr.GetByteArray("Time"));
                    actualValues.Add(dr.GetByteArray("DateTimeOffset"));
                    actualValues.Add(dr.GetByteArray("Float"));
                    actualValues.Add(dr.GetByteArray("Double"));
                    actualValues.Add(dr.GetByteArray("Decimal"));
                    actualValues.Add(dr.GetByteArray("Long"));
                    actualValues.Add(dr.GetByteArray("Int"));
                    actualValues.Add(dr.GetByteArray("Short"));
                    actualValues.Add(dr.GetByteArray("Byte"));
                    actualValues.Add(dr.GetByteArray("Bool1"));
                    actualValues.Add(dr.GetByteArray("Bool2"));
                    actualValues.Add(dr.GetByteArray("Money1"));
                    actualValues.Add(dr.GetByteArray("Money2"));
                    actualValues.Add(dr.GetByteArray("Guid"));
                    actualValues.Add(dr.GetByteArray("ByteStream1"));
                    actualValues.Add(dr.GetByteArray("ByteStream2"));
                    actualValues.Add(dr.GetByteArray("ByteStream3"));
                    actualValues.Add(dr.GetByteArray("ByteStream4"));

                    // null values
                    actualValues.Add(dr.GetByteArray("NullString1"));
                    actualValues.Add(dr.GetByteArray("NullString2"));
                    actualValues.Add(dr.GetByteArray("NullString3"));
                    actualValues.Add(dr.GetByteArray("NullString4"));
                    actualValues.Add(dr.GetByteArray("NullString5"));
                    actualValues.Add(dr.GetByteArray("NullString6"));
                    actualValues.Add(dr.GetByteArray("NullString7"));
                    actualValues.Add(dr.GetByteArray("NullDateTime1"));
                    actualValues.Add(dr.GetByteArray("NullDateTime2"));
                    actualValues.Add(dr.GetByteArray("NullDateTime3"));
                    actualValues.Add(dr.GetByteArray("NullDateTime4"));
                    actualValues.Add(dr.GetByteArray("NullTime"));
                    actualValues.Add(dr.GetByteArray("NullDateTimeOffset"));
                    actualValues.Add(dr.GetByteArray("NullFloat"));
                    actualValues.Add(dr.GetByteArray("NullDouble"));
                    actualValues.Add(dr.GetByteArray("NullDecimal"));
                    actualValues.Add(dr.GetByteArray("NullLong"));
                    actualValues.Add(dr.GetByteArray("NullInt"));
                    actualValues.Add(dr.GetByteArray("NullShort"));
                    actualValues.Add(dr.GetByteArray("NullByte"));
                    actualValues.Add(dr.GetByteArray("NullBool"));
                    actualValues.Add(dr.GetByteArray("NullMoney1"));
                    actualValues.Add(dr.GetByteArray("NullMoney2"));
                    actualValues.Add(dr.GetByteArray("NullGuid"));
                    actualValues.Add(dr.GetByteArray("NullByteStream1"));
                    actualValues.Add(dr.GetByteArray("NullByteStream2"));
                    actualValues.Add(dr.GetByteArray("NullByteStream3"));
                    actualValues.Add(dr.GetByteArray("NullByteStream4"));
                });

            // assert
            for (int x = 0; x < actualValues.Count; x++)
            {
                bool bothNull = expectedValues[x] == null && actualValues[x] == null;

                if (!bothNull)
                {
                    Assert.IsNotNull(expectedValues[x]);
                    Assert.IsNotNull(actualValues[x]);
                    CollectionAssert.AreEqual(expectedValues[x], actualValues[x]);
                }
            }

            // cleanup
        }

    }
}
