﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tycho.Sql.Tests.DTOs
{
    [ExcludeFromCodeCoverage, TableParameter(null)]
    public class TableTypeWithMisconfiguredTableParameter
    {
        [TableParameterColumn("SomeName")]
        public string Id { get; set; }
        [TableParameterColumn("SomeOtherName", IsNullable = false)]
        public bool BooleanValue { get; set; }
        [TableParameterColumn("ThirdColumn", IsNullable = true, MaxLength = 20)]
        public string ThirdValue { get; set; }
    }
}
