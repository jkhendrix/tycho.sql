﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tycho.Sql.Tests.DTOs
{
    [ExcludeFromCodeCoverage, TableParameter("dbo.SomeType")]
    public class TableTypeWithParameters
    {
        [TableParameterColumn("SomeName")]
        public string Id { get; set; }
        
        [TableParameterColumn("SomeOtherName", IsNullable = false)]
        public bool BooleanValue { get; set; }
        
        [TableParameterColumn("ThirdColumn", IsNullable = false, MaxLength = 20)]
        public string ThirdValue { get; set; }
        
        [TableParameterColumn("NullColumn")]
        public int? NullColumn { get; set; }
    }
}
