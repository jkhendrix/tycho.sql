﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tycho.Sql.Tests.DTOs
{
    [ExcludeFromCodeCoverage]
    public class TableTypeWithNoTableParameter
    {
        public string Id { get; set; }
        public bool BooleanValue { get; set; }
    }
}
