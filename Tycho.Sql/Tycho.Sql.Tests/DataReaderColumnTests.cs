﻿using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tycho.Sql.Tests
{
    [TestClass, ExcludeFromCodeCoverage]
    public class DataReaderColumnTests
    {
        [TestMethod, TestCategory("Unit")]
        public void DataReaderColumn_Constructor_ThrowsIfDataTypeUnknown()
        {
            // setup
            bool caughtException = false;

            // act
            try
            {
                var c = new DataReaderColumn(1, "blah");
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Unknown column type"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void DataReaderColumn_Constructor_ThrowsIfDataTypeUnsupported()
        {
            // setup
            bool caughtException = false;

            // act
            try
            {
                var c = new DataReaderColumn(1, "Structured");
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("Unable to resolve native data type"))
                    caughtException = true;
            }

            // assert
            Assert.IsTrue(caughtException);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void DataReaderColumn_Constructor_HandlesAllExpectedSqlDataTypes()
        {
            // setup

            // act
            HandlesType(SqlDbType.VarChar, NativeType.String);
            HandlesType(SqlDbType.NVarChar, NativeType.String);
            HandlesType(SqlDbType.Char, NativeType.String);
            HandlesType(SqlDbType.NChar, NativeType.String);
            HandlesType(SqlDbType.Text, NativeType.String);
            HandlesType(SqlDbType.NText, NativeType.String);
            HandlesType(SqlDbType.Xml, NativeType.String);

            HandlesType(SqlDbType.DateTime, NativeType.DateTime);
            HandlesType(SqlDbType.DateTime2, NativeType.DateTime);
            HandlesType(SqlDbType.SmallDateTime, NativeType.DateTime);
            HandlesType(SqlDbType.Date, NativeType.DateTime);

            HandlesType(SqlDbType.Time, NativeType.TimeSpan);

            HandlesType(SqlDbType.DateTimeOffset, NativeType.DateTimeOffset);

            HandlesType(SqlDbType.Real, NativeType.Float);

            HandlesType(SqlDbType.Float, NativeType.Double);

            HandlesType(SqlDbType.Decimal, NativeType.Decimal);

            HandlesType(SqlDbType.BigInt, NativeType.Long);

            HandlesType(SqlDbType.Int, NativeType.Int);

            HandlesType(SqlDbType.SmallInt, NativeType.Short);

            HandlesType(SqlDbType.TinyInt, NativeType.Byte);

            HandlesType(SqlDbType.Bit, NativeType.Bool);

            HandlesType(SqlDbType.Money, NativeType.SqlMoney);
            HandlesType(SqlDbType.SmallMoney, NativeType.SqlMoney);

            HandlesType(SqlDbType.UniqueIdentifier, NativeType.Guid);

            HandlesType(SqlDbType.Binary, NativeType.ByteStream);
            HandlesType(SqlDbType.VarBinary, NativeType.ByteStream);
            HandlesType(SqlDbType.Image, NativeType.ByteStream);
            HandlesType(SqlDbType.Udt, NativeType.ByteStream);
            HandlesType(SqlDbType.Variant, NativeType.ByteStream);

            // assert

            // cleanup
        }

        private void HandlesType(SqlDbType expectedDataType, NativeType expectedNativeType)
        {
            // act
            var c = new DataReaderColumn(1, expectedDataType.ToString());

            // assert
            Assert.AreEqual(expectedDataType, c.DataType, string.Format("DataType Failure in {0}", expectedDataType));
            Assert.AreEqual(expectedNativeType, c.NativeType, string.Format("NativeType Failure in {0}", expectedNativeType));
        }

        [TestMethod, TestCategory("Unit")]
        public void DataReaderColumn_Constructor_DataTypeAcceptsCaseInsensitiveValues()
        {
            // setup
            var expectedDataType = SqlDbType.VarChar;


            // act
            var c = new DataReaderColumn(1, "varCHAR");

            // assert
            Assert.AreEqual(expectedDataType, c.DataType);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void DataReaderColumn_Object_OrdinalIsAccessible()
        {
            // setup
            int expectedOrdinal = 23;

            // act
            var c = new DataReaderColumn(expectedOrdinal, "VarChar");

            // assert
            Assert.AreEqual(expectedOrdinal, c.Ordinal);

            // cleanup
        }
    }
}
