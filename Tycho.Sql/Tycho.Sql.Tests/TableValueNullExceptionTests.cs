﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tycho.Sql.Tests
{
    [TestClass, ExcludeFromCodeCoverage]
    public class TableValueNullExceptionTests
    {
        [TestMethod, TestCategory("Unit")]
        public void TableValueNullException_Constructor_Works()
        {
            // setup

            // act
            var actual = new TableValueNullException();

            // assert
            Assert.IsNotNull(actual);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void TableValueNullException_Constructor_WithMessage_Works()
        {
            // setup
            string expectedMessage = "Expected Message";

            // act
            var actual = new TableValueNullException(expectedMessage);

            // assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expectedMessage, actual.Message);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void TableValueNullException_Constructor_WithMessageAndInnerException_Works()
        {
            // setup
            string expectedInnerMessage = "Inner Exception";
            Exception expectedException = new Exception(expectedInnerMessage);
            string expectedMessage = "Expected Message";
            
            // act
            var actual = new TableValueNullException(expectedMessage, expectedException);

            // assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expectedMessage, actual.Message);
            Assert.IsNotNull(actual.InnerException);
            Assert.AreEqual(expectedInnerMessage, actual.InnerException.Message);

            // cleanup
        }
    }
}
