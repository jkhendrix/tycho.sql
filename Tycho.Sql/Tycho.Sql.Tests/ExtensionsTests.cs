﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tycho.Sql.Tests
{
    [TestClass, ExcludeFromCodeCoverage]
    public class ExtensionsTests
    {
        [TestMethod, TestCategory("Unit")]
        public void SubstringBefore_ReturnsSubstringWhenMatchFound()
        {
            // setup
            string expected = "This is the substring.";
            string match = "^^match..";
            string value = expected + match;

            // act
            var actual = value.SubstringBefore(match);

            // assert
            Assert.AreEqual(expected, actual);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void SubstringBefore_ReturnsOriginalWhenMatchNotFound()
        {
            // setup
            string expected = "This is the substring.";
            string match = "^^match..";
            string value = expected;

            // act
            var actual = value.SubstringBefore(match);

            // assert
            Assert.AreEqual(expected, actual);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void TruncateTo_ReturnsOriginalValueIfMaxLengthGreaterThanStringLength()
        {
            // setup
            string source = "12345678901234567890abcdeabcde";
            string expectedValue = source;
            int maxLength = 50;             // greater than source length

            // act
            var actualValue = source.TruncateTo(maxLength);

            // assert
            Assert.AreEqual(expectedValue, actualValue);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void TruncateTo_ReturnsTruncatedValueIfMaxLengthLessThanStringLength()
        {
            // setup
            int maxLength = 20;             // less than source length
            string source = "12345678901234567890abcdeabcde";
            string expectedValue = "12345678901234567890";

            // act
            var actualValue = source.TruncateTo(maxLength);

            // assert
            Assert.AreEqual(expectedValue, actualValue);

            // cleanup
        }

        [TestMethod, TestCategory("Unit")]
        public void TruncateTo_ReturnsOriginalValueIfMaxLengthIsNull()
        {
            // setup
            int? maxLength = null;
            string source = "12345678901234567890abcdeabcde";
            string expectedValue = source;

            // act
            var actualValue = source.TruncateTo(maxLength);

            // assert
            Assert.AreEqual(expectedValue, actualValue);

            // cleanup
        }

    }
}
